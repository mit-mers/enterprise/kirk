;;; -*- mode: common-lisp; -*-

(rmpl:defpackage #:costa-rica/area-3-sentinel-easy
  (:use #:cl
        #:rmpl/lib/auv
        #:rmpl/lib/path-planning
        #:rmpl/lib/vrp)
  (:import-from #:kirk-v2/planner/rmpl-lib/path-planning
                #:move-from-to))

(in-package #:costa-rica/area-3-sentinel-easy)

(defclass falkor (surface-vehicle)
  ())

(defclass glider (auv)
  ()
  (:default-initargs
   :dynamics-json-string (uiop:read-file-string "/home/etimmons/mtk/workspaces/primary/kirk-v2/tests/scouts-api/area-3-sentinel-easy/glider-dynamics.json")))

;; This will be provided by the path planning library.
(define-control-program move ((v glider) to)
  (declare (primitive v)))

(define-control-program surface (glider)
  (declare (primitive glider)))

(define-control-program deploy (falkor glider)
  (declare (primitive falkor)))

(define-control-program retrieve (falkor glider)
  (declare (primitive falkor)))

(define-control-program dropoff-glider (glider first-task)
  (let ((falkor (slot-value *world* 'falkor)))
    (move falkor (task-entry-point first-task))
    (deploy falkor glider)))

(define-control-program end-glider-mission (glider end-task)
  (let ((falkor (slot-value *world* 'falkor)))
    (seq (:slack :throughout)
      (surface glider)
      (retrieve falkor glider))))

(defclass world-with-environment (standard-model-object)
  ((environment
    :initform (make-instance
               'path-planning-environment
               :geojson-blob
               (kirk-v2-http/json:parse-json-string (uiop:read-file-string "/home/etimmons/mtk/workspaces/primary/kirk-v2/tests/scouts-api/area-3-sentinel-easy/map.json")))
    :final t)))


;;; These are likely going to be the only thing of substance (along with the
;;; package forms) that will need to be updated per call to the planner.

(defclass world (world-with-environment)
  ;; These slots are marked as final because no primitive action can change the
  ;; object to which they are bound. This sort of information can help the
  ;; planner *immensely*.
  ((sentinel
    :type glider
    :final t
    :initform (make-instance 'glider
                             :start-location (make-instance 'point
                                                            :x -84.81967383
	                                                        :y 9.15032611)
                             :id 1))
   (group-1-tasks
    :type list
    :final t
    :initform (list
               (make-instance 'point-sample-task
                              :x -84.80255127
                              :y 9.17350946
                              ;; :z -744.57
                              :z -790)
               (make-instance 'point-sample-task
                              :x -84.80374512
                              :y 9.17378925
                              ;; :z -757.02
                              :z -790)
               (make-instance 'point-sample-task
                              :x -84.79961792
                              :y 9.15874164
                              ;; :z -765.7
                              :z -790)
               (make-instance 'point-sample-task
                              :x -84.79932275
                              :y 9.15764996
                              ;; :z -784.84
                              :z -790)
               (make-instance 'point-sample-task
                              :x -84.80683081
                              :y 9.15665845
                              ;; :z -785.55
                              :z -790)))
   ;; -887
   (group-n-tasks
    :type list
    :final t
    :initform (list
               (make-instance 'point-sample-task
                              :x -84.82277124
                              :y 9.14343091
                              ;; :z -910.42
                              :z -887)))
   ;; -830
   (group-2-tasks
    :type list
    :final t
    :initform (list
               (make-instance 'point-sample-task
                              :x -84.81967383
                              :y 9.15032611
                              ;; :z -840.88
                              :z -830)
               (make-instance 'point-sample-task
                              :x -84.8221853
                              :y 9.14932764
                              ;; :z -855.55
                              :z -830)))

   ;; -787
   (group-3-tasks
    :type list
    :final t
    :initform (list
               (make-instance 'point-sample-task
                              :x -84.81910217
                              :y 9.16011584
                              ;; :z -791.61
                              :z -787)
               (make-instance 'point-sample-task
                              :x -84.81791089
                              :y 9.16012848
                              ;; :z -812.72
                              :z -787)
               (make-instance 'point-sample-task
                              :x -84.80566772
                              :y 9.15504987
                              ;; :z -809.13
                              :z -787)))

   ;; ;; -787
   ;; (group-4-tasks
   ;;  :type list
   ;;  :final t
   ;;  :initform (list
   ;;             (make-instance 'point-sample-task
   ;;                            :x -84.80566772
   ;;                            :y 9.15504987
   ;;                            ;; :z -809.13
   ;;                            :z -787)))

   ;; -760
   (group-5-tasks
    :type list
    :final t
    :initform (list
               (make-instance 'point-sample-task
                              :x -84.79961792
                              :y 9.15874164
                              ;; :z -765.7
                              :z -760)
               (make-instance 'point-sample-task
                              :x -84.79932275
                              :y 9.15764996
                              ;; :z -784.84
                              :z -760)
               (make-instance 'point-sample-task
                              :x -84.80683081
                              :y 9.15665845
                              ;; :z -785.55
                              :z -760)))

   ;; -727
   (group-6-tasks
    :type list
    :final t
    :initform (list
               (make-instance 'point-sample-task
                              :x -84.79988782
                              :y 9.17355396
                              ;; :z -729.69
                              :z -727)
               (make-instance 'point-sample-task
                              :x -84.79892285
                              :y 9.1732641
                              ;; :z -737.27
                              :z -727)
               (make-instance 'point-sample-task
                              :x -84.80255127
                              :y 9.17350946
                              ;; :z -744.57
                              :z -727)
               (make-instance 'point-sample-task
                              :x -84.80374512
                              :y 9.17378925
                              ;; :z -757.02
                              :z -727)))

   ;; -887
   (group-7-tasks
    :type list
    :final t
    :initform (list
               (make-instance 'point-sample-task
                              :x -84.82726794
                              :y 9.19427124
                              ;; :z -902.27
                              :z -887)
               (make-instance 'point-sample-task
                              :x -84.82699109
                              :y 9.19176434
                              ;; :z -903.15
                              :z -887)
               (make-instance 'point-sample-task
                              :x -84.82774292
                              :y 9.19731757
                              ;; :z -918.52
                              :z -887)))))

(define-control-program main ()
  (let (;; Extract our vehicles from the world state. *WORLD* is a dynamic
        ;; variable that Kirk binds to the current state of the world (as
        ;; extracted from the planning request) when planning starts.
        (sentinel (slot-value *world* 'sentinel)))
    (with-blocks (vrp-block-1 vrp-block-2 vrp-block-3
                              vrp-block-4 vrp-block-5 vrp-block-6 vrp-block-7
                              main-block)
      ;; Assemble the VRP problem. This function returns an object with all the
      ;; typical VRP constraints and variables
      (assert! (simple-temporal :from-event (start-of main-block)
                                :to-event (end-of main-block)
                                :lower-bound 0
                                :upper-bound (* 24 60 60)))
      (rmpl-block main-block
        (let ((vrp-1 (make-vrp-problem
                      (list sentinel)
                      (slot-value *world* 'group-1-tasks)
                      ;; Allow tasks to be dropped.
                      :allow-dropping t
                      ;; Use spock to evaluate the goodness of the dropping and
                      ;; ordering of the tasks.
                      ;; :reward-function 'spock
                      ))
              ;; (vrp-2 (make-vrp-problem
              ;;         (list sentinel)
              ;;         (slot-value *world* 'group-2-tasks)
              ;;         ;; Allow tasks to be dropped.
              ;;         :allow-dropping t
              ;;         ;; Use spock to evaluate the goodness of the dropping and
              ;;         ;; ordering of the tasks.
              ;;         :reward-function 'spock
              ;;         ))
              ;; (vrp-3 (make-vrp-problem
              ;;         (list sentinel)
              ;;         (slot-value *world* 'group-3-tasks)
              ;;         ;; Allow tasks to be dropped.
              ;;         :allow-dropping t
              ;;         ;; Use spock to evaluate the goodness of the dropping and
              ;;         ;; ordering of the tasks.
              ;;         :reward-function 'spock
              ;;         ))
              ;; (vrp-5 (make-vrp-problem
              ;;         (list sentinel)
              ;;         (slot-value *world* 'group-5-tasks)
              ;;         ;; Allow tasks to be dropped.
              ;;         :allow-dropping t
              ;;         ;; Use spock to evaluate the goodness of the dropping and
              ;;         ;; ordering of the tasks.
              ;;         :reward-function 'spock
              ;;         ))
              ;; (vrp-6 (make-vrp-problem
              ;;         (list sentinel)
              ;;         (slot-value *world* 'group-6-tasks)
              ;;         ;; Allow tasks to be dropped.
              ;;         :allow-dropping t
              ;;         ;; Use spock to evaluate the goodness of the dropping and
              ;;         ;; ordering of the tasks.
              ;;         :reward-function 'spock
              ;;         ))
              ;; (vrp-7 (make-vrp-problem
              ;;         (list sentinel)
              ;;         (slot-value *world* 'group-7-tasks)
              ;;         ;; Allow tasks to be dropped.
              ;;         :allow-dropping t
              ;;         ;; Use spock to evaluate the goodness of the dropping and
              ;;         ;; ordering of the tasks.
              ;;         :reward-function 'spock
              ;;         ))
              )
          (seq ()
            (seq (:slack t
                  :block vrp-block-1)
              (perform-vrp vrp-1))
            ;; (move-from-to sentinel
            ;;               (vrp-vehicle-last-task vrp-1 sentinel)
            ;;               (vrp-vehicle-first-task vrp-2 sentinel))
            ;; (seq (:slack t
            ;;       :block vrp-block-2)
            ;;   (perform-vrp vrp-2))
            ;; (move-from-to sentinel
            ;;               (vrp-vehicle-last-task vrp-2 sentinel)
            ;;               (vrp-vehicle-first-task vrp-3 sentinel))
            ;; (seq (:slack t
            ;;       :block vrp-block-3)
            ;;   (perform-vrp vrp-3))
            ;; (move-from-to sentinel
            ;;               (vrp-vehicle-last-task vrp-3 sentinel)
            ;;               (vrp-vehicle-first-task vrp-5 sentinel))
            ;; (seq (:slack t
            ;;       :block vrp-block-5)
            ;;   (perform-vrp vrp-5))
            ;; (move-from-to sentinel
            ;;               (vrp-vehicle-last-task vrp-5 sentinel)
            ;;               (vrp-vehicle-first-task vrp-6 sentinel))
            ;; (seq (:slack t
            ;;       :block vrp-block-6)
            ;;   (perform-vrp vrp-6))
            ;; (move-from-to sentinel
            ;;               (vrp-vehicle-last-task vrp-6 sentinel)
            ;;               (vrp-vehicle-first-task vrp-7 sentinel))
            ;; (seq (:slack t
            ;;       :block vrp-block-7)
            ;;   (perform-vrp vrp-7))
            ))))))
