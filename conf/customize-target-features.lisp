;; Configure SBCL to include core compression

(lambda (list)
  (flet ((enable (x)
           (pushnew x list))
         (disable (x)
           (setf list (remove x list))))
    (enable :sb-core-compression)
    list))
