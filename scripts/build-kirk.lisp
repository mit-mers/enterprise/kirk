(require :asdf)

(ensure-directories-exist (asdf:system-relative-pathname :kirk-v2 "bin/"))

(when (uiop:os-windows-p)
  (uiop:copy-file (asdf:system-relative-pathname :kirk-v2 "License.rtf")
                  (asdf:system-relative-pathname :kirk-v2 "bin/License.rtf")))

(asdf:load-system :kirk-v2)
(asdf:load-system :kirk-v2/cli/main)
(setf uiop:*image-entry-point* #'kirk-v2/cli/main::main)
(sb-ext:save-lisp-and-die (asdf:system-relative-pathname :kirk-v2
                                                         (if (uiop:os-windows-p)
                                                             "bin/kirk.exe"
                                                             "bin/kirk"))
                          :executable t
                          :toplevel #'uiop:restore-image
                          :compression (uiop:featurep :sb-core-compression)
                          :save-runtime-options t)
