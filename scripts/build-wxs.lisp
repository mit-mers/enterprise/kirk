;;;; Builds a WiX XML file for building an MSI installer

(in-package #:cl-user)

(require :asdf)

(asdf:load-system :s-xml)

(defun build-wxs ()
  `(("Wix" "xmlns" "http://schemas.microsoft.com/wix/2006/wi")
    (("Product" "Id" "*"
                "Name" "Kirk Executive"
                "Version" ,(asdf:component-version (asdf:find-system :kirk-v2))
                "Manufacturer" "https://mers.csail.mit.edu"
                ;; This code needs to be unique and match the next reference
                ;; to it.
                "UpgradeCode" "081A497C-E34C-430D-BA19-328491CE5464"
                "Language" "1033")
     (("Package" "Id" "*"
                 "Manufacturer" "https://mers.csail.mit.edu"
                 "InstallerVersion" "200"
                 "Compressed" "yes"
                 "Platform" "x64"
                 "InstallScope" "perMachine"))
     (("Media" "Id" "1"
               "Cabinet" "kirk.cab"
               "EmbedCab" "yes"))
     (("Property" "Id" "PREVIOUSVERSIONSINSTALLED"
                  "Secure" "yes"))
     (("Upgrade" "Id" "081A497C-E34C-430D-BA19-328491CE5464")
      (("UpgradeVersion" "Minimum" "0.1.0"
                         "Maximum" "99.0.0"
                         "Property" "PREVIOUSVERSIONSINSTALLED"
                         "IncludeMinimum" "yes"
                         "IncludeMaximum" "no")))
     (("InstallExecuteSequence")
      (("RemoveExistingProducts" "After" "InstallInitialize")))

     (("Directory" "Id" "TARGETDIR"
                   "Name" "SourceDir")
      (("Directory" "Id" "ProgramFiles64Folder"
                    "Name" "PFiles")
       (("Directory" "Id" "BaseFolder"
                     "Name" "Kirk")
        (("Directory" "Id" "INSTALLDIR")
         (("Directory" "Id" "BINDIR"
                       "Name" "bin")
          (("Component" "Id" "Kirk_SetPATH"
                        "Guid" "542E7B3A-DE9C-4642-B20F-0BD634D02881"
                        "DiskId" "1"
                        "Win64" "yes")
           (("CreateFolder"))
           (("Environment" "Id" "Env_PATH"
                           "System" "yes"
                           "Action" "set"
                           "Name" "PATH"
                           "Part" "last"
                           "Value" "[BINDIR]")))
          (("Component" "Id" "Kirk_Bin"
                        "Guid" "6CA19542-EBAD-4DD3-8078-40BD56C899F2"
                        "DiskId" "1"
                        "Win64" "yes")
           (("File" "Name" "kirk.exe"
                    "Source" "bin/kirk.exe"))))))))

     (("Feature" "Id" "Minimal"
                 "Title" "Kirk"
                 "ConfigurableDirectory" "INSTALLDIR"
                 "Level" "1")
      (("ComponentRef" "Id" "Kirk_Bin"))
      (("Feature" "Id" "SetPath"
                  "Level" "1"
                  "Title" "Set Environment Variable: PATH")
       (("ComponentRef" "Id" "Kirk_SetPATH"))))
     (("WixVariable" "Id" "WixUILicenseRtf"
                     "Value" "License.rtf"))
     (("Property" "Id" "WIXUI_INSTALLDIR"
                  "Value" "INSTALLDIR"))
     (("UIRef" "Id" "WixUI_FeatureTree")))))

(defun write-wxs ()
  (with-open-file (s (asdf:system-relative-pathname :kirk-v2 "kirk.wxs")
                   :if-exists :supersede :direction :output)
    (format s "<?xml version='1.0'?>~%")
    (s-xml:print-xml (build-wxs) :stream s :pretty t)))

(print (build-wxs))
(write-wxs)
