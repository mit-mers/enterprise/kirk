(defpackage #:kirk-scripts
  (:use #:cl))

(in-package #:kirk-scripts)

(require :asdf)

(defvar *script-file-pathname* *load-truename*
  "The pathname to this file.")

(defvar *root-pathname* (uiop:pathname-parent-directory-pathname
                         (uiop:pathname-directory-pathname *script-file-pathname*))
  "The pathname to the root directory of Kirk-v2 repo.")

(defvar *build-root-pathname* (merge-pathnames "build/"
                                               *root-pathname*)
  "The pathname to the root of the build directory. Defaults to build/ inside
*ROOT-PATHNAME*")

(defun delete-kirk-scripts-package ()
  (delete-package :kirk-scripts)
  (setf uiop:*image-dump-hook* (remove 'delete-kirk-scripts-package uiop:*image-dump-hook*)))

(uiop:register-image-dump-hook 'delete-kirk-scripts-package)
