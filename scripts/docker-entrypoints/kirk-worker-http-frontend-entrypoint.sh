#! /bin/sh
#-*- sh-basic-offset: 2; -*-
#
#

set -e

# Set the default arguments based on $ENV.

if [ "x$ENV" = "xdev" ]; then
  # In dev, we want swank running and listening on every interface (since this
  # *is* a docker entrypoint and we'll likely want to connect from outside the
  # container).
  set -- --swank --swank-port 4005 --swank-interface 0.0.0.0 --swank-use-asdf-fasl-cache "$@"
  # Additionally, in dev, we would like to be able to mount code, C-c C-k it,
  # and not have the .fasl written next to the .lisp file using whatever UID
  # the container is running with...
fi

exec kirk-worker-http-frontend "$@"
