;;;; Script to build Kirk releases

(in-package #:cl-user)

(require "asdf")

(load "scripts/dependencies.lisp")

(uiop:define-package #:kirk-v2-scripts/build-release
    (:use #:cl))

(in-package #:kirk-v2-scripts/build-release)

(asdf:load-system :adopt)

(defparameter *option-help*
  (adopt:make-option
   :help
   :help "Print this help text and exit"
   :short #\h
   :long "help"
   :reduce (constantly t)))

(defparameter *option-static*
  (adopt:make-option
   :static
   :help "Build a static executable"
   :long "static"
   :reduce (constantly t)))

(defparameter *ui*
  (adopt:make-interface
   :name "scripts/build-release.lisp"
   :summary "Build script for Kirk-V2"
   :help "Build script for KIRK-V2"
   :usage "[options]"
   :contents (list *option-help*
                   *option-static*)))

(defvar *args*)
(defvar *opts*)

(multiple-value-setq (*args* *opts*) (adopt:parse-options *ui*))

(when (gethash :help *opts*)
  (adopt:print-help-and-exit *ui*))

;; TODO: This is a bit hacky, but speeds up the build significantly when
;; starting from scratch (like in CI). The root problem is that
;; asdf-release-ops will occasionally cause the same code to be compiled twice:
;; once in the child process and once in the parent. This is because we use
;; asdf:monolithic-lib-op in the parent. However, moving that op to the child
;; didn't quite work as it would occasionally error out due to package
;; variance.

;; Needed on SBCL 2.1.2 to compile cl-mathstats (a dependency of Odo). This is
;; obviously not ideal. But should be temporary. If it's an SBCL bug, it'll
;; hopefully get fixed in 2.1.3 or 2.1.4 (2.1.3 freeeze has already
;; started). If SBCL says it's not a bug, we'll cut out cl-mathstats.

(when (uiop:string-prefix-p "sbcl-2.1.2" (uiop:implementation-identifier))
  (declaim (notinline coerce)))

(asdf:load-system :kirk-v2)
(asdf:load-system :kirk-v2/cli/main)

(defparameter *op* (if (gethash :static *opts*)
                       'asdf-release-ops:static-release-archive-op
                       'asdf-release-ops:dynamic-release-archive-op))

(asdf:operate *op* :kirk-v2)

(format uiop:*stdout*
        "A ~:[dynamic~;static~] release has been built at ~A~%"
        (gethash :static *opts*)
        (asdf:output-file *op* :kirk-v2))
