;;;; KIRK-V2 System Definition.

#-:asdf3.1
(error "Requires ASDF >=3.1")

(defsystem #:kirk-v2-tests
  :version (:read-file-form "src/version.lisp" :at (2 2))
  ;; :class :package-inferred-system
  :pathname "test"
  :serial t
  :components ((:file "package")
               (:file "test-suite" :depends-on ("package"))
               (:file "sample-state-plan" :depends-on ("package"))
               (:file "dynamic-scheduling" :depends-on ("package" "sample-state-plan"))
               (:file "dynamic-dispatching" :depends-on ("package" "sample-state-plan"))
               (:file "rmpl-model" :depends-on ("package"))
               ;;(:file "delay-scheduling" :depends-on ("package" "sample-state-plan"))
               ;; I (Cam) am pretty sure the use of mtk-executive is killing test ops
               ;; I suspect it has something to do with the hunchentoot server exiting
               ;; (:file "morning-lecture")
               )
  :depends-on (#:kirk-v2))
