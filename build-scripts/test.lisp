(in-package :cl-user)

(defun main (argv)
  (qlot:with-local-quicklisp :kirk-v2
    (ql:quickload :kirk-v2/test)
    (funcall (intern #.(string :run) :prove) :kirk-v2/test)))
