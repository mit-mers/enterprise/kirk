(in-package #:kirk-v2)

(doc:defsection @kirk (:title "Kirk")
  "This is the Kirk executive documentation and code cleanup is currently in progress.

Kirk has several components. The goal is to allow users to combine these
components in multiple ways/reuse them in other executives to avoid code
duplication and wasted effort."
  (@kirk-scheduler section))

(doc:defsection @kirk-scheduler (:title "Scheduler")
  "The scheduler component is responsible for dynamically scheduling the
execution of events. It keeps track of a partial schedule and presents a series
of decisions that can be made. It is ultimately up to the dispatcher to choose
which decision to commit to and communicate it to the appropriate agents.

More detail can be found in [`kirk-v2/scheduler:@scheduler`]")
