(uiop:define-package #:kirk-v2
  (:use #:cl)
  (:use-reexport #:kirk-v2/executive)
  (:local-nicknames (#:doc #:40ants-doc)))

(in-package #:kirk-v2)
