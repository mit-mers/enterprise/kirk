(in-package #:kirk-v2/dispatcher)

(defclass event-dispatcher (dispatcher)
  ((%scheduler))
  (:documentation
   "A dispatcher that sends events to be dispatched. Will call DISPATCH! with
an instance of BATCH-DISPATCH-EVENTS."))

(defclass batch-dispatch-events (dispatch-driver-item)
  ((%events
    :initarg :events
    :initform nil
    :reader events-of))
  (:documentation
   "THUNK will be a function that takes two keyword arguments: EVENTS (defaulting to all events),
and TIME."))

(defmethod initialize-instance :after ((dispatcher event-dispatcher) &key plan)
  (setf (slot-value dispatcher '%scheduler)
        (make-instance ;;'sched::amc-scheduler
                       'sched:minimum-dispatchable-form-scheduler
                       :state-plan plan
                       :treat-loose-precedence-as-strong-p t)))


;; * Dispatching

(defmethod run-dispatcher ((dispatcher event-dispatcher))
  (let* ((scheduler (scheduler-of dispatcher))
         (clock (clock-of dispatcher))
         (start-stamp (exec:clock-timestamp clock))
         (message nil)
         (dispatched (make-hash-table))
         (next-decisions nil))
    ;; (setf (start-timestamp-of dispatcher) start-stamp)
    (flet ((get-next-decisions ()
             (sort (remove-if (lambda (x) (some (lambda (y) (gethash y dispatched)) x))
                              (copy-list (sched:active-decisions scheduler))
                              :key #'sched:dispatch-events)
                   (lambda (left right)
                     (or (< (sched:dispatch-earliest-time left)
                            (sched:dispatch-earliest-time right))
                         (and (= (sched:dispatch-earliest-time left)
                                 (sched:dispatch-earliest-time right))
                              (< (sched:dispatch-constrains-next-decision-within left)
                                 (sched:dispatch-constrains-next-decision-within right))))))))
      (loop
        ;; First, process the message, if any.
        (unless (null message)
          (destructuring-bind (message-type &rest args) message
            (ecase message-type
              ;; Message from the driver that events have actually been executed.
              (:executed
               (loop
                 :for (events time) :in args
                 :do (sched:mark-events-executed scheduler events time))
               ;; This could invalidate our next decision!
               (setf next-decisions nil)))))
        (when (null next-decisions)
          (setf next-decisions (get-next-decisions)))
        ;; While we have actionable decisions, do them!
        (let ((current-time (time:timestamp-difference (exec:clock-timestamp clock)
                                                       start-stamp)))
          (loop
            :for next := (pop next-decisions)
            :while next
            :if (<= (sched:dispatch-earliest-time next) current-time)
              :do
                 (let ((events-to-dispatch (copy-list (sched:dispatch-events next))))
                   (mapc (lambda (e) (setf (gethash e dispatched) t)) events-to-dispatch)
                   (dispatch! (driver-of dispatcher)
                              (make-instance 'batch-dispatch-events
                                              :events events-to-dispatch)
                              current-time
                              (lambda (&key
                                         (events events-to-dispatch)
                                         (time (time:timestamp-difference (exec:clock-timestamp clock)
                                                                          start-stamp)))
                                (let* ((events (alex:ensure-list events))
                                       (time (if (typep time 'local-time:timestamp)
                                                 (time:timestamp-difference time start-stamp)
                                                 time))
                                       (message (list (list events time))))
                                  (queue:mailbox-send-message (mailbox-of dispatcher)
                                                              (list* :executed message))))))
            :else
              :do (push next next-decisions)
                  (return)))
        ;; Now, wait for more messages or for the next decision to be
        ;; actionable.
        (if (sched:has-unexecuted-events-p scheduler)
            ;; There are events left to execute! Sleep until we get a new
            ;; message. It's tempting to use CLOCK-SLEEP here, but it doesn't tie
            ;; in well with any efficient sleeping functions (like used by
            ;; MAILBOX-RECEIVE-MESSAGE). We could spawn another thread that
            ;; pushes a message into the mailbox, but let's keep things single
            ;; threaded for now.
            (let ((next-time (if next-decisions (sched:dispatch-earliest-time (first next-decisions)))))
              (loop
                (setf message (safe-queue:mailbox-receive-message (mailbox-of dispatcher)
                                                                  :timeout 1f-2))
                (when (or (not (null message))
                          (and (not (null next-time))
                               (>= (time:timestamp-difference (exec:clock-timestamp clock)
                                                              start-stamp)
                                   next-time)))
                  ;; Break out of the loop if we receive a message or time
                  ;; progresses to the point we can do something useful.
                  (return))))
            ;; We're done!
            (return))))))
