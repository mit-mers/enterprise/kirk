(uiop:define-package #:kirk-v2/dispatcher
  (:use #:cl)
  (:local-nicknames (#:alex #:alexandria)
                    (#:doc #:40ants-doc)
                    (#:exec #:mtk-executive)
                    (#:queue #:safe-queue)
                    (#:sched #:kirk-v2/scheduler)
                    (#:time #:local-time))
  (:export #:dynamic-dispatcher
           #:observe-event!
           #:execute-now!
           #:confirm-execution!))

(in-package #:kirk-v2/dispatcher)
