(in-package #:kirk-v2/dispatcher)

(defclass mtk-executive-event-driver (dispatch-driver)
  ((%context
    :initarg :context
    :reader context-of)
   (%child-executive
    :initarg :child-executive
    :reader child-executive-of)
   (%child-contexts-ht
    :initform (make-hash-table)
    :reader child-contexts-ht-of)))

(defclass child-context ()
  ((%state-plan
    :initarg :state-plan
    :reader state-plan-of)
   (%thunk
    :accessor thunk-of)
   (%context
    :initarg :context
    :accessor context-of)))

(defun child-context-of (driver event)
  (gethash event (child-contexts-ht-of driver)))

(defun (setf child-context-of) (value driver event)
  (setf (gethash event (child-contexts-ht-of driver))
        value))

(defun make-event-state-plan (start-or-end name args)
  (odo:make-state-plan :start-event (odo:make-event :name (list start-or-end (list* name args)))))

(defun make-child-context (driver state-plan event)
  (let* ((child-context (make-instance 'child-context
                                       :state-plan state-plan))
         (real-context
           (exec:make-execution-context
            (child-executive-of driver)
            nil
            (make-instance 'exec:state-plan-goal :state-plan state-plan)
            nil
            (lambda (context message)
              (declare (ignore context))
              (typecase message
                (exec:event-executed-message
                 (exec:send-message (context-of driver)
                                    (make-instance 'exec:event-executed-message
                                                   :time (exec:time-of message)
                                                   :event-name (odo:name event)))
                 (funcall (thunk-of child-context)
                          :time (exec:time-of message)
                          :events event)))))))
    (setf (context-of child-context) real-context)
    child-context))

(defmethod initialize-instance :after ((driver mtk-executive-event-driver) &key plan)
  ;; Go through the plan, extract every episode corresponding to an
  ;; activity. Then create a context for each of them in the child.
  (dolist (ep (odo:goal-episode-list plan))
    (let ((activity-name (odo:activity-name ep))
          (activity-args (odo:activity-args ep)))
      (unless (null activity-name)
        (let ((start-event (odo:start-event ep))
              (end-event (odo:end-event ep))
              (start-plan (make-event-state-plan :start activity-name activity-args))
              (end-plan (make-event-state-plan :end activity-name activity-args)))
          ;; HACK? This only works if each event is attached to at most one
          ;; episode.
          (setf (child-context-of driver start-event)
                (make-child-context driver start-plan start-event))
          (setf (child-context-of driver end-event)
                (make-child-context driver end-plan end-event)))))))

(defmethod dispatch! ((driver mtk-executive-event-driver) (item batch-dispatch-events)
                      time thunk)
  "DEPRECATED"
  (let ((immediately-executed-events ()))
    (dolist (event (events-of item))
      (let ((child-context (child-context-of driver event)))
        (if (null child-context)
            (let ((msg-to-parent (make-instance 'exec:event-executed-message
                                                :time time
                                                :event-name (odo:name event))))
              (exec:send-message (context-of driver) msg-to-parent)
              (push event immediately-executed-events))
            (progn
              (setf (thunk-of child-context) thunk)
              (exec:execute-plan! (context-of child-context)
                                  (state-plan-of child-context))))))
    (unless (null immediately-executed-events)
      (funcall thunk :events immediately-executed-events :time time))))

(defmethod execute-now! ((driver mtk-executive-event-driver) (item batch-dispatch-events))
  "Should only ever be called wtih real events"
  (let ((immediately-executed-events ())
        (time (exec:clock-timestamp (clock-of driver))))
    (dolist (event (events-of item))
      (let ((child-context (child-context-of driver event)))
        (if (null child-context)
            (let ((msg-to-parent (make-instance 'exec:event-executed-message
                                                :time time
                                                :event-name (odo:name event))))
              (exec:send-message (context-of driver) msg-to-parent)
              (push event immediately-executed-events))
            (exec:execute-plan! (context-of child-context)
                                (state-plan-of child-context)))))
    (unless (null immediately-executed-events)
      (confirm-execution! driver immediately-executed-events))))
