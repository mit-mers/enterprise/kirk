(in-package #:kirk-v2/dispatcher)

(defclass dispatcher ()
  ((%clock
    :initarg :clock
    :reader clock-of)
   (%mailbox
    :initarg :mailbox
    :initform (safe-queue:make-mailbox)
    :reader mailbox-of
    :documentation
    "A mailbox used to communicate information from other threads (external to
the dispatcher) to the main dispatch thread.")
   (%driver
    :initarg :driver
    :reader driver-of)
   (%scheduler
    :reader scheduler-of)
   ;; These slots are set only once dispatching begins.
   ;; (%start-time
   ;;  :accessor start-time-of)
   )
  (:documentation
   "A dispatcher is responsible for receiving real-time execution decisions (RTEDs) from a scheduler
    and sending events to a `driver' to be performed in the real-world. An online (or dynamic)
    dispatcher is additionally responsible for keeping a schedule updated as events are observed.
    Schedules may include noop events in RTEDs, which should indicate to the dispatcher to simply
    update the schedule immediately at the time of the RTED without sending said noop events to the
    driver."))

(defgeneric run-dispatcher (dispatcher))

(defgeneric observe-event! (dispatcher event-id))
