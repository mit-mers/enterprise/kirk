;;;; A dynamic dispatcher largely based on event-dispatcher. A key difference is that we wait as
;;;; long as possible to send events to the driver. This allows contingent events to preempt free
;;;; event execution
;;;;
;;;; The scheduler follows the semantics of variable-delay controllability (VDC) checking in our
;;;; response to contingent events. The execution range of contingent events in the commitments is
;;;; not necessarily representative of the /possible/ range of observations. This is due to the
;;;; "worst-case scenario" behavior of VDC. As a result we:
;;;;
;;;; 1. Buffer contingent responses to their lower bounds (as given by the commitments)
;;;; 2. Imagine that contingent events occur at their upper bounds if we have not received an
;;;;    observation by then

(in-package #:kirk-v2/dispatcher)

(defclass dynamic-dispatcher (dispatcher)
  ((%scheduler)
   (start-timestamp
    :initform nil
    :accessor start-timestamp-of
    :documentation "Track the start time of dispatching")
   (epsilon
    :initform 0.001
    :initarg :epsilon
    :reader epsilon-of
    :documentation
    "Used to compare current time with execution time for an event. We'll send a free event to the
     driver to be executed when it's within epsilon time units of its lower bound. An epsilon of 0
     will execute events at the exact lower bounds. You may want a small epsilon to account for a
     lag in execution, eg. from sending a ROS message or an HTTP POST.")
   (reschedule-early-ctg-p
    :initarg :reschedule-early-ctg-p
    :initform nil
    :reader reschedulep
    :documentation "See the eponymous slot on `sched:dynamic-scheduler'")
   (rted-invalidated-p
    :initform nil
    :accessor rted-invalidated-p
    :documentation "Marks whether the current decision might have been preempted by a contingent event")
   (history
    :initform (make-hash-table :test #'equal)
    :accessor history-of
    :documentation "Maps events to the times at which they were dispatched to the driver")
   (verbose
    :initarg :verbose
    :initform nil
    :accessor verbosep
    :documentation "Display a lot of debug info"))
  (:documentation
   "An online dispatcher that sends events to a driver to be executed and updates a schedule as
    contingent events are observed. Assume the driver is the thing that's actually capable of
    sending instructions to an agent to be executed immediately. This dispatcher can differentiate
    between normal events and noop events. Noop events won't be sent to the driver, and will instead
    simply result in an updated schedule."))

(defmethod initialize-instance :after ((dispatcher dynamic-dispatcher) &key (plan nil))
  (when (not (null plan)) (init-scheduler dispatcher plan)))

(defmethod init-scheduler ((dispatcher dynamic-dispatcher) plan)
  (setf (slot-value dispatcher '%scheduler)
        (make-instance 'sched:dynamic-scheduler :state-plan plan
                                                :reschedule-early-ctg-p (reschedulep dispatcher)
                                                :verbose (verbosep dispatcher))))

(defmethod observe-event! ((dispatcher dynamic-dispatcher) event-id)
  "A exogenous event has been seen and we need to update the schedule accordingly. Mark the current
   RTED invalidated"
  (let* ((scheduler (scheduler-of dispatcher))
         (clock (clock-of dispatcher))
         (elapsed-time (time:timestamp-difference (exec:clock-timestamp clock)
                                                  (start-timestamp-of dispatcher))))

    (when (sched:update-schedule! scheduler event-id elapsed-time)
      ;; force the dispatcher to ask for a new decision now that the schedule has been updated
      (setf (rted-invalidated-p dispatcher) t))))

(defstruct dispatch-status
  message
  next-rted)

(defmethod run-dispatcher ((dispatcher dynamic-dispatcher))
  "Ask the scheduler which events should be sent to the driver to be executed"
  (let* ((clock (clock-of dispatcher))
         ;; allocate a status object outside the dispatch loop
         (start-stamp (exec:clock-timestamp clock))
         (status (make-dispatch-status)))

    (setf (start-timestamp-of dispatcher) start-stamp)

    ;; run until `tick' says we don't have any more events to dispatch
    ;; TODO is this going to block other calls? eg. observe-event!?
    (loop :while (tick dispatcher status))))

(defmethod tick ((dispatcher dynamic-dispatcher) status)
  "The logic for deciding what to dispatch next. Use a mailbox to communicate with a driver. The
   procedure is as follows:

    1. See if we have a message from the driver confirming execution. If so, update the schedule
    2. Get the next decision. Make sure it's not too early and check we're not just waiting for
       the driver to tell us these events have been executed
    3. When we're within epsilon of the event's scheduled time, either:
        a. If a normal event, send it to the driver to execute with a callback with instructions
           for sending a message confirming execution
        b. If a noop event, immediately update the schedule and unset the decision. Skip the driver

   NOTE: time: we track times in absolute (clock) times, while we send times relative to the start
   event to the scheduler.

   Returns can-continue-p"
  (let* ((driver (driver-of dispatcher))
         (scheduler (scheduler-of dispatcher))
         (clock (clock-of dispatcher))
         (start-stamp (start-timestamp-of dispatcher)))

    (unless (sched:has-unexecuted-events-p scheduler)
      ;; nothing left to do!
      (print-when-verbose dispatcher "DONE!~%")
      (return-from tick nil))

    ;;; Step 1 - updating schedule after execution

    ;; check the mailbox for messages from the driver
    (let ((new-message (safe-queue:mailbox-receive-message (mailbox-of dispatcher) :timeout 1f-2)))
      (when (not (null new-message))
        ;; keep track of the message
        (setf (dispatch-status-message status) new-message)

        ;; update the schedule accordingly
        (destructuring-bind (message-type &rest args) new-message
          (ecase message-type
            (:executed
             (loop :for (event-ids execution-time) :in args
                   :do (loop :for event-id :in event-ids
                             :do
                                ;; FYI `execution-time' is a timestamp. the scheduler uses time
                                ;; relative to the start event, hence `elapsed-time'
                                (let ((elapsed-time (time:timestamp-difference execution-time start-stamp)))
                                  (multiple-value-bind (recorded-p already-executed too-early too-late)
                                      (sched:update-schedule! scheduler event-id elapsed-time)
                                    (when (not recorded-p)
                                      (print-when-verbose dispatcher "~A Failed: ~A~%" event-id
                                                          (cond (already-executed "Already executed")
                                                                (too-early "Too early")
                                                                (too-late "Too late"))))))))

             ;; invalidate next decision (if one exists) after updating the schedule
             (setf (dispatch-status-next-rted status) nil))))))

    ;; a contingent event came in. we are forced to get a new decision
    (when (rted-invalidated-p dispatcher)
      (setf (dispatch-status-next-rted status) nil)
      (setf (rted-invalidated-p dispatcher) nil))

    ;; Step 2 - getting the next decision

    ;; get the next decision if we don't have one
    (when (null (dispatch-status-next-rted status))
      (setf (dispatch-status-next-rted status) (sched:get-next-rted scheduler)))

    ;; double check we haven't already tried to dispatch this decision
    (when (reduce #'(lambda (prev event-noop) (and prev (gethash event-noop (history-of dispatcher))))
                  (sched:rted-events (dispatch-status-next-rted status))
                  :initial-value t)

      ;; we need to wait for the driver to let us know these events have been executed. they should
      ;; (hopefully) be showing up in the messages momentarily
      (print-when-verbose dispatcher "We already tried to dispatch ~A~%" (dispatch-status-next-rted status))
      (return-from tick t))

    ;; try to send the decision to the driver.
    (let ((elapsed-time (time:timestamp-difference (exec:clock-timestamp clock)
                                                   start-stamp))
          ;; `event-time' is relative to the start event
          (event-time (sched:rted-time (dispatch-status-next-rted status)))
          (event-noops (sched:rted-events (dispatch-status-next-rted status))))

      (unless (<= (- event-time elapsed-time) (epsilon-of dispatcher))

        ;; it's too early to dispatch the next decision, so run this loop again and hopefully enough
        ;; time passes
        (print-when-verbose dispatcher "Too early to dispatch ~A~%" event-noops)
        (return-from tick t))

      ;;; Step 3 - initiating execution

      (print-when-verbose dispatcher "Dispatching ~A at ~A~%" event-noops elapsed-time)

      ;; separate events that actually need to be executed vs noops
      (let* (real-events
             noop-events)
        (loop :for (event-id noop) :in event-noops
              :if noop
                :do (setf noop-events (cons event-id real-events))
              :else
                :do (setf real-events (cons event-id noop-events)))

        ;; 3.a) Send the real events to the driver
        (execute-now! driver (make-instance 'batch-dispatch-events :events real-events))

        ;; 3.b) Update the schedule with the noop events at the exact event time
        (loop :for event-id :in noop-events
              :do (sched:update-schedule! scheduler event-id event-time))

        ;; track when events were dispatched
        (mapc (lambda (e) (setf (gethash e (history-of dispatcher)) elapsed-time))
              (nconc real-events noop-events))

        ;; events were definitely dispatched. return t to indicate we can continue
        t))))

(defmethod print-when-verbose ((dispatcher dynamic-dispatcher) &rest message)
  (when (verbosep dispatcher)
    (apply 'format t message)))
