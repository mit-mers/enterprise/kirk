(in-package #:kirk-v2/dispatcher)

(defclass dispatch-driver ()
  ((%clock
    :initarg :clock
    :reader clock-of)
   (%mailbox
    :initarg :mailbox
    :initform (safe-queue:make-mailbox)
    :accessor mailbox-of))
  (:documentation
   "A DISPATCH-DRIVER is used to actually ensure events are actually performed. The implementation of
    driver should only include the logic necessary to perform an action immediately. The dispatcher
    is responsible for deciding /when/ an event should be executed - a driver is solely responsible
    for performing the action. It should specialize `EXECUTE-NOW!' with the logic required to ensure
    the event is performed in the real-world. It should share a clock and a means to
    communicate (eg. a mailbox) with the dispatcher"))

(defgeneric confirm-execution! (dispatch-driver events)
  (:method ((driver dispatch-driver) events)
    (let* ((clock (clock-of driver))
           (mailbox (mailbox-of driver))
           (time (exec:clock-timestamp clock))
           (message (list (list events time))))
      (queue:mailbox-send-message mailbox (list* :executed message))))
  (:documentation
   "Inform the dispatcher about when an event was successfully performed/executed. The implementation
    of the driver is responsible for calling this method, which should probably happen somewhere
    towards the end of `execute-now!'"))

(defgeneric execute-now! (driver item)
  (:method ((driver dispatch-driver) item)
    (confirm-execution! driver (events-of item)))
  (:documentation
   "Perform an action immediately using DRIVER. The ITEM describes the activities and/or events to be
    executed. Should call `confirm-execution!' after execution. The default method is a noop that
    simply calls `confirm-execution!' at the time of execution."))

(defclass dispatch-driver-item ()
  ()
  (:documentation
   "Class for any object that can be dispatched using DISPATCH!."))


;;; NOTE This method is deprecated!

(defgeneric dispatch! (driver item time thunk)
  (:documentation "Dispatch something using DRIVER. The ITEM describes the
activities and/or events to be dispatched. TIME is the nominal time at which
execution should happen. THUNK is a function that is used to communicate to the
dispatcher that something has actually been executed. It is allowed to have
only keyword arguments and they must be defaulted such that calling THUNK with
no arguments indicates everything ITEM indicates should be dispatched has been
executed at the current time."))
