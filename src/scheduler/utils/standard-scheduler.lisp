(in-package #:%kirk-v2/scheduler/utils)

(defclass standard-scheduler (sched:scheduler)
  (;; Provided by the user
   (%state-plan
    :initarg :state-plan
    :reader state-plan-of
    :documentation
    "The state plan being scheduled.")
   (%logger
    :initarg :logger
    :initform (log4cl:make-logger)
    :reader logger)
   (%image-directory
    :initarg :image-directory
    :initform "/tmp/test-out/"
    :reader image-directory)

   ;; Lazily computed by us.
   (%d-graph
    :reader d-graph-of
    :documentation
    "The direct distance graph representation of STN.")
   (%reasoning-graph
    :accessor reasoning-graph-of
    :documentation
    "The graph the algorithm reasons over.")

   ;; Keeping track of state
   (%ready-p
    :accessor ready-p
    :initform nil
    :documentation
    "Boolean stating if the scheduler is ready.")
   (%png-id
    :initform -1
    :accessor png-id)
   (%layout-override
    :initform nil
    :accessor layout-override)))

(defmethod slot-unbound (class (scheduler standard-scheduler) (slot-name (eql '%d-graph)))
  (setf (slot-value scheduler '%d-graph)
        (scheduler-state-plan-to-d-graph scheduler (state-plan-of scheduler))))

(defgeneric scheduler-state-plan-to-d-graph (scheduler state-plan))

(defmethod scheduler-state-plan-to-d-graph ((scheduler standard-scheduler) state-plan)
  (make-distance-graph-from-state-plan state-plan))

(defmethod event-index ((scheduler standard-scheduler) event)
  (event-index (d-graph-of scheduler) event))

(defmethod sched:ensure-scheduler-ready :around ((scheduler standard-scheduler))
  (unless (ready-p scheduler)
    (multiple-value-prog1 (call-next-method)
      (maybe-save-graph scheduler :save-layout nil :graph (d-graph-of scheduler))
      (maybe-save-graph scheduler :save-layout t)
      (setf (ready-p scheduler) t))))

(defmethod sched:mark-events-executed :around ((scheduler standard-scheduler) event-or-events relative-time)
  (call-next-method scheduler
                    (mapcar (alex:curry #'event-index scheduler)
                            (alex:ensure-list event-or-events))
                    relative-time))

(defmethod sched:mark-events-executed :after ((scheduler standard-scheduler) event-or-events relative-time)
  (maybe-save-graph scheduler :new-thread t))

(defmacro log (scheduler category &rest args)
  (let ((log-fun (find-symbol (string category) :log)))
    `(,log-fun :logger (logger ,scheduler) ,@args)))

(defgeneric maybe-save-graph (scheduler &key save-layout new-thread
                                          graph))

(defmethod maybe-save-graph ((scheduler standard-scheduler)
                             &key save-layout new-thread
                               (graph (reasoning-graph-of scheduler)))
  ;; (alex:when-let ((log-directory (image-directory scheduler)))
  ;;   (let* ((id (incf (png-id scheduler)))
  ;;          (pn (merge-pathnames (format nil "~d.png" id)
  ;;                               log-directory))
  ;;          (graph graph))
  ;;     (flet ((doit ()
  ;;              ;; HACK: the Raster port doesn't like being called from multiple
  ;;              ;; threads. Asked on IRC for a better solution to this. Could
  ;;              ;; maybe use CLIM:WITH-PORT-LOCKED?? But I think that'd
  ;;              ;; serialize all the drawing, which I'd prefer not to do.
  ;;              (let ((climi::*all-ports* nil))
  ;;                (log scheduler :debug
  ;;                     "Writing png to ~a" pn)
  ;;                (ensure-directories-exist pn)
  ;;                (let (record)
  ;;                  (mcclim-raster-image:with-output-to-raster-image-file (s pn)
  ;;                    (setf record (draw-distance-graph s graph :layout-override (layout-override scheduler))))
  ;;                  (when save-layout
  ;;                    (setf (layout-override scheduler)
  ;;                          (mtk-mcclim-dot:make-layout-override record :include-edges-p t)))))))
  ;;       (if new-thread
  ;;           (progn
  ;;             (setf graph (copy-distance-graph graph))
  ;;             (bt:make-thread #'doit))
  ;;           (doit))))
  ;;   t)
  )


;;; Decisions

(defclass standard-dispatch (sched:dispatch)
  ((events
    :initarg :events
    :reader sched:dispatch-events)
   (earliest-time
    :initarg :earliest-time
    :reader sched:dispatch-earliest-time)
   (constrains-next-decision-within
    :initarg :constrains-next-decision-within
    :reader sched:dispatch-constrains-next-decision-within)))

(defmethod print-object ((dispatch standard-dispatch) stream)
  (print-unreadable-object (dispatch stream :type t :identity t)
    (format stream "EVENTS: ~A EARLIEST-TIME: ~A CONSTRAINS: ~A"
            (sched:dispatch-events dispatch)
            (sched:dispatch-earliest-time dispatch)
            (sched:dispatch-constrains-next-decision-within dispatch))))
