(in-package #:%kirk-v2/scheduler/utils)

(defclass edge-overlay ()
  ((%outgoing-edges
    :reader outgoing-edges-of)
   (%incoming-edges
    :reader incoming-edges-of))
  (:documentation
   "Graph edges."))

(defun copy-edge-overlay (edges)
  (let ((out (allocate-instance (find-class 'edge-overlay))))
    (flet ((copy-array (array)
             (let ((copy (make-array (length array))))
               (loop :for ht :across array
                     :for i :upfrom 0
                     :do (setf (aref copy i) (alex:copy-hash-table ht)))
               copy)))
      (setf (slot-value out '%outgoing-edges) (copy-array (slot-value edges '%outgoing-edges))
            (slot-value out '%incoming-edges) (copy-array (slot-value edges '%incoming-edges))))
    out))

(defmethod initialize-instance :after ((graph edge-overlay) &key number-of-nodes connectedp)
  (labels ((make-ht (index)
             (let ((ht (make-hash-table :size number-of-nodes)))
               (when connectedp
                 (dotimes (i number-of-nodes)
                   (unless (= i index)
                     (setf (gethash i ht) t))))
               ht))
           (make-hts ()
             (loop :repeat number-of-nodes
                   :for index :upfrom 0
                   :collect (make-ht index))))
    (setf (slot-value graph '%incoming-edges)
          (make-array number-of-nodes :initial-contents (make-hts)))
    (setf (slot-value graph '%outgoing-edges)
          (make-array number-of-nodes :initial-contents (make-hts)))))

(defmethod num-edges ((edges edge-overlay))
  (loop :for table :across (outgoing-edges-of edges)
        :summing (hash-table-count table)))

(defmethod add-edge! ((edges edge-overlay) from to)
  (setf (gethash to (aref (outgoing-edges-of edges) from)) t
        (gethash from (aref (incoming-edges-of edges) to)) t)
  (values))

(defmethod remove-edge! ((edges edge-overlay) from to)
  (remhash to (aref (outgoing-edges-of edges) from))
  (remhash from (aref (incoming-edges-of edges) to))
  (values))

(defun outgoing-edges (edges node)
  (alex:hash-table-keys (aref (outgoing-edges-of edges) node)))

(defun call-do-outgoing-edges (edges node thunk)
  (dolist (j (alex:hash-table-keys (aref (outgoing-edges-of edges) node)))
    (funcall thunk j)))

(defmacro do-outgoing-edges ((edges node node-binding) &body body)
  `(call-do-outgoing-edges ,edges ,node (lambda (,node-binding) ,@body)))

(defun incoming-edges (edges node)
  (alex:hash-table-keys (aref (incoming-edges-of edges) node)))

(defun call-do-incoming-edges (edges node thunk)
  (dolist (j (alex:hash-table-keys (aref (incoming-edges-of edges) node)))
    (funcall thunk j)))

(defmacro do-incoming-edges ((edges node node-binding) &body body)
  `(call-do-incoming-edges ,edges ,node (lambda (,node-binding) ,@body)))
