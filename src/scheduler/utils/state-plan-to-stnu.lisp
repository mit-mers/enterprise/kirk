(in-package #:%kirk-v2/scheduler/utils)

;; we're adapting some OpSAT code for going from constraints to temporal-networks
;; see https://git.mers.csail.mit.edu/mars-toolkit/opsat-v3/-/blob/master/src/subsolvers/temporal-networks/stnu.lisp
(defun state-plan-to-stnu (state-plan)
  "Extract a temporal-networks:stnu from an odo:state-plan. Assume the plan is already known to be
   controllable."
  (let* ((network (tn:make-simple-temporal-network-with-uncertainty (odo:name state-plan)))
         (stcs (odo:constraint-list state-plan))
         (episodes (odo:goal-episode-list state-plan))
         (uncontrollable-events (make-hash-table :test #'equal)))

    ;; convert and add STC constraints into a temporal network.
    (iter (for stc in stcs)
          (for contingent-p = (typep (odo:type stc)
                                     'odo:contingent-constraint))
          (let ((res (op:process-constraint stc network nil
                                            :contingent-p contingent-p
                                            :uncontrollable-events uncontrollable-events)))
            (when (eql res :UNCONTROLLABLE)
              (error "scheduler received uncontrollable state plan"))))

    ;; pull STC constraints from episodes and add them into the same temporal network.
    (iter (for episode in episodes)
          (for temporal-constraint = (odo:temporal-constraint episode))
          ;; TODO I'm sure this can be simplified. look up how to use `for' with `typep'
          (when temporal-constraint
            (let* ((stc (odo:make-constraint temporal-constraint
                                             :contingent-p (odo:contingent-p episode)
                                             :name (odo:name episode)))
                   (contingent-p (typep (odo:type stc)
                                        'odo:contingent-constraint))
                   (res (op:process-constraint stc network nil
                                              :contingent-p contingent-p
                                              :uncontrollable-events uncontrollable-events)))
              (if (eql res :UNCONTROLLABLE)
                (error "scheduler received uncontrollable state plan")))))
    network))
