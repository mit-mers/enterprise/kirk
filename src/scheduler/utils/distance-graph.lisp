(in-package #:%kirk-v2/scheduler/utils)

(defclass distance-graph ()
  ((%d
    :accessor d-of)
   (%edges
    :accessor edges-of)
   (%index-to-event-array
    :accessor index-to-event-array-of)
   (%event-to-index-ht
    :accessor event-to-index-ht-of)))

(defun copy-distance-graph (graph)
  (let ((out (allocate-instance (find-class 'distance-graph))))
    (setf (slot-value out '%d) (alex:copy-array (slot-value graph '%d))
          (slot-value out '%edges) (copy-edge-overlay (slot-value graph '%edges))
          (slot-value out '%index-to-event-array) (alex:copy-array (slot-value graph '%index-to-event-array))
          (slot-value out '%event-to-index-ht) (alex:copy-hash-table (slot-value graph '%event-to-index-ht)))
    out))

(defmethod event-index ((graph distance-graph) event)
  (gethash event (event-to-index-ht-of graph)))

(defmethod index-event ((graph distance-graph) index)
  (aref (index-to-event-array-of graph) index))

(defmethod initialize-instance :after ((graph distance-graph) &key n connectedp)
  (setf (d-of graph) (make-array (list n n) :element-type 'double-float
                                            :initial-element +double-inf+)
        (edges-of graph) (make-instance 'edge-overlay
                                        :number-of-nodes n
                                        :connectedp connectedp)
        (index-to-event-array-of graph) (make-array n :initial-element nil)
        (event-to-index-ht-of graph) (make-hash-table :test 'eq :size n)))

(defun ensure-double (thing)
  (cond
    ((eql thing :infinity)
     +double-inf+)
    ((eql thing :-infinity)
     +double-negative-inf+)
    (t
     (coerce thing 'double-float))))

(defun make-distance-graph-from-state-plan (state-plan &key augmentedp)
  ;; TODO is this where we need to compile a fixed delay stnu to vanilla stnu?
  (let* ((events (odo:state-plan-event-list state-plan))
         (n (if augmentedp (1+ (length events)) (length events)))
         (graph (make-instance 'distance-graph :n n))
         (start-event (odo:start-event state-plan))
         (now-event (odo:make-event :name '#:now))
         (index-to-event-array (index-to-event-array-of graph))
         (event-to-index-ht (event-to-index-ht-of graph))
         (edges (edges-of graph))
         (i -1))
    ;; Make the start event index zero. It just makes everyone's life
    ;; easier. Also make NOW index one.
    ;;
    ;; TODO: Make this more efficient by using the iterator.
    (dolist (event (append (if augmentedp
                               (list start-event now-event)
                               (list start-event))
                           (remove start-event events)))
      (let ((idx (incf i)))
        (setf (gethash event event-to-index-ht) idx
              (aref index-to-event-array idx) event)))

    (when augmentedp
      (loop
        :for j :upfrom 2 :below n
        :do (setf (distance graph j 1) 0d0)
            (add-edge! edges j 1)))

    ;; There shouldn't be any conditional constraints or episodes by the time
    ;; we go to dispatch.
    (dolist (c (odo:constraint-list state-plan))
      (let ((expr (odo:constraint-expression c)))
        ;; when the predicate of the constraint is simple-temporal...
        (when (and (odo:application-expression-p expr)
                   (eql (odo:operator expr) 'odo:simple-temporal))
          (let ((from (gethash (odo:application-argument-value expr 'odo:from) event-to-index-ht))
                (to (gethash (odo:application-argument-value expr 'odo:to) event-to-index-ht))
                (lower-bound (ensure-double (odo:application-argument-value expr 'odo:lower-bound)))
                (upper-bound (ensure-double (odo:application-argument-value expr 'odo:upper-bound))))
            (alex:minf (distance graph from to) upper-bound)
            (alex:minf (distance graph to from) (- lower-bound))
            (add-edge! edges from to)
            (add-edge! edges to from)))))

    ;; CHECK: We go through the goal episodes only. This is fine when using
    ;; RMPL as input (as every goal has a corresponding value), but may not be
    ;; fine in the general case.
    (dolist (ep (odo:goal-episode-list state-plan))
      (let* ((tc (odo:episode-temporal-constraint ep))
             (from (gethash (odo:episode-start-event ep) event-to-index-ht))
             (to (gethash (odo:episode-end-event ep) event-to-index-ht)))
        (add-edge! edges from to)
        (add-edge! edges to from)
        (if tc
            (let* ((expr (odo:constraint-expression tc))
                   (lower-bound (ensure-double (odo:application-argument-value expr 'odo:lower-bound)))
                   (upper-bound (ensure-double (odo:application-argument-value expr 'odo:upper-bound))))
              (alex:minf (distance graph from to) upper-bound)
              (alex:minf (distance graph to from) (- lower-bound)))
            (alex:minf (distance graph to from) 0d0))))
    graph))

(defmethod number-of-nodes ((graph distance-graph))
  (array-dimension (d-of graph) 0))

(defmethod distance ((graph distance-graph) i j)
  (aref (d-of graph) i j))

(defmethod (setf distance) (new-value (graph distance-graph) i j)
  (setf (aref (d-of graph) i j) new-value))

(defmethod remove-edge! ((graph distance-graph) i j)
  (remove-edge! (edges-of graph) i j)
  (setf (aref (d-of graph) i j) +double-inf+)
  (values))

(defmethod apsp-graph ((graph distance-graph))
  (let* ((out (copy-distance-graph graph))
         (edges (edges-of out)))
    (dotimes (i (number-of-nodes out))
      (dotimes (j (number-of-nodes out))
        (unless (= i j)
          (add-edge! edges i j))))
    (floyd-warshall! (d-of out))
    out))

(defmacro do-graph-outgoing-edges ((graph node binding d &optional result) &body body)
  (let ((%graph (gensym "GRAPH"))
        (%node (gensym "NODE")))
    `(let ((,%graph ,graph)
           (,%node ,node))
       (do-outgoing-edges ((edges-of ,%graph) ,%node ,binding)
         (let ((,d (distance ,%graph ,%node ,binding)))
           ,@body))
       ,result)))

(defmacro do-graph-incoming-edges ((graph node binding d &optional result) &body body)
  (let ((%graph (gensym "GRAPH"))
        (%node (gensym "NODE")))
    `(let ((,%graph ,graph)
           (,%node ,node))
       (do-incoming-edges ((edges-of ,%graph) ,%node ,binding)
         (let ((,d (distance ,%graph ,binding ,%node)))
           ,@body))
       ,result)))

(defun i-before-j-p (po i j)
  (plusp (aref po i j)))

(defun extract-partial-order (d)
  (let ((before (make-array (array-dimensions d)
                            :element-type 'bit
                            :initial-element 0)))
    (dotimes (i (array-dimension d 0))
      (dotimes (j (array-dimension d 0))
        (unless (= i j)
          ;; if i <- j is negative, i precedes j
          (when (minusp (aref d j i))
            (setf (aref before i j) 1)))))
    before))
