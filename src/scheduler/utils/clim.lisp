(in-package #:%kirk-v2/scheduler/utils)

(defun draw-distance-graph (stream graph &key layout-override)
  (let ((dim (number-of-nodes graph)))
    (clim:format-graph-from-roots
     (alex:iota dim)
     (lambda (object stream)
       (clim:surrounding-output-with-border (stream :shape :rounded)
         (princ object stream)))
     (lambda (object)
       (let ((out ()))
         (do-graph-outgoing-edges (graph object next _ out)
           (declare (ignore _))
           (push next out))))
     :arc-drawer (make-instance 'mtk-mcclim-dot:dot-arc-drawer
                                :edge-label-printer (lambda (drawer stream from to)
                                                      (declare (ignore drawer))
                                                      (let ((distance (distance graph from to)))
                                                        (when (float-features:float-infinity-p distance)
                                                          (setf distance
                                                                (if (minusp distance)
                                                                    "-∞"
                                                                    "∞")))
                                                        (princ distance stream))))
     :stream stream
     :orientation :horizontal
     :merge-duplicates t
     :graph-type :dot-digraph
     :layout-override layout-override)))
