(in-package #:%kirk-v2/scheduler/utils)

(defgeneric event-index (thing event))

(defgeneric index-event (thing index))
