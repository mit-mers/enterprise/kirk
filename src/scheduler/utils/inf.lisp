(in-package #:%kirk-v2/scheduler/utils)

(defconstant +double-inf+ float-features:double-float-positive-infinity)
(defconstant +double-negative-inf+ float-features:double-float-negative-infinity)
