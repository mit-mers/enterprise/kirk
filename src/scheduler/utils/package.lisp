(uiop:define-package #:%kirk-v2/scheduler/utils
  (:use #:cl
        #:anaphora
        #:iterate)
  (:shadow #:log)
  (:local-nicknames (#:sched #:kirk-v2/scheduler)
                    (#:alex #:alexandria)
                    (#:tn #:temporal-networks)
                    (#:op #:opsat-v3/subsolvers/temporal-networks/common))
  (:export #:+double-inf+
           #:+double-negative-inf+
           #:coalesce-d-graph
           #:coalescing-scheduler
           #:d-graph-of
           #:distance
           #:do-graph-incoming-edges
           #:do-graph-outgoing-edges
           #:do-incoming-edges
           #:do-outgoing-edges
           #:edge-overlay
           #:event-index
           #:extract-partial-order
           #:floyd-warshall!
           #:i-before-j-p
           #:index-event
           #:log
           #:make-distance-graph-from-state-plan
           #:num-edges
           #:number-of-nodes
           #:reasoning-graph-of
           #:remove-edge!
           #:scheduler-state-plan-to-stn
           #:standard-dispatch
           #:standard-scheduler
           #:state-plan-of
           #:state-plan-to-stnu
           #:stn-distance-graph-of
           #:stn-event-of-index
           #:stn-index-of-event
           #:stn-number-of-events
           #:stn-of
           #:stn-to-d-graph))

(in-package #:%kirk-v2/scheduler/utils)
