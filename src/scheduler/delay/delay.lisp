(in-package #:%kirk-v2/scheduler/delay)

(defclass sched:delay-scheduler (sched:minimum-dispatchable-form-scheduler)
  ())

;; (defmethod sched:ensure-scheduler-ready ((scheduler sched:delay-mdf-scheduler))
;;   "Compute a distance graph and its minimum dispatchable form from the state plan."
;;   (let* ((state-plan (utils:state-plan-of scheduler))
;;          (graph (utils::apsp-graph (utils:d-graph-of scheduler)))
;;          (number-of-events (utils:number-of-nodes graph))
;;          (ubs (make-array number-of-events
;;                           :element-type 'double-float
;;                           :initial-element utils:+double-inf+))
;;          (lbs (make-array number-of-events
;;                           :element-type 'double-float
;;                           :initial-element 0d0))
;;          (start-event-index (utils:event-index scheduler (odo:state-plan-start-event state-plan))))
;;     (compute-mdf! graph)
;;     (setf (utils:reasoning-graph-of scheduler) graph)

;;     (setf (aref lbs start-event-index) 0d0
;;           (scheduler-time-window-ubs scheduler) ubs
;;           (scheduler-time-window-lbs scheduler) lbs)

;;     ;; Next, compute the enabled events.
;;     (dotimes (i number-of-events)
;;       (setf (gethash i (scheduler-unexecuted-events scheduler)) t)
;;       (let (blocking-events)
;;         (utils:do-graph-outgoing-edges (graph i j d)
;;           (when (or (minusp d)
;;                     (and (treat-loose-precedence-as-strong-p scheduler)
;;                          (zerop d)
;;                          (plusp (utils:distance graph j i))))
;;             (push j blocking-events)))
;;         (if blocking-events
;;             (setf (gethash i (scheduler-blocking-events scheduler)) blocking-events)
;;             (setf (gethash i (scheduler-enabled-events scheduler)) t))))
;;     ;; Something has gone horribly wrong if the start event isn't enabled.
;;     (assert (gethash start-event-index (scheduler-enabled-events scheduler))))
;;   scheduler)

;; TODO when we first instantiate the scheduler, convert to fixed delay (exported from t-c) and then to vanilla
;; TODO we need to go from state-plan with `gamma' slots to the t-n representation
;; see (make-distance-graph-from-state-plan) from distance graph

;; (defmethod scheduler-state-plan-to-d-graph ((scheduler delay-mdf-scheduler) state-plan)
;;   (make-distance-graph-from-state-plan (convert-to-vanilla-stnu state-plan)))

;; (defun convert-to-vanilla-stnu-state-plan (state-plan)
;;   "Convert a state-plan with a variable-delay STNU and return a state-plan with
;;    its equivalent vanilla STNU. To do so, we go from variable-delay to
;;    fixed-delay (Bhargava, Pittman, Williams 2022) and then to a vanilla STNU.
;;    The latter conversion takes two steps: (1) merge finite delays into
;;    contingent links, (2) replace contingent links that have infinite delay with
;;    equivalent requirement episodes"
;;   ;; (let* ((episodes (odo:goal-episode-list state-plan))
;;   ;;        (vanilla-stnu-state-plan (odo:make-state-plan)))
;;   ;;   (print episodes))
;;   (dolist (episode (odo:state-plan-goal-episode-list state-plan)))
;;   )

;; (defun vdc-to-fixed-delay-state-plan (vdc-state-plan)
;;   (let* ((fixed-delay-state-plan (odo:make-state-plan))
;;          ())))

;; (defun fixed-delay-to-vanilla-state-plan (fixed-delay-state-plan)
;;   ())

;; TODO probably implement a delay planner too? just to keep VDC and DC separate? mebbe?

;; (defun sched:vdc-to-fixed (stnu)
;;   (tc::convert-variable-to-fixed-delay stnu))
