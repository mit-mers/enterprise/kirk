(in-package #:kirk-v2/scheduler)

(doc:defsection @scheduler (:title "Scheduler")
  "A scheduler takes some temporally flexible plan as static input as well as
execution times of events as dynamic input. As dynamic output, it provides a
set of decisions that a dynamic execution algorithm can perform."
  (@scheduler-api section))

(doc:defsection @scheduler-api (:title "Scheduler API")
  "This section describes the common interface among schedulers.

Every scheduler must inherit from the [`scheduler`] class. Additionally,
it must accept the `:state-plan` initarg, specifying a state plan that is to be
scheduled.

A scheduler may perform some pre-compilation on the plan to turn it into a form
that is easier to reason over. Schedulers may perform this pre-compilation
lazily, however [`ensure-scheduler-ready`] can be used to force any
pre-compilation to happen at a known time.

The scheduler maintains a partial schedule (what events have already been
executed and when). This partial schedule is updated using
[`mark-events-executed`].

The scheduler can be queried with the following functions:

- [`has-unexecuted-events-p`]
- [`active-decisions`]

Each decision is a subclass of [`execution-decision`].

The [`dispatch`] decision states that the events in [`dispatch-events`] must be
executed simulatenously some time after [`dispatch-earliest-time`]."
  (scheduler class)
  (ensure-scheduler-ready generic-function)
  (mark-events-executed generic-function)
  (has-unexecuted-events-p generic-function)
  (active-decisions generic-function)

  (execution-decision class)
  (dispatch class)
  (dispatch-events generic-function)
  (dispatch-earliest-time generic-function)
  (dispatch-constrains-next-decision-within generic-function)

  (scheduler-error condition)
  (scheduler (reader scheduler-error))
  (simple-scheduler-error condition)
  (infeasible-execution-time condition)
  (infeasible-execution-time-event (reader infeasible-execution-time))
  (infeasible-execution-time-time (reader infeasible-execution-time)))
