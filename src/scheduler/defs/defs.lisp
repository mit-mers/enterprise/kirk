(in-package #:%kirk-v2/scheduler/defs)

(define-condition sched:scheduler-error (error)
  ((sched:scheduler
    :initarg :scheduler
    :reader sched:scheduler
    :documentation
    "The scheduler that produced the error."))
  (:documentation
   "The base error for any error that happens during scheduling."))

(define-condition sched:simple-scheduler-error (sched:scheduler-error simple-error)
  ()
  (:documentation
   "A catchall error."))

(define-condition sched:infeasible-execution-time (sched:scheduler-error)
  ((event
    :initarg :event
    :reader sched:infeasible-execution-time-event
    :documentation
    "The event being executed")
   (time
    :initarg :time
    :reader sched:infeasible-execution-time-time
    :documentation
    "The time at which the event is being executed."))
  (:documentation
   "Error signaled when an event has been executed outside of its time window,
making the schedule infeasible.")
  (:report (lambda (condition stream)
             (format stream "Executing event ~A at time ~A results in infeasibility in ~A."
                     (sched:infeasible-execution-time-event condition)
                     (sched:infeasible-execution-time-time condition)
                     (sched:scheduler condition)))))

;; TODO add a delay controllability scheduler here!
;;
;;scheduler tells dispatcher what's dispatchable, and the dispatcher decides what to do

(defclass sched:scheduler ()
  ()
  (:documentation
   "The scheduler protocol class."))

(defgeneric sched:ensure-scheduler-ready (scheduler)
  (:documentation
   "Ensures that any prep work has been completed. Does not need to be called
explicitly. The SCHEDULER must ensure this is called if needed by any other
calls."))

(defgeneric sched:has-unexecuted-events-p (scheduler)
  (:documentation
   "Returns non-NIL if there are unexecuted events."))

;; dispatcher calls this

(defgeneric sched:mark-events-executed (scheduler event-or-events time)
  (:documentation
   "Mark EVENT-OR-EVENTS as executed, at TIME.

May signal an INFEASIBLE-EXECUTION-TIME error."))

;; LOOK here
;; list of decisions
;; deadline for making decisions

(defgeneric sched:active-decisions (scheduler)
  (:documentation
   "Returns two VALUES. The first is a list of EXECUTION-DECISION objects. The
second is the time by which a decision must be chosen and acted upon, or NIL.

The only guarantee required is that if a dispatcher chooses a *single*
EXECUTION-DECISION randomly, executes it correctly, and records its execution
time with ENSURE-SCHEDULER-READY, then that process will result in a correct
execution. Schedulers are free to give further guarantees."))

(defclass sched:execution-decision ()
  ())

;; dispatch a series of events simultaneously

(defclass sched:dispatch (sched:execution-decision)
  ())

(defgeneric sched:dispatch-events (sched:dispatch)
  (:documentation
   "Return the list of events that should be simultaneously executed for the
DISPATCH decision."))

(defgeneric sched:dispatch-earliest-time (sched:dispatch)
  (:documentation
   "Return the earliest time at which DISPATCH-EVENTS can be executed."))

(defgeneric sched:dispatch-constrains-next-decision-within (sched:dispatch)
  (:documentation
   "Returns a REAL dt or NIL. If all events in this decision are executed at
time t0, then the second return value of ACTIVE-DECISIONS will be no greater
than t0 + dt.

This gives you a decent indication of how much slack a certain decision has
with respect to latencies and processing speed of `mark-events-executed`.

For instance, consider the following nodes in a d-graph:

```
=---0.1---=
|         |
|         v
a         b
^         |
|         |
=--- 4----=
```

Note that both a and b are eligible for execution (neither has an outgoing
negative edge). However, if a is executed first then b must be executed within
one tenth of a second. If b is executed first then a must be executed within 4
seconds. When operating on slow systems or with high communications latency, it
would be preferable to execute b first, as you may not even get confirmation
that a has been executed before the time at which b needs to be executed."))
