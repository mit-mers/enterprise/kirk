(uiop:define-package #:%kirk-v2/scheduler/defs
  (:use #:cl)
  (:local-nicknames (#:doc #:40ants-doc)
                    (#:sched #:kirk-v2/scheduler)))

(in-package #:%kirk-v2/scheduler/defs)
