(in-package #:%kirk-v2/scheduler/dynamic)

;;;; This implementation of a dynamic scheduler largely follows Nikhil's work with a delay
;;;; scheduler. It allows variable observation delay as well.
;;;; https://git.mers.csail.mit.edu/nkb/delay-executive

(defparameter +ZERO-POINT+ "++ZERO-POINT++"
  "Event ID to use for the zero point")

(defclass sched:dynamic-scheduler ()
  ((state-plan
    :initarg :state-plan
    :initform nil
    :accessor input-state-plan-of
    :documentation "Odo state plan given to scheduler from the dispatcher")
   (input-stnu
    :initform nil
    :accessor vdc-stnu-of
    :documentation "Input STNU with variable observation delay")
   (stnu
    :initform nil
    :documentation "STNU with fixed observation delay that's being executed")
   (gamma
    :initform (make-hash-table :test #'equal)
    :documentation "Gamma function. Fixed observation delay by event-id")

   (distances
    :initform nil
    :accessor distances-of
    :documentation "Unlabeled distance graph as a hash table in the form of
                    { `(temporal-event temporal-event): number }")
   (events
    :initform nil
    :documentation "Hash table of all temporal events")
   (edges
    :initform nil
    :documentation "List of all d-edges")
   (executed-events
    :initform nil
    :accessor executed-events-of
    :documentation "Hash table of the temporal events executed so far")
   (zero-point
    :initform nil
    :documentation "Zero point of the distance graph")
   (zero-points
    :initform nil)

   (reschedule-early-ctg-p
    :initarg :reschedule-early-ctg-p
    :initform nil
    :reader reschedulep
    :documentation
    "Allow rescheduling when a ctg event arrives before its lower bound. This is a situation that can
     happen when events with variable observation delay in the original STNU get tighter bounds in
     the fixed-delay STNU that is actually dispatched. If nil, early ctg events will be buffered to
     their fixed-delay equivalent lower bounds")
   (verbose
    :initarg :verbose
    :initform nil
    :accessor verbosep
    :documentation "Display a lot of debug info"))

  (:documentation
   "Dynamically schedules events for online execution. Allows variable observation delay in the STNU"))

(defmethod initialize-instance :after ((scheduler sched:dynamic-scheduler) &key)
  "Initialize the scheduler's slots if possible"
  (let ((state-plan (input-state-plan-of scheduler)))
    (unless state-plan
      (print-when-verbose scheduler "Scheduler is missing an input state-plan. Cannot continue scheduling~%")
      (return-from initialize-instance))

    (let* ((variable-delay-stnu (utils:state-plan-to-stnu state-plan))
           (fixed-delay-stnu (tc:convert-variable-to-fixed-delay variable-delay-stnu)))

      (setf (slot-value scheduler 'stnu) fixed-delay-stnu)
      (setf (vdc-stnu-of scheduler) variable-delay-stnu)

      (when (verbosep scheduler)
        (format t "~%VARIABLE-DELAY STNU:~%")
        (tn:do-constraints (constraint variable-delay-stnu)
          (format t "~A ~A ~A  [~A, ~A] ~A~%"
                  (tn:name (tn:from-event constraint))
                  (if (tn:controllable? (tn:to-event constraint)) "->" "=>")
                  (tn:name (tn:to-event constraint))
                  (tn:lower-bound constraint)
                  (tn:upper-bound constraint)
                  (if (tn:controllable? (tn:to-event constraint))
                      " "
                      (format nil "γ=[~A,~A]" (tn:min-observation-delay constraint) (tn:max-observation-delay constraint)))))

        (format t "~%FIXED-DELAY STNU:~%")
        (tn:do-constraints (constraint fixed-delay-stnu)
          (format t "~A ~A ~A  [~A, ~A] ~A~%"
                  (tn:name (tn:from-event constraint))
                  (if (tn:controllable? (tn:to-event constraint)) "->" "=>")
                  (tn:name (tn:to-event constraint))
                  (tn:lower-bound constraint)
                  (tn:upper-bound constraint)
                  (if (tn:controllable? (tn:to-event constraint))
                      " "
                      (format nil "γ=~A" (tn:min-observation-delay constraint))))))

      (multiple-value-bind (controllable-p conflict orig-rev-graph gamma full-rev-graph normalized-stn)
          ;; NOTE at this point in Kirk's lifecycle, we will have already run a DC check with opsat.
          ;; This implies (1) the QSP is already known to be DC, and (2) it would be great to grab the
          ;; d-graph from opsat when it returns DC instead of re-running the DC check here just to
          ;; generate the d-graph again
          (tc:delay-controllable? fixed-delay-stnu)
        (declare (ignore controllable-p conflict orig-rev-graph normalized-stn))

        (set-slots! scheduler fixed-delay-stnu full-rev-graph gamma)))))

(defmethod set-slots! ((scheduler sched:dynamic-scheduler) fixed-delay-stnu full-rev-graph gamma)
  "Prep for scheduling. This method needs to be called before any scheduling actually occurs"
  (let* ((allmax (loop :for end :being the hash-keys :in full-rev-graph
                         :using (hash-value edges)
                       :nconc (remove-if
                                 #'(lambda (x) (and (tc:d-edge-conditional? x)
                                                    (not (tc:d-edge-is-upper-case? x))))
                                 edges)))
         (events (get-events allmax))
         (executed-events (make-hash-table :test #'equal))
         (distances (get-distances allmax events))
         zero-points
         (zero-point (get-zero-points scheduler fixed-delay-stnu events allmax distances zero-points))
         (gamma-with-event-ids (rewrite-gamma gamma)))

    (setf (gethash (list +ZERO-POINT+ +ZERO-POINT+) distances) 0)
    (setf (gethash +ZERO-POINT+ executed-events) t)
    (setf (slot-value scheduler 'gamma) gamma-with-event-ids)
    (setf (slot-value scheduler 'distances) distances)
    (setf (slot-value scheduler 'edges) allmax)
    (setf (slot-value scheduler 'events) events)
    (setf (slot-value scheduler 'executed-events) executed-events)
    (setf (slot-value scheduler 'zero-point) zero-point)
    (setf (slot-value scheduler 'zero-points) zero-points)

    (when (verbosep scheduler)
      (format t "~%Edges:~%")
      (tc:print-edge-set allmax)

      (format t "~%Distances~%")
      (loop :for pair :being the hash-keys :in distances :using (hash-value distance)
            do (format t "~A to ~A: ~A~%" (first pair) (second pair) distance))

      (format t "READY!~%"))))

(defun get-events (edges)
  (let ((events (make-hash-table :test #'equal)))
    (dolist (edge edges)
      (let ((start (tn:name (tc:d-edge-from-node edge)))
            (end (tn:name (tc:d-edge-to-node edge))))
        (setf (gethash start events) t)
        (setf (gethash end events) t)))
    events))

(defun get-distances (edges events)
  (let ((distances (make-hash-table :test #'equal)))
  ;; initialize distance graph and nodes from allmax
    (dolist (edge edges)
      (let ((start (tn:name (tc:d-edge-from-node edge)))
            (end (tn:name (tc:d-edge-to-node edge))))
        (setf (gethash (list start end) distances) (tc:d-edge-weight edge))))

    ;; 0s on the diagonals
    (loop for n being the hash-keys in events do
      (setf (gethash (list n n) distances) 0))

    ;; triangle the rest of the distances
    (loop for n2 being the hash-keys in events do
      (loop for n1 being the hash-keys in events do
        (loop for n3 being the hash-keys in events do
          (let* ((dist-1-2-result (multiple-value-list (gethash (list n1 n2) distances)))
                 (dist-2-3-result (multiple-value-list (gethash (list n2 n3) distances)))
                 (dist-1-3-result (multiple-value-list (gethash (list n1 n3) distances))))
            (when (and (second dist-1-2-result)
                       (second dist-2-3-result)
                       (or (not (second dist-1-3-result))
                           (> (first dist-1-3-result)
                              (+ (first dist-1-2-result)
                                 (first dist-2-3-result)))))
              (setf (gethash (list n1 n3) distances)
                    (+ (first dist-1-2-result)
                       (first dist-2-3-result))))))))
    distances))

(defmethod get-zero-points ((scheduler sched:dynamic-scheduler) network events edges distances zero-points)
  "Determine zero point and zero points"
  (let ((zero-point (make-instance 'tn:temporal-event :network network :name +ZERO-POINT+)))
    (loop for start-id being the hash-keys in events do
      (let ((neg-link-found nil))
        (loop for end-id being the hash-keys in events do
          (let ((hash-result (multiple-value-list (gethash (list start-id end-id) distances))))
            (when (and (second hash-result) ; a distance was found
                       (< (first hash-result) 0))
              (setf neg-link-found t)
              (return))))
        (when (not neg-link-found)
          (let ((e2 (tc:create-edge-from-constraint :start (find-event scheduler start-id)
                                                    :end zero-point
                                                    :weight 0)))
            (setf edges (cons e2 edges)))
          (setf zero-points (cons start-id zero-points)))))

    (dolist (z zero-points)
      (loop for n being the hash-keys in events do
        (let ((zero-hash (multiple-value-list (gethash (list +ZERO-POINT+ n) distances)))
              (hash-result (multiple-value-list (gethash (list z n) distances))))
          (if (and (second hash-result)
                   (or (not (second zero-hash))
                       (> (first zero-hash) (first hash-result))))
              (setf (gethash (list +ZERO-POINT+ n) distances) (first hash-result))))
        ;; same as above but in the other direction
        (let ((zero-hash (multiple-value-list (gethash (list n +ZERO-POINT+) distances)))
              (hash-result (multiple-value-list (gethash (list n z) distances))))
          (when (and (second hash-result)
                     (or (not (second zero-hash))
                         (> (first zero-hash) (first hash-result))))
            (setf (gethash (list n +ZERO-POINT+) distances) (first hash-result))))))

    zero-point))

(defun rewrite-gamma (gamma)
  "Return a hash-table with event-id keys instead of tn:temporal-event keys"
  (let ((new-gamma (make-hash-table :test #'equal)))
    (maphash #'(lambda (event delay) (setf (gethash (tn:name event) new-gamma) delay))
             gamma)
    new-gamma))

(defmethod sched:get-next-rted ((scheduler sched:dynamic-scheduler))
  "Get the next event that should be scheduled. The types of events in decisions may be as follows:

    - an executable free event with a lower bound in the future
    - a noop free event representing the lower bound of a normalized contingent event. eg. the B' in
      A => B in [l, u] normalized to A -> B' => B where B' => B in [0, u - l]
    - a (noop) contingent event with an upper bound that needs to be imagined to guarantee
      variable-delay controllability

   Being contingent doesn't matter when we hand the decision to the dispatcher, but the dispatcher
   does need to know if it should actually try to execute the event vs. basically checking it off,
   hence the `noop' added to events in the return

   FYI: RTED == real-time execution decision, a 2-tuple of <time, events>

   Returns an RTED struct, else nil if no rted remains"

  (let ((enabled-events (get-enabled-events scheduler))
        (commitments (get-commitments scheduler)))
    (collect-rted enabled-events commitments)))

(defun collect-rted (enabled-events commitments)
  "Do the actual work of finding delay RTEDs. See get-next-rted for a description"

  ;; FYI the normalization routine in t-c creates very specific names for synthetic free events,
  ;; like `++LOWER-CTG-EVENT-NAME-REWOL++'
  (let ((normalized-lower-pattern (ppcre:create-scanner "^\\+\\+LOWER-.+-REWOL\\+\\+$"))
        (time utils:+double-inf+)
        events)

    (flet ((update-rted (event-contingent-pair)
             (let* ((event-id (first event-contingent-pair))
                    (contingent-p (second event-contingent-pair))
                    (bounds (gethash event-contingent-pair commitments))
                    (lower (first bounds))
                    (upper (second bounds))
                    ;; we schedule against the upper bounds of contingent events, lower bounds of
                    ;; free events
                    (event-time (if contingent-p upper lower))
                    ;; whether or not this a noop free event derived from the lower bound of a
                    ;; normalized contingent event
                    (normalized-lower-p (not (null (ppcre:scan normalized-lower-pattern event-id))))
                    (noop-p (or contingent-p normalized-lower-p))
                    (event-noop (list event-id noop-p)))

               (unless (or
                        ;; no decisions about later free events
                        (and (not contingent-p)
                             (<= lower time))
                        ;; no decisions about contingent events that could be observed before the
                        ;; next decision
                        (and contingent-p
                             (<= upper time)))
                 (return-from update-rted))

               ;; at this point, we know the event should be scheduled. determine if it comes before
               ;; any events that were already found in the loop. if so, clear out the old events,
               ;; update the time
               (when (< event-time time)
                 (setf time event-time)
                 (setf events nil))

               (setf events (append events (list event-noop))))))

      (loop :for event-contingent-pair :in enabled-events
            :do (update-rted event-contingent-pair)))

    ;; return an RTED
    (make-rted :time time :events events)))

(defmethod sched:update-schedule! ((scheduler sched:dynamic-scheduler) event-id execution-time)
  "Record the time when an event occurred. Update remaining commitments. May be free or contingent
   event. Mostly Figs 5 and 6. The outer function controls rescheduling logic. The inner function,
   `%update-schedule!', is responsible for actually updating the schedule

   Parameters:
    - event-id - string (probably all caps) that matches the ID/name of an event
    - execution-time - absolute time when the event occurred
    - strict-ctg-p - If t, contingent events are not allowed to be scheduled prior to their lower
                     bounds. This is a situation that can happen when events with variable
                     observation delay in the original STNU get tighter bounds in the fixed-delay
                     STNU that is actually dispatched. If nil, early ctg events will be buffered
                     to their fixed-delay equivalent lower bounds

   Additionally, if an contingent event is observed too early, it will be buffered until its start time.

   Returns values:
    - bool - recorded
    - bool - already executed
    - bool - did not schedule because it was too early
    - bool - did not schedule because it was too late
    - bool - scheduled, but the contingent event with observation delay arrived early"
  (let ((strict-ctg-p (reschedulep scheduler)))
    (multiple-value-bind (successp already-executed-p too-early-p too-late-p early-ctg-p)
        (%update-schedule! scheduler event-id execution-time :strict-ctg-p strict-ctg-p)

      (when successp
        (return-from sched:update-schedule! (values successp already-executed-p too-early-p too-late-p early-ctg-p)))

      (when (and early-ctg-p strict-ctg-p)
        ;; a contingent event arrived before we were expecting it and we're allowed to reschedule
        (print-when-verbose scheduler "Rescheduling allowed: trying to reschedule ~A at ~A~%" event-id execution-time)
        (let ((rescheduled-p (sched:maybe-reschedule scheduler event-id execution-time)))
          (print-when-verbose scheduler "~A rescheduling ~A~%" (if rescheduled-p "Success" "Failure") event-id))

        ;; regardless of whether rescheduling worked, we need to actually schedule this ctg event.
        ;; do it again, but don't set strict-ctg-p so, in the event the rescheduling was
        ;; unsuccessful, it buffers the ctg event to its original lower bound
        (%update-schedule! scheduler event-id execution-time)))))

(defmethod %update-schedule! ((scheduler sched:dynamic-scheduler) event-id execution-time
                                   &key (strict-ctg-p nil))
  "See `sched:update-schedule!'"
  (with-slots (distances edges stnu zero-point events executed-events gamma) scheduler
    (let* ((node (find-event scheduler event-id))
           ;; FYI the contingent-constraint only lives in the STNU, hence node-in-stnu
           (node-in-stnu (find-event-in-stnu (slot-value scheduler 'stnu) event-id))
           (contingent-constraint (slot-value (or node-in-stnu node) 'tn:contingent-constraint))
           (contingent-p (not (null contingent-constraint)))
           (fixed-delay (alex:ensure-gethash event-id gamma 0))
           (D_ZX (gethash (list +ZERO-POINT+ event-id) distances))
           (D_XZ (gethash (list event-id +ZERO-POINT+) distances))

           ;; correct the execution time of delayed contingent events. we can safely try to correct
           ;; all execution times because the observation delay will be 0 unless VDC says otherwise
           (recorded-execution-time (- execution-time fixed-delay))

           (already-executed (not (null (gethash event-id executed-events))))

           ;; enforce -D(X,Z) < T <= D(Z,X) for free events
           ;; enforce D(Z,X) - lower < T <= D(Z,X) for contingent events
           (too-early (if (not contingent-p)
                          (< recorded-execution-time (- D_XZ))
                          (< recorded-execution-time (- D_ZX (slot-value contingent-constraint 'tn:lower-bound)))))
           (too-late (< D_ZX recorded-execution-time))
           (early-ctg-event nil))

      ;; If a contingent event arrives too early, "buffer" it until the window from VDC, which is
      ;; its lower bound + observation delay
      (when contingent-p
        (let* ((ctg-upper (slot-value contingent-constraint 'tn:upper-bound))
               (ctg-lower (slot-value contingent-constraint 'tn:lower-bound))
               ;; TODO we actually do need to subtract observation delay it seems
               (ctg-range (- ctg-upper ctg-lower))
               (ctg-earliest-time (- D_ZX ctg-range)))

          ;; a contingent event with delay isn't respecting the lower bounds of its fixed-delay equivalent
          (when (< recorded-execution-time ctg-earliest-time)
            (setf early-ctg-event t)
            (print-when-verbose scheduler "~A observed at ~A, earliest is ~A~%" event-id execution-time ctg-earliest-time)

            (when strict-ctg-p
              (print-when-verbose scheduler "strict-ctg-p is t - cannot buffer ~A~%" event-id)
              (return-from %update-schedule! (values nil already-executed too-early too-late early-ctg-event)))

            (setf recorded-execution-time ctg-earliest-time))))

      (print-when-verbose scheduler "Scheduling ~A (~A) at ~A~%" event-id (if contingent-p "ctg" "free") recorded-execution-time)

      (when (or already-executed too-early too-late)
        (print-when-verbose scheduler "Failed to schedule ~A: ~A~%" event-id (cond (already-executed "Already executed")
                                                                                   (too-early "Too early")
                                                                                   (too-late "Too late")))
        (return-from %update-schedule! (values nil already-executed too-early too-late early-ctg-event)))

      ;; mark the event executed
      (setf (gethash event-id executed-events) t)

      (if contingent-p
          ;;; FAST-EX Fig 6
          ;; 6.1
          (progn
            (setf edges (remove-if #'(lambda (edge) (and (tc:d-edge-is-upper-case? edge)
                                                         (equal (tc:d-edge-conditioned-node edge) node)))
                                   edges))
            ;; 6.2
            (setf (gethash (list event-id +ZERO-POINT+) distances) (- recorded-execution-time))
            (setf edges (cons (tc:create-edge-from-constraint :start node
                                                              :end zero-point
                                                              :weight (- recorded-execution-time))
                              edges))
            ;; 6.3
            (loop :for other-event-id :being the hash-key :in events
                  :if (not (gethash other-event-id executed-events))
                    :do
                       (setf edges (cons (tc:create-edge-from-constraint :start (find-event scheduler other-event-id) :end zero-point
                                                                         :weight (- recorded-execution-time))
                                         edges)))

            ;; 6.4
            (sink-dijkstra scheduler)

            ;; 6.5
            (setf (gethash (list +ZERO-POINT+ event-id) distances) recorded-execution-time)
            (setf edges (cons (tc:create-edge-from-constraint :start zero-point
                                                              :end node
                                                              :weight recorded-execution-time)
                              edges))
            ;; 6.6
            (source-dijkstra scheduler))

          (progn
            ;;; FAST-EX Fig 5
            ;; 5.1
            ;; TODO fix handling keys in distances
            (setf (gethash (list +ZERO-POINT+ event-id) distances) recorded-execution-time)
            (setf edges (cons (tc:create-edge-from-constraint :start zero-point
                                                              :end node
                                                              :weight recorded-execution-time)
                              edges))
            (source-dijkstra scheduler)

            ;; 5.2
            (setf (gethash (list event-id +ZERO-POINT+) distances) (- recorded-execution-time))
            (loop :for other-event-id :being the hash-key :in events
                  :if (not (gethash other-event-id executed-events))
                    :do
                       (setf edges (cons (tc:create-edge-from-constraint :start (find-event scheduler other-event-id) :end zero-point
                                                                         :weight (- recorded-execution-time))
                                         edges)))
            (setf edges (cons (tc:create-edge-from-constraint :start node
                                                              :end zero-point
                                                              :weight (- recorded-execution-time))
                              edges))
            (sink-dijkstra scheduler)))

      (if (verbosep scheduler)
          (format t "Success scheduling ~A~%" event-id))

      ;; execution was recorded
      (values t already-executed too-early too-late early-ctg-event))))

(defmethod get-commitments ((scheduler sched:dynamic-scheduler))
  "Get a hash table of time ranges when events may be scheduled.

   Returns { `(event-id contingent-p) : `(lower upper) }"

  (with-slots (distances events zero-point executed-events stnu) scheduler
    (let ((commitments (make-hash-table :test #'equal)))
      (loop :for event-id :being the hash-keys :in events
            :do
               (let* ((node (find-event scheduler event-id))
                      (node-in-stnu (find-event-in-stnu stnu event-id))
                      (already-executed-p (not (null (gethash event-id executed-events))))
                      (contingent-constraint (slot-value (or node-in-stnu node) 'tn:contingent-constraint))
                      (contingent-p (not (null contingent-constraint)))
                      (D_XZ (gethash (list event-id +ZERO-POINT+) distances))
                      (D_ZX (gethash (list +ZERO-POINT+ event-id) distances))
                      (lower (if (or (not contingent-p) already-executed-p)
                                 (- D_XZ)
                                 (let* ((ctg-upper (slot-value contingent-constraint 'tn:upper-bound))
                                        (ctg-lower (slot-value contingent-constraint 'tn:lower-bound))
                                        (ctg-range (- ctg-upper ctg-lower)))
                                   (- D_ZX ctg-range))))
                      (upper D_ZX))

                 (setf (gethash (list event-id contingent-p) commitments)
                       (list lower upper))))
      commitments)))

(defmethod get-enabled-events ((scheduler sched:dynamic-scheduler))
  "Returns enabled events. Similar to Fig 3 in Hunsberger 2013

   Returns (list (event-id contingent-p))"
  (with-slots (distances events executed-events stnu) scheduler
    (let (enabled-events)
      (loop :for n1 :being the hash-keys :in events
            :when (not (gethash n1 executed-events))
              :do
                 (let ((enabled t))
                   (loop :for n2 :being the hash-keys :in events
                         :when (not (gethash n2 executed-events))
                           :do
                              (when (> 0 (gethash (list n1 n2) distances))
                                (setf enabled nil)))
                   (when enabled
                     (let* ((node (find-event scheduler n1))
                            (node-in-stnu (find-event-in-stnu stnu n1))
                            (contingent-p (not (null (slot-value (or node-in-stnu node) 'tn:contingent-constraint))))
                            (event-contingent-pair (list n1 contingent-p)))
                       (setf enabled-events (cons event-contingent-pair enabled-events))))))
      enabled-events)))

(defmacro set-bound (bound constraint original-value)
  `(setf (funcall ,bound ,constraint) ,original-value))

(defmethod sched:maybe-reschedule ((scheduler sched:dynamic-scheduler) event-id execution-time)
  "Convert a ctg event to a free event in the original variable-delay STNU and recheck VDC. If
   controllable, future decisions will use this STNU. Else, we'll continue scheduling as normal

   Returns: did-reschedule-p"

  (let* ((original-constraints (tighten-ctg-event! scheduler event-id execution-time))
         (vdc-stnu (vdc-stnu-of scheduler))
         (fixed-delay-stnu (tc:convert-variable-to-fixed-delay vdc-stnu))
         did-reschedule-p)
    (multiple-value-bind (controllable-p conflict orig-rev-graph gamma full-rev-graph)
        ;; check controllability. if controllable, reset scheduling for this new STNU
        (tc:delay-controllable? fixed-delay-stnu)
      (declare (ignore conflict orig-rev-graph))

      (if controllable-p
          ;; we found a better schedule! update the scheduler accordingly
          (let ((original-executed-events (alex:copy-hash-table (slot-value scheduler 'executed-events)))
                (original-distances (alex:copy-hash-table (slot-value scheduler 'distances))))

            (setf (slot-value scheduler 'stnu) fixed-delay-stnu)

            ;; will overwrite the current progress in the STNU
            (set-slots! scheduler fixed-delay-stnu full-rev-graph gamma)

            ;; fast-forward to this same point in the STNU
            (loop :for executed-event-id :being the hash-key :in original-executed-events
                  :unless (equal executed-event-id +ZERO-POINT+)
                    :do
                       (let ((original-time (gethash (list +ZERO-POINT+ executed-event-id) original-distances)))
                         (%update-schedule! scheduler executed-event-id original-time)))

            (setf did-reschedule-p t)
            (print-when-verbose scheduler "Rescheduled ~A and rebuilt FAST-EX vars~%" event-id))

          ;; revert to the original STNU
          (progn
            (tn:do-constraints (constraint vdc-stnu)
              (let ((originals (gethash constraint original-constraints)))
                (when (not (null originals))
                  (loop :for (bound . original-value) :in originals
                        :do (cond ((equal bound :lower) (setf (tn:lower-bound constraint) original-value))
                                  ((equal bound :upper) (setf (tn:upper-bound constraint) original-value))
                                  ((equal bound :gamma-) (setf (tn:min-observation-delay constraint) original-value))
                                  ((equal bound :gamma+) (setf (tn:max-observation-delay constraint) original-value)))))))
            (print-when-verbose scheduler "Cannot reschedule early ctg event ~A at ~A~%" event-id execution-time))))

    did-reschedule-p))

(defmethod tighten-ctg-event! ((scheduler sched:dynamic-scheduler) event-id execution-time)
  "Modify the input variable delay STNU by narrowing the bounds on the delayed contingent event.

   Returns hash-table mapping constraints to alist of their original bounds"
  (let* ((input-vdc-stnu (vdc-stnu-of scheduler))
         (distances (slot-value scheduler 'distances))
         (originals (make-hash-table :test #'equal)))

    (tn:do-constraints (constraint input-vdc-stnu)
      (let ((to (tn:to-event constraint)))
        (when (equal (tn:name to) event-id)
          (let* ((from-id (tn:name (tn:from-event constraint)))
                 (original-lower (tn:lower-bound constraint))
                 (original-upper (tn:upper-bound constraint))
                 (original-gamma- (or (tn:min-observation-delay constraint) 0))
                 (original-gamma+ (or (tn:max-observation-delay constraint) 0))
                 (original-bounds (list (cons :lower original-lower)
                                        (cons :upper original-upper)
                                        (cons :gamma- original-gamma-)
                                        (cons :gamma+ original-gamma+)))
                 (start-time (gethash (list +ZERO-POINT+ from-id) distances))
                 (elapsed-time (- execution-time start-time)))

            ;; save the original constraints so they can be reset later
            (setf (gethash constraint originals) original-bounds)

            (destructuring-bind (lower upper gamma- gamma+)
                (sched:narrow-bounds elapsed-time original-lower original-upper original-gamma- original-gamma+)
              (print-when-verbose scheduler "~A at ~A turns into [~A,~A] gamma=[~A,~A]~%" event-id execution-time lower upper gamma- gamma+)
              (setf (tn:lower-bound constraint) lower)
              (setf (tn:upper-bound constraint) upper)
              (setf (tn:min-observation-delay constraint) gamma-)
              (setf (tn:max-observation-delay constraint) gamma+))))))

    originals))

(defun sched:narrow-bounds (observation-time original-lower original-upper original-gamma- original-gamma+)
  "Given an observation time and the original ctg and delay bounds, find a new (tighter) set of bounds
   that match the observation"
  (let ((lower (max original-lower (- observation-time original-gamma+)))
        (upper (min original-upper (- observation-time original-gamma-)))
        (gamma- (max original-gamma- (- observation-time original-upper)))
        (gamma+ (min original-gamma+ (- observation-time original-lower))))
    (list lower upper gamma- gamma+)))

(defmethod sched:has-unexecuted-events-p ((scheduler sched:dynamic-scheduler))
  "Are we done yet?"
  (with-slots (executed-events events) scheduler
    (< (hash-table-count executed-events) (hash-table-count events))))

(defmethod find-event ((scheduler sched:dynamic-scheduler) event-id)
  "Find an event in the d-graph. This method exists because the STNU and the edges in a scheduler use
   two different underlying temporal networks."
  (let (node)
    (loop :for edge :in (slot-value scheduler 'edges)
          :do (let* ((start (tc:d-edge-from-node edge))
                     (end (tc:d-edge-to-node edge))
                     (is-start-p (equal (tn:name start) event-id))
                     (is-end-p (equal (tn:name end) event-id)))
                (when is-start-p
                  (setf node start))
                (when is-end-p
                  (setf node end))
                (when node (return))))
    node))

(defun find-event-in-stnu (stnu event-id)
  "Find an event in the STNU by name"
  (tn:do-events (event stnu)
    (when (equal (tn:name event) event-id)
      (return-from find-event-in-stnu event)))
  nil)



;;; The dijkstra algos

(defmethod sink-dijkstra ((scheduler sched:dynamic-scheduler))
  "Effectively updates upper bounds to future events by wrapping Dijkstra SSSP to Z. Sets D(X, Z) for
   each event X. See Fig. 4"
  (with-slots (edges distances zero-point events) scheduler
    (let ((edge-list (make-hash-table :test 'equal))
          (orig-distances (make-hash-table :test 'equal)))
      (loop for k being the hash-keys in distances do
        (setf (gethash k orig-distances) (gethash k distances)))
      (dolist (e edges)
        (let* ((start (tc:d-edge-from-node e))
               (end (tc:d-edge-to-node e))
               (weight (tc:d-edge-weight e))
               (h_U (gethash (list +ZERO-POINT+ (tn:name start)) distances))
               (h_V (gethash (list +ZERO-POINT+ (tn:name end)) distances)))
          (when (or (null (tc:d-edge-conditioned-node e))
                    (tc:d-edge-is-upper-case? e))
            (let* ((new-edge (tc:create-edge-from-constraint :start end :end start
                                                             :weight (+ h_U weight (- h_V))))
                   (updated-edge-list (cons new-edge (gethash end edge-list))))
              (setf (gethash end edge-list) updated-edge-list)))))

      (let ((results (dijkstra-shortest-paths edge-list zero-point)))
        (loop for n being the hash-keys in events do
          (setf (gethash (list n +ZERO-POINT+) distances)
                (- (gethash n results)
                   (gethash (list +ZERO-POINT+ n) orig-distances))))))))

(defmethod source-dijkstra ((scheduler sched:dynamic-scheduler))
  "Effectively updates lower bounds to future events by wrapping Dijkstra SSSP from Z. Sets D(Z, X)
   for each event X. See Fig. 4"
  (with-slots (edges distances zero-point events) scheduler
    (let ((edge-list (make-hash-table :test 'equal))
          (orig-distances (make-hash-table :test 'equal)))
      (loop for k being the hash-keys in distances do
        (setf (gethash k orig-distances) (gethash k distances)))
      (dolist (e edges)
        (let* ((start (tc:d-edge-from-node e))
               (end (tc:d-edge-to-node e))
               (weight (tc:d-edge-weight e))
               (h_U (gethash (list (tn:name start) +ZERO-POINT+) distances))
               (h_V (gethash (list (tn:name end) +ZERO-POINT+) distances)))
          (when (or (null (tc:d-edge-conditioned-node e))
                    (tc:d-edge-is-upper-case? e))
            (let* ((new-edge (tc:create-edge-from-constraint :start start :end end
                                                             :weight (+ h_V weight (- h_U))))
                   (updated-edge-list (cons new-edge (gethash start edge-list))))
              (setf (gethash start edge-list) updated-edge-list)))))

      (let ((results (dijkstra-shortest-paths edge-list zero-point)))
        (loop for n being the hash-keys in events do
          (setf (gethash (list +ZERO-POINT+ n) distances)
                (- (gethash n results)
                   (gethash (list n +ZERO-POINT+) orig-distances))))))))

(defstruct dijkstra-struct
  (weight 0)
  (node nil))

(defun dijkstra-struct< (q1 q2)
  (< (dijkstra-struct-weight q1)
     (dijkstra-struct-weight q2)))

;; see p. 258 in Intro to Algos textbook
(defun dijkstra-shortest-paths (edge-list start)
  (let ((min-distances (make-hash-table :test 'equal))
        ;;(visited (make-hash-table :test 'equal))
        ;;(all-nodes (loop for n being the hash-keys of edge-list collect n))
        (queue (make-queue :priority-queue :compare #'dijkstra-struct<)))
    (setf (gethash (tn:name start) min-distances) 0)
    (qpush queue (make-dijkstra-struct :weight 0
                                       :node start))
    (loop while (> (qsize queue) 0) do
      (let* ((qs (qpop queue))
             (curr (dijkstra-struct-node qs))
             (curr-distance (gethash (tn:name curr) min-distances)))
        (dolist (e (gethash curr edge-list))
          (let* ((end (tc:d-edge-to-node e))
                 (old-min-result (multiple-value-list (gethash (tn:name end) min-distances)))
                 (weight (tc:d-edge-weight e))
                 (next-weight (+ curr-distance weight)))
            (if (< next-weight 0)
                (progn
                  (print "found a negative edge!")
                  (tc:print-edge e)
                  (return-from dijkstra-shortest-paths min-distances)))
            (when (or (not (second old-min-result))
                      (< next-weight (first old-min-result)))
              (setf (gethash (tn:name end) min-distances) next-weight)
              (qpush queue (make-dijkstra-struct :weight next-weight
                                                 :node end)))))))
    min-distances))

(defmethod print-when-verbose ((scheduler sched:dynamic-scheduler) &rest message)
  (when (verbosep scheduler)
    (apply 'format t message)))
