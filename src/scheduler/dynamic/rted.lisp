(in-package #:%kirk-v2/scheduler/dynamic)

(defstruct rted
  "Real-time execution decision. Defaults to a `wait' decision"
  time
  ;; events is (values event-id noop-p)
  events)

(defun no-time-or-events-p (decision)
  "Whether an RTED has a time and list of free events associated with it"
  (or (not (rted-time decision))
      (not (rted-events decision))))

(deftype wait ()
  "A real-time execution decision representing a `wait' for a contingent event"
  `(satisfies no-time-or-events-p))

(defun sched:rted-time (decision)
  (rted-time decision))

(defun sched:rted-events (decision)
  (rted-events decision))
