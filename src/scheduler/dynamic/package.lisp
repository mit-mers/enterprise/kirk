(uiop:define-package #:%kirk-v2/scheduler/dynamic
  (:use #:cl
        #:iterate
        #:queues)
  (:local-nicknames (#:sched #:kirk-v2/scheduler)
                    (#:utils #:%kirk-v2/scheduler/utils)
                    (#:alex #:alexandria)
                    (#:cltl2 #:trivial-cltl2)
                    (#:tc #:temporal-controllability)
                    (#:tn #:temporal-networks)
                    (#:op #:opsat-v3/subsolvers/temporal-networks/common))
  (:export #:dynamic-scheduler
           #:freeze-ctg

           ;; the scheduler's public API
           #:update-schedule!
           #:get-next-rted
           #:get-commitments
           #:get-enabled-events

           ;; RTEDs public API
           #:make-rted
           #:rted-events
           #:rted-time
           #:wait

           ;; for testing and debugging
           ;; TODO remove these - just use :: instead
           #:collect-rted
           #:vdc-stnu-of
           #:maybe-reschedule
           #:narrow-bounds))

(in-package #:%kirk-v2/scheduler/dynamic)
