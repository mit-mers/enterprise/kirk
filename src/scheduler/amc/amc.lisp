(in-package #:%kirk-v2/scheduler/amc)

(defclass amc-scheduler (utils:coalescing-scheduler utils:standard-scheduler)
  (;; These slots are provided by the user.
   (%monitored-locations
    :initarg :monitored-locations
    :initform nil
    :reader monitored-locations)
   (%addition-locations
    :initarg :addition-locations
    :initform nil
    :reader addition-locations)

   ;; These slots are computed by us before beginning scheduling.
   (%monitored-edges
    :accessor monitored-edges)
   (%addition-edges
    :accessor addition-edges)
   (%start-event-index
    :accessor start-event-index)

   ;; These slots are initialized by us before beginning scheduling and modified
   ;; throughout the scheduling
   (%enabled-events
    :initform (make-hash-table)
    :accessor scheduler-enabled-events
    :documentation
    "A hash table where the keys are enabled events. The values are always T.")
   (%blocking-events
    :initform (make-hash-table)
    :accessor scheduler-blocking-events
    :documentation
    "A hash table that maps events to a list of events that are blocking them
from being enabled.")
   (%dispatched-events
    :initform (make-hash-table)
    :reader scheduler-dispatched-events
    :documentation
    "A hash table. Keys are the events that are dispatched (they've been
commanded to be executed, but we have not yet received the time at which they
were actually executed). The values are always T.")
   (%unexecuted-events
    :initform (make-hash-table)
    :reader scheduler-unexecuted-events
    :documentation
    "A hash table. Keys are the events that are as yet undispatched. The values
are always T."))
  (:documentation
   "A basic scheduler. Maintains time windows for every event."))

(defclass amc-location ()
  ((%from :initarg :from :reader from)
   (%to :initarg :to :reader to)
   (%lower-bound-p :initarg :lower-bound-p :reader lower-bound-p)
   (%upper-bound-p :initarg :upper-bound-p :reader upper-bound-p)
   (%start-conditions :initarg :start-conditions :reader start-conditions)
   (%end-conditions :initarg :end-conditions :reader end-conditions)))

(defclass monitored-location (amc-location)
  ())

(defclass addition-location (amc-location)
  ())

(defclass amc-edge ()
  ((%from :initarg :from :reader from)
   (%to :initarg :to :reader to)
   (%start-conditions :initarg :start-conditions :accessor start-conditions)
   (%end-conditions :initarg :end-conditions :accessor end-conditions)))

(defclass monitored-edge (amc-edge)
  ())

(defclass addition-edge (amc-edge)
  ())

(defun locations-to-edges (scheduler locations type)
  (labels ((sort-conditions (conditions)
             (mapcar (lambda (x) (sort (copy-list x) #'<))
                     (mapcar (alex:curry #'utils:event-index scheduler) conditions)))
           (location-to-edges (loc)
             (let ((out ()))
               (when (upper-bound-p loc)
                 (push (make-instance type
                                      :from (utils:event-index scheduler (from loc))
                                      :to (utils:event-index scheduler (to loc))
                                      :start-conditions (sort-conditions (start-conditions loc))
                                      :end-conditions (sort-conditions (end-conditions loc)))
                       out))
               (when (lower-bound-p loc)
                 (push (make-instance type
                                      :from (utils:event-index scheduler (to loc))
                                      :to (utils:event-index scheduler (from loc))
                                      :start-conditions (sort-conditions (start-conditions loc))
                                      :end-conditions (sort-conditions (end-conditions loc)))
                       out))
               out)))
    (alex:mappend #'location-to-edges locations)))

(defun canonicalize-edges (edges d)
  (labels ((matching-edges (edge list)
             (remove-if-not (lambda (x) (and (= (from edge) (from x))
                                             (= (to edge) (to x))))
                            list))
           (condition-remove-dominated-events (condition)
             (remove-if (lambda (i) (some (lambda (j) (minusp (aref d j i))) condition)) condition))
           (remove-dominated-conditions (conditions)
             (remove-if (lambda (c1)
                          (some (lambda (c2)
                                  (every (lambda (i)
                                           (some (lambda (j)
                                                   (minusp (aref d j i)))
                                                 c2))
                                         c1))
                                conditions))
                        conditions))
           (coalesce (edge edges)
             (setf (start-conditions edge)
                   (remove-dominated-conditions
                    (mapcar #'condition-remove-dominated-events (start-conditions edge))))
             (setf (end-conditions edge)
                   (remove-dominated-conditions
                    (mapcar #'condition-remove-dominated-events (end-conditions edge))))
             (dolist (new-edge edges)
               (setf (start-conditions edge)
                     (remove-dominated-conditions
                      (union (start-conditions edge)
                             (mapcar #'condition-remove-dominated-events
                                     (start-conditions new-edge))
                             :test #'equal)))
               (setf (end-conditions edge)
                     (remove-dominated-conditions
                      (union (start-conditions edge)
                             (mapcar #'condition-remove-dominated-events
                                     (start-conditions new-edge))
                             :test #'equal))))
             edge))
    (loop
      :for edge := (pop edges)
      :until (null edge)
      :for matching-edges := (matching-edges edge edges)
      :collect (coalesce edge matching-edges)
      :do (setf edges (set-difference edges matching-edges)))))


;; * Initialization

(defmethod utils:scheduler-state-plan-to-stn ((scheduler amc-scheduler) state-plan)
  (utils:state-plan-to-stn state-plan :augmentedp t))

(defmethod slot-unbound (class (scheduler amc-scheduler) slot-name)
  (if (member slot-name '(%enabled-events %blocking-events %dispatched-events
                          %unexecuted-events %start-event-index))
      (progn (defs:ensure-scheduler-ready scheduler) (slot-value scheduler slot-name))
      (call-next-method)))

(defun implicit-addition-edges (num-events)
  ;; TODO: Remove hardcoding indices??
  (loop
    :for i :upfrom 2 :below num-events
    :collect (make-instance 'addition-edge
                            :from i
                            :to 0
                            :end-conditions (list (list i))
                            :start-conditions (list (list i)))
    :collect (make-instance 'addition-edge
                            :from 0
                            :to i
                            :end-conditions (list (list i))
                            :start-conditions (list (list i)))))

(defun implicit-monitored-edges (num-events po)
  ;; TODO: Remove hardcoding indices??
  (list*
   (make-instance 'monitored-edge
                  :from 0
                  :to 1
                  :start-conditions (list (list 0))
                  :end-conditions (list (list 1)))
   (loop
     :for i :upfrom 2 :below num-events
     :for start-conditions :=  (list
                                (list* 0
                                       (loop :for j :upfrom 2 :below num-events
                                             :when (utils:i-before-j-p po i j)
                                               :collect j)))
     :collect (make-instance 'monitored-edge
                             :from i
                             :to 0
                             :end-conditions (list (list i))
                             :start-conditions start-conditions))))

(defmethod defs:ensure-scheduler-ready ((scheduler amc-scheduler))
  "Compute an STN from the state plan and compute its minimum dispatchable
form."
  (let* ((state-plan (utils:state-plan-of scheduler))
         (stn (utils:stn-of scheduler))
         (d-graph (utils:apsp-graph-of scheduler))
         (po (utils:extract-partial-order d-graph))
         (number-of-events (array-dimension d-graph 0))
         (start-event-index (utils:stn-index-of-event stn (odo:start-event state-plan)))
         edges
         monitored-edges
         addition-edges)
    (setf edges (make-instance 'utils:edge-overlay :number-of-nodes number-of-events))

    ;; Canonicalize addition and monitored locations/edges
    (setf monitored-edges (append
                           (implicit-monitored-edges number-of-events po)
                           (locations-to-edges scheduler (monitored-locations scheduler)
                                               'monitored-edge)))
    (setf addition-edges (append
                          (implicit-addition-edges number-of-events)
                          (locations-to-edges scheduler (addition-locations scheduler)
                                              'addition-edge)))

    (setf monitored-edges (canonicalize-edges monitored-edges d-graph)
          addition-edges (canonicalize-edges addition-edges d-graph))
    (setf (utils:edges-of scheduler) edges
          (start-event-index scheduler) start-event-index
          (monitored-edges scheduler) monitored-edges
          (addition-edges scheduler) addition-edges)

    ;; Next, compute the enabled events.
    (dotimes (i number-of-events)
      ;; TODO: Remove hardcoding these 1s.
      (unless (= i 1)
        (setf (gethash i (scheduler-unexecuted-events scheduler)) t)
        (let (blocking-events)
          (utils:do-outgoing-edges (edges i j)
            (unless (= j 1)
              (let ((d (aref d-graph i j)))
                (when (not (plusp d))
                  (push j blocking-events)))))
          (if blocking-events
              (setf (gethash i (scheduler-blocking-events scheduler)) blocking-events)
              (setf (gethash i (scheduler-enabled-events scheduler)) t)))))

    ;; Something has gone horribly wrong if the start event isn't enabled.
    (assert (gethash start-event-index (scheduler-enabled-events scheduler))))
  scheduler)


;; * Methods

(defmethod defs:has-unexecuted-events-p ((scheduler amc-scheduler))
  (plusp (hash-table-count (scheduler-unexecuted-events scheduler))))

(defmethod defs:next-event-time ((scheduler amc-scheduler))
  (let ((events-of-interest (set-difference (alex:hash-table-keys (scheduler-enabled-events scheduler))
                                            (alex:hash-table-keys (scheduler-dispatched-events scheduler)))))
    (unless (null events-of-interest)
      (loop
        :with distances := (utils:apsp-graph-of scheduler)
        :with start-event-index := (start-event-index scheduler)
        :for index :in events-of-interest
        :minimizing (- (aref distances index start-event-index))))))

(defmethod defs:latest-execution-time ((scheduler amc-scheduler))
  ;; TODO: Remove hardcoding of 1.
  (aref (utils:apsp-graph-of scheduler) (start-event-index scheduler) 1))

(defmethod defs:active-events ((scheduler amc-scheduler) time)
  (let ((events-of-interest (set-difference (alex:hash-table-keys (scheduler-enabled-events scheduler))
                                            (alex:hash-table-keys (scheduler-dispatched-events scheduler)))))
    (loop
      :with distances := (utils:apsp-graph-of scheduler)
      :with start-event-index := (start-event-index scheduler)
      :for index :in events-of-interest
      :if (<= (- (aref distances index start-event-index)) time)
        :collect index)))

(defmethod defs:mark-events-dispatched ((scheduler amc-scheduler) indices)
  (dolist (index indices)
    (setf (gethash index (scheduler-dispatched-events scheduler)) t)))

(defgeneric interested-edges (scheduler from to))

(defmethod interested-edges ((scheduler amc-scheduler) i j)
  (let ((count (array-dimension (utils:apsp-graph-of scheduler) 0))
        (start-event (start-event-index scheduler))
        (out ()))
    (dotimes (u count)
      (unless (= u start-event)
        (push (list u start-event) out)
        (push (list start-event u) out)))
    ;; (loop :for u :below count
    ;;       :do (loop :for v :below count
    ;;                 :unless (= u v)
    ;;                   :do (push (list u v) out)))
    out))

(defmethod defs:mark-events-executed ((scheduler amc-scheduler) indices relative-time)
  (dolist (event indices)
    (assert (gethash event (scheduler-enabled-events scheduler)))
    (remhash event (scheduler-unexecuted-events scheduler))
    (remhash event (scheduler-dispatched-events scheduler))
    ;; Propagate!
    (let ((start-event (start-event-index scheduler))
          (distances (utils:apsp-graph-of scheduler))
          (edges (utils:edges-of scheduler))
          (relative-time (coerce relative-time 'double-float))
          (unexecuted-events (scheduler-unexecuted-events scheduler)))
      (remhash event (scheduler-enabled-events scheduler))
      (setf (aref distances event start-event) (- relative-time)
            (aref distances start-event event) relative-time)

      (let ((i event)
            (j start-event))
        (dolist (edge (interested-edges scheduler i j))
          (destructuring-bind (u v) edge
            (alex:minf (aref distances u v)
                       (+ (aref distances u i) (aref distances i j) (aref distances j v))))))

      (let ((i start-event)
            (j event))
        (dolist (edge (interested-edges scheduler i j))
          (destructuring-bind (u v) edge
            (alex:minf (aref distances u v)
                       (+ (aref distances u i) (aref distances i j) (aref distances j v))))))

      (utils:do-incoming-edges (edges event event-2)
        (let ((distance (aref distances event-2 event)))
          (when (and (gethash event-2 unexecuted-events)
                     (not (plusp distance))
                     (null (alex:deletef (gethash event-2 (scheduler-blocking-events scheduler))
                                         event)))
            (setf (gethash event-2 (scheduler-enabled-events scheduler)) t)))))))
