(uiop:define-package #:%kirk-v2/scheduler/amc
  (:use #:cl)
  (:local-nicknames (#:defs #:%kirk-v2/scheduler/defs)
                    (#:utils #:%kirk-v2/scheduler/utils)
                    (#:alex #:alexandria))
  (:export #:amc-scheduler))

(in-package #:%kirk-v2/scheduler/amc)
