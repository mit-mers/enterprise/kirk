(uiop:define-package #:%kirk-v2/scheduler/mdf
  (:use #:cl)
  (:local-nicknames (#:sched #:kirk-v2/scheduler)
                    (#:utils #:%kirk-v2/scheduler/utils)
                    (#:alex #:alexandria)
                    (#:cltl2 #:trivial-cltl2)))

(in-package #:%kirk-v2/scheduler/mdf)
