(in-package #:%kirk-v2/scheduler/mdf)

(defclass sched:minimum-dispatchable-form-scheduler (utils:standard-scheduler)
  (;; Provided by the user.
   (%treat-loose-precedence-as-strong-p
    :initform nil
    :initarg :treat-loose-precedence-as-strong-p
    :reader treat-loose-precedence-as-strong-p)
   ;; These slots are initialized by us before beginning scheduling and modified
   ;; throughout the scheduling
   (%time-window-lbs
    :accessor scheduler-time-window-lbs
    :documentation
    "A vector containing the current lower bound of each event. Relative to the
    start time.")
   (%time-window-ubs
    :accessor scheduler-time-window-ubs
    :documentation
    "A vector containing the current upper bound of each event. Relative to the
    start time.")
   (%enabled-events
    :initform (make-hash-table)
    :accessor scheduler-enabled-events
    :documentation
    "A hash table where the keys are enabled events. The values are always T.")
   (%blocking-events
    :initform (make-hash-table)
    :accessor scheduler-blocking-events
    :documentation
    "A hash table that maps events to a list of events that are blocking them
from being enabled.")
   (%unexecuted-events
    :initform (make-hash-table)
    :reader scheduler-unexecuted-events
    :documentation
    "A hash table. Keys are the events that are as yet undispatched. The values
are always T."))
  (:documentation
   "A basic scheduler. Maintains time windows for every event.

Provides the initarg :TREAT-LOOSE-PRECEDENCE-AS-STRONG-P. If non-NIL, then zero
weight edges in the APSP graph paired with a positive weight edge in the
opposite direction are treated as edges with weight negative epsilon. This
prevents situations where there is something like `a` ---[0, inf]---> `b`,
executing b first, and then realizing that `a` needs to be executed at the
exact same time. You almost certainly want to set this to T unless you are
running in a very controlled environment."))


;; * Initialization

(defmethod slot-unbound (class (scheduler sched:minimum-dispatchable-form-scheduler) slot-name)
  (if (member slot-name '(%time-window-lbs %time-window-ubs
                          %enabled-events %blocking-events %unexecuted-events
                          %blocks-events utils::%reasoning-graph))
      (progn (sched:ensure-scheduler-ready scheduler) (slot-value scheduler slot-name))
      (call-next-method)))

(defmethod sched:ensure-scheduler-ready ((scheduler sched:minimum-dispatchable-form-scheduler))
  "Compute an distance graph from the state plan and compute its minimum
dispatchable form."
  (let* ((state-plan (utils:state-plan-of scheduler))
         (graph (utils::apsp-graph (utils:d-graph-of scheduler)))
         (number-of-events (utils:number-of-nodes graph))
         (ubs (make-array number-of-events
                          :element-type 'double-float
                          :initial-element utils:+double-inf+))
         (lbs (make-array number-of-events
                          :element-type 'double-float
                          :initial-element 0d0))
         (start-event-index (utils:event-index scheduler (odo:start-event state-plan))))
    (compute-mdf! graph)
    (setf (utils:reasoning-graph-of scheduler) graph)

    (setf (aref lbs start-event-index) 0d0
          (scheduler-time-window-ubs scheduler) ubs
          (scheduler-time-window-lbs scheduler) lbs)

    ;; Next, compute the enabled events.
    (dotimes (i number-of-events)
      (setf (gethash i (scheduler-unexecuted-events scheduler)) t)
      (let (blocking-events)
        (utils:do-graph-outgoing-edges (graph i j d)
          (when (or (minusp d)
                    (and (treat-loose-precedence-as-strong-p scheduler)
                         (zerop d)
                         (plusp (utils:distance graph j i))))
            (push j blocking-events)))
        (if blocking-events
            (setf (gethash i (scheduler-blocking-events scheduler)) blocking-events)
            (setf (gethash i (scheduler-enabled-events scheduler)) t))))
    ;; Something has gone horribly wrong if the start event isn't enabled.
    (assert (gethash start-event-index (scheduler-enabled-events scheduler))))
  scheduler)


;; * Methods

(defmethod sched:has-unexecuted-events-p ((scheduler sched:minimum-dispatchable-form-scheduler))
  (plusp (hash-table-count (scheduler-unexecuted-events scheduler))))

(defmethod sched:active-decisions ((scheduler sched:minimum-dispatchable-form-scheduler))
  (let ((decisions ())
        (max-linger-time utils:+double-inf+)
        (graph (utils:reasoning-graph-of scheduler))
        (unexecuted-events (scheduler-unexecuted-events scheduler))
        (lbs (scheduler-time-window-lbs scheduler))
        (ubs (scheduler-time-window-ubs scheduler))
        (skip-ht (make-hash-table))
        (treat-loose-precedence-as-strong-p (treat-loose-precedence-as-strong-p scheduler)))
    (flet ((doit (event _)
             (declare (ignore _))
             (unless (gethash event skip-ht)
               (let ((simultaneous-events ())
                     (constrains-within utils:+double-inf+))
                 (utils:do-graph-outgoing-edges (graph event event-2 d)
                   (when (gethash event-2 unexecuted-events)
                     (alex:minf constrains-within d)
                     (when (and (zerop d)
                                (zerop (utils:distance graph event-2 event)))
                       (push event-2 simultaneous-events)
                       (setf (gethash event-2 skip-ht) t))))
                 (when (or (not treat-loose-precedence-as-strong-p)
                           ;; In this case, it's possible that not all
                           ;; simultaneous events are enabled. For instance,
                           ;; consider:
                           ;;
                           ;; a - 0 -> b, b - 0 -> a, b - 0 -> c.
                           ;;
                           ;; a is considered enabled, even under treat loose
                           ;; as strong. But b is not. So we need to confirm
                           ;; that all simultaneous events are enabled.
                           (every (alex:rcurry #'gethash (scheduler-enabled-events scheduler))
                                  simultaneous-events))
                   (push (make-instance 'utils:standard-dispatch
                                        :events (mapcar (alex:curry #'utils:index-event graph)
                                                        (list* event simultaneous-events))
                                        :constrains-next-decision-within constrains-within
                                        :earliest-time (aref lbs event))
                         decisions))
                 (alex:minf max-linger-time (aref ubs event))))))
      (declare (dynamic-extent #'doit))
      (maphash #'doit (scheduler-enabled-events scheduler)))
    (values decisions max-linger-time)))

(defmacro if-high-safety (() &body body &environment env)
  (let* ((optimize (cltl2:declaration-information 'optimize env))
         (safety (first (alex:assoc-value optimize 'safety)))
         (speed (first (alex:assoc-value optimize 'speed))))
    (if (or (>= safety speed))
        `(progn
           ,@body)
        `(progn))))

(defmethod sched:mark-events-executed ((scheduler sched:minimum-dispatchable-form-scheduler)
                                       indices time)
  (let ((time (coerce time 'double-float))
        (graph (utils:reasoning-graph-of scheduler))
        (lbs (scheduler-time-window-lbs scheduler))
        (ubs (scheduler-time-window-ubs scheduler))
        (treat-loose-precedence-as-strong-p (treat-loose-precedence-as-strong-p scheduler))
        (unexecuted-events (scheduler-unexecuted-events scheduler)))
    (dolist (index indices)
      ;; Propagate!
      (unless (gethash index (scheduler-enabled-events scheduler))
        (error 'sched:simple-scheduler-error
               :scheduler scheduler
               :format-control "Event ~A is not enabled"
               :format-arguments (list (utils:index-event graph index))))
      (unless (>= (aref ubs index) time (aref lbs index))
        (error 'sched:infeasible-execution-time
               :scheduler scheduler
               :time time
               :event (utils:index-event graph index)))
      (remhash index (scheduler-unexecuted-events scheduler))
      (remhash index (scheduler-enabled-events scheduler))
      (setf (aref lbs index) time
            (aref ubs index) time)
      (utils:do-graph-outgoing-edges (graph index index-2 distance)
        ;; We should never have to propagate along outgoing negative edges.
        (when (and (gethash index-2 unexecuted-events)
                   (not (minusp distance)))
          (setf (aref ubs index-2) (min (aref ubs index-2) (+ time distance)))
          ;; Sanity check. An upper bound should never be less than the current
          ;; time.
          (if-high-safety ()
            (unless (>= (aref ubs index-2) time)
              (error 'sched:simple-scheduler-error
                     :scheduler scheduler
                     :format-control "Sanity check failed! An upper bound window is less than current time.")))
          ;; Sanity check. An upper bound should never be less than the lower
          ;; bound.
          (if-high-safety ()
            (unless  (>= (aref ubs index-2) (aref lbs index-2))
              (error 'sched:simple-scheduler-error
                     :scheduler scheduler
                     :format-control "Sanity check failed! Upper bound is less than lower bound propagating along outgoing edges.")))))
      (utils:do-graph-incoming-edges (graph index index-2 distance)
        (when (gethash index-2 unexecuted-events)
          (setf (aref lbs index-2) (max (aref lbs index-2) (- time distance)))
          ;; Sanity check. An upper bound should never be less than the lower
          ;; bound.
          (if-high-safety ()
            (unless (>= (aref ubs index-2) (aref lbs index-2))
              (error 'sched:simple-scheduler-error
                     :scheduler scheduler
                     :format-control "Sanity check failed! Upper bound is less than lower bound propagating along incoming edges.")))
          ;; If this incoming edge is negative, then INDEX was previously
          ;; blocking INDEX-2. Remove it from the blocking list and potentially
          ;; enable it.
          (when (and (or (minusp distance)
                         (and (zerop distance)
                              treat-loose-precedence-as-strong-p
                              (plusp (utils:distance graph index index-2))))
                     (null (alex:deletef (gethash index-2 (scheduler-blocking-events scheduler))
                                         index)))
            (setf (gethash index-2 (scheduler-enabled-events scheduler)) t)))))))


;;; Math

(defun dominates-upper (d a b c)
  (let ((d-ab (utils:distance d a b))
        (d-bc (utils:distance d b c))
        (d-ac (utils:distance d a c)))
    (and (not (minusp d-ac))
         (not (minusp d-bc))
         (= d-ac (+ d-ab d-bc)))))

(defun dominates-lower (d a b c)
  (let ((d-ab (utils:distance d a b))
        (d-bc (utils:distance d b c))
        (d-ac (utils:distance d a c)))
    (and (minusp d-ab)
         (minusp d-ac)
         (= d-ac (+ d-ab d-bc)))))

(defun compute-mdf! (d)
  (let ((marked (make-array (list (utils:number-of-nodes d)
                                  (utils:number-of-nodes d))
                            :element-type 'bit

                            :initial-element 0))
        (v (utils:number-of-nodes d)))
    (flet ((is-marked (i j)
             (not (zerop (aref marked i j))))
           (mark! (i j)
             (setf (aref marked i j) 1)))
      (dotimes (a v)
        (dotimes (b v)
          (unless (= a b)
            (dotimes (c v)
              (unless (or (= a c) (= b c))
                (let ((ud1 (dominates-upper d b a c))
                      (ud2 (dominates-upper d a b c))
                      (ld1 (dominates-lower d c a b))
                      (ld2 (dominates-lower d c b a)))
                  (cond
                    ((and ud1 ud2)
                     (unless (or (is-marked a c)
                                 (is-marked b c))
                       (mark! a c)))
                    (ud1
                     (mark! b c))
                    (ud2
                     (mark! a c)))

                  (cond
                    ((and ld1 ld2)
                     (unless (or (is-marked c a)
                                 (is-marked c b))
                       (mark! c a)))
                    (ld1
                     (mark! c b))
                    (ld2
                     (mark! c a)))))))))
      (dotimes (i v)
        (dotimes (j v)
          (when (is-marked i j)
            (utils:remove-edge! d i j)))))))
