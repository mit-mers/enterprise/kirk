(uiop:define-package #:kirk-v2/scheduler
  (:use #:cl)
  (:local-nicknames (#:doc #:40ants-doc))
  (:export #:foo)
  ;; Provided by defs/
  (:export
   #:@scheduler
   #:active-decisions
   #:dispatch
   #:dispatch-constrains-next-decision-within
   #:dispatch-earliest-time
   #:dispatch-events
   #:ensure-scheduler-ready
   #:execution-decision
   #:has-unexecuted-events-p
   #:infeasible-execution-time
   #:infeasible-execution-time-event
   #:infeasible-execution-time-time
   #:mark-events-executed
   #:scheduler
   #:scheduler-error
   #:simple-scheduler-error)
  ;; Provided by amc/
  (:export
   #:amc-scheduler)
  ;; Provided by mdf/
  (:export
   #:minimum-dispatchable-form-scheduler)
  ;; provided by dynamic/
  (:export
   #:dynamic-scheduler
   #:update-schedule!
   #:freeze-ctg
   #:get-next-rted
   #:rted-events
   #:rted-time
   #:vdc-stnu-of
   #:maybe-reschedule
   #:narrow-bounds)
  ;; provided by delay/
  (:export
   #:delay-scheduler
   #:state-plan-to-stnu
   #:vdc-to-fixed))

(in-package #:kirk-v2/scheduler)
