(uiop:define-package #:kirk-v2/version
    (:use #:cl
          #:split-sequence)
  (:export
   #:freeze-version!
   #:kirk-version))

(in-package #:kirk-v2/version)

;; If the position of this form changes, the asd file needs to be updated as
;; well.
(defparameter *base-version* "0.0.2"
  "A string containing the base version of Kirk.")

(defparameter *prerelease-category* "0"
  "A string naming the prerelease category. Starts at 0, then when getting close
to a release goes to alpha, beta, so on. If this is not a prerelease version,
must be NIL.")

(defvar *frozen-version* nil
  "If non-NIL, this version string is used instead of computing a new one.")


;; * git integration

(defun git-installed-p ()
  (ignore-errors
   (zerop
    (nth-value 2 (uiop:run-program '("git" "--version")
                                   :ignore-error-status t)))))

(defun git-describe ()
  (uiop:run-program '("git" "describe" "--match" "v[0-9]*\\.[0-9]*\\.[0-9]*" "--dirty")
                    :output '(:string :stripped t)))

(defun compute-git-version ()
  (uiop:with-current-directory ((uiop:pathname-directory-pathname (asdf:system-source-file :kirk-v2)))
    (destructuring-bind (_ number-commits sha &optional dirty) (split-sequence #\- (git-describe))
      (declare (ignore _))
      (uiop:strcat *base-version*
                   "-" *prerelease-category* "." number-commits
                   "+" sha (if dirty "-dirty" "")))))


;; * Kirk version functions

(defun kirk-version ()
  (or *frozen-version*
      (ignore-errors
       (and (git-installed-p)
            (compute-git-version)))
      (uiop:strcat *base-version*
                   "-" *prerelease-category*
                   "+unknown")))

(defun freeze-version! ()
  (setf *frozen-version* (kirk-version)))
