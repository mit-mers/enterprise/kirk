;;;; Documentation framework for Kirk-V2.

(uiop:define-package #:kirk-v2/doc
    (:use #:cl
          #:alexandria)
  (:shadow #:defsection)
  ;; These are symbols also defined by mgl-pax.
  (:export #:defsection
           #:macro
           #:section
           #:structure-accessor
           #:accessor
           #:reader
           #:constant)
  ;; These are symbols directly from this package.
  (:export #:*sections*
           #:*mgl-pax-symbol-list*

           #:@kirk-v2
           #:@kirk-v2-dev))

(in-package #:kirk-v2/doc)

(declaim (special @kirk-v2 @kirk-v2-dev))

(defparameter *mgl-pax-symbol-list*
  '(macro section structure-accessor reader constant accessor))

(defvar *sections* nil
  "All sections of the Kirk-V2 manual. We store the defsection forms verbatim so
that we can generate documentation for them whenever mgl-pax is loaded.")

(defun extract-symbols (entries)
  (remove nil (mapcar #'(lambda (x) (if (listp x) (first x) nil))
                      entries)))

(defun extract-sections (entries)
  (flet ((maybe-section (entry)
           (when (and (listp entry)
                      (eql 'section (second entry)))
             (first entry)))
         (maybe-symbol (entry)
           (when (and (listp entry)
                      (not (eql 'section (second entry))))
             (first entry))))
    (list (delete nil (mapcar #'maybe-section entries))
          (delete nil (mapcar #'maybe-symbol entries)))))

(defun section-sections (name)
  (first (extract-sections (assoc-value *sections* name))))

(defun section-symbols (name)
  (second (extract-sections (assoc-value *sections* name))))

(defun export-section (name &optional (package *package*))
  (import name (find-package package))
  (export name (find-package package))
  (dolist (s (section-symbols name))
    (import s (find-package package))
    (export s (find-package package)))
  (dolist (s (section-sections name))
    (export-section s (find-package package)))
  (values))

(defmacro defsection (name (&key title (export t)
                                 export-children
                                 (package *package*))
                      &body entries)
  `(progn
     (setf (assoc-value *sections* ',name) '((:title ,title
                                              :package (find-package ,package))
                                             ,@entries))
     ,(when export
            (let ((syms (extract-symbols entries)))
              `(export ',(list* name syms))))
     ,(when export-children
            `(export-section ',name))
     ',name))
