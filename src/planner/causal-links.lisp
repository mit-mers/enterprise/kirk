(in-package #:kirk-v2/planner)

(defstruct req
  (guard nil)
  name
  value)

(defun extract-state-constraint-requirement (sc &optional parent-guard)
  (let ((expression (odo:constraint-expression sc)))
    (ecase (odo:operator expression)
      (odo:odo-equal
       (let ((name (odo:name (odo:operator
                                                (odo:application-argument-value expression 'odo:left))))
             (value (odo:application-argument-value expression 'odo:right)))
         (make-req :name name :value value :guard parent-guard)))
      (odo:->
       (let ((right (odo:application-argument-value expression 'odo:right))
             (left (odo:application-argument-value expression 'odo:left)))
         (assert (eql (odo:operator right) 'odo:odo-equal))
         (let ((name (odo:name (odo:operator
                                                  (odo:application-argument-value right 'odo:left))))
               (value (odo:application-argument-value right 'odo:right)))
           (make-req :name name :value value :guard (if (null parent-guard)
                                                                          left
                                                                          (odo:and parent-guard left)))))))))

(defun annotate-event-with-produces! (event sc &optional guard)
  (push (extract-state-constraint-requirement sc guard) (odo:annotation event :kirk/produces)))

(defun req-satisfied-p (req prod)
  (and (equal (req-name req) (req-name prod))
       (equal (req-value req) (req-value prod))))

(defun req-violated-p (req prod)
  (and (equal (req-name req) (req-name prod))
       (not (equal (req-value req) (req-value prod)))))

;; (defun reqs-match-p (left right)
;;   (and (equal (req-name left) (req-name right))
;;        (equal (req-value left) (req-value right))))

(defun coalesce-req-guards (reqs)
  (when (every #'req-guard reqs)
    (apply #'odo:or (mapcar #'req-guard reqs))))

(defun maybe-add-guard (pred &rest maybe-guards)
  (setf maybe-guards (remove nil maybe-guards))
  (cond
    ((alex:length= 1 maybe-guards)
     (odo:-> (first maybe-guards) pred))
    (maybe-guards
     (odo:-> (apply #'odo:and maybe-guards) pred))
    (t
     pred)))

(defun add-causal-links! (csp state-plan ep point sc &optional guard)
  ;;(assert (eql point :start))
  ;; Loop over every episode except EP, finding one that produces the
  ;; requirement.

  ;; Loop over every event, finding the events that produce something that
  ;; satisfies SC.
  (let ((req (extract-state-constraint-requirement sc guard))
        (possible-supports nil)
        (possible-threats nil))
    ;;(break "REQ: ~S" req)
    (dolist (event (odo:state-plan-event-list state-plan))
      (let* ((event-produces (odo:annotation event :kirk/produces))
             (event-satisfying-reqs (remove-if-not (alex:curry #'req-satisfied-p req) event-produces))
             (event-violating-reqs (remove-if-not (alex:curry #'req-violated-p req) event-produces)))
        (when event-satisfying-reqs
          (push (cons event event-satisfying-reqs) possible-supports))
        (when (and (not (eql event (odo:start-event state-plan)))
                   event-violating-reqs)
          (push (cons event event-violating-reqs) possible-threats))))
    (ecase point
      (:start
       ;; If this is an at start, we remove the episode's events from possible
       ;; support and threats
       (setf possible-supports (remove (odo:episode-start-event ep)
                                       (remove (odo:episode-end-event ep)
                                               possible-supports)
                                       :key #'car))
       (setf possible-threats (remove (odo:episode-start-event ep)
                                      (remove (odo:episode-end-event ep)
                                              possible-threats)
                                      :key #'car)))
      (:all
       ;; The end cannot provide support.
       (setf possible-supports (remove (odo:episode-end-event ep) possible-supports
                                       :key #'car))
       ;; If the start event can support, it MUST be the support.
       (when (member (odo:episode-start-event ep) possible-supports :key #'car)
         (setf possible-supports (list (find (odo:episode-start-event ep)
                                             possible-supports :key #'car))))
       ;; The end of this episode cannot threaten itself
       (setf possible-threats (remove (odo:episode-end-event ep) possible-threats
                                      :key #'car))
       ;; The start of the episode cannot threaten itself
       (setf possible-threats (remove (odo:episode-start-event ep) possible-threats
                                      :key #'car))))
    (assert possible-supports)
    ;;(break "THREATS: ~S" possible-threats)
    (let (producer-var
          producer-event
          producer-guard)
      (if (alex:length= 1 possible-supports)
          (progn
            (setf producer-event (car (first possible-supports)))
            (setf producer-guard (coalesce-req-guards (cdr (first possible-supports))))
            (unless (eql producer-event (odo:episode-start-event ep))
              ;; If this is the only producer, we need to ensure its guard is
              ;; active when our guard is active!
              (cond
                ((and producer-guard (req-guard req))
                 (odo:add-constraint!
                  csp
                  (odo:make-constraint
                   (odo:-> (req-guard req) producer-guard))))
                (producer-guard
                 (odo:add-constraint! csp (odo:make-constraint producer-guard))))
              (odo:add-constraint!
               csp
               (odo:make-constraint
                (maybe-add-guard
                 (odo:simple-temporal producer-event
                                      (ecase point
                                        ((:start :all)
                                         (odo:episode-start-event ep)))
                                      :lower-bound 1d-3)
                 producer-guard (req-guard req))
                :annotations (list :causal-link t)))))
          (progn
            (if (req-guard req)
                (progn
                  (setf producer-var
                        ;; Would be nice to use conditional vars, but there's
                        ;; some issue when evaluating the expression as part of
                        ;; grounding the plan (it doesn't know about the
                        ;; inactive value).
                        (odo:make-variable 'member
                                           :members (list* nil
                                                           (mapcar (alex:compose #'odo:name #'car)
                                                                   possible-supports))))
                  (odo:add-constraint!
                   csp
                   (odo:make-constraint
                    (odo:<-> (req-guard req) (odo:odo-equal producer-var nil)))))
                (setf producer-var
                      (odo:make-variable 'member
                                         :members (mapcar (alex:compose #'odo:name #'car)
                                                          possible-supports))))
            (dolist (s possible-supports)
              (destructuring-bind (support-event . support-reqs) s
                ;; If this is chosen as the support, its guard must be active
                ;; (assuming it has one).
                (alex:when-let ((support-guard (coalesce-req-guards support-reqs)))
                  (odo:add-constraint!
                   csp
                   (odo:make-constraint
                    (odo:-> (odo:odo-equal producer-var (odo:name support-event))
                            support-guard))))
                (odo:add-constraint! csp
                                     (odo:make-constraint
                                      (odo:-> (odo:odo-equal producer-var (odo:name support-event))
                                              (odo:simple-temporal support-event
                                                                   (ecase point
                                                                     ((:start :all)
                                                                      (odo:episode-start-event ep)))
                                                                   :lower-bound 1d-3))
                                      :annotations (list :causal-link t)))))))
      (dolist (threat possible-threats)
        (destructuring-bind (threat-event . threat-reqs) threat
          (let ((threat-guard (coalesce-req-guards threat-reqs)))
            ;; Each threatening event has to come before or after this episode.
            (cond
              ;; ((and producer-event
              ;;       (equal (odo:name threat-event) (odo:name (odo:episode-start-event ep)))
              ;;       (eql (odo:name producer-event) (odo:name (odo:episode-end-event ep))))
              ;;  (break))
              ;; ((and (eql point :all)
              ;;       (eql producer-event (odo:episode-start-event ep)))
              ;;  ;; The start is the supporting event. The threatening event must come
              ;;  ;; after the end of this episode.
              ;;  (odo:add-constraint! csp
              ;;                       (odo:make-constraint
              ;;                        (odo:simple-temporal
              ;;                         (odo:episode-end-event ep)
              ;;                         threat-event
              ;;                         :lower-bound 1d-3)
              ;;                        :annotations (list :threat t))))
              (producer-event
               ;; There is only a single producer. The threatening event can
               ;; come after the consuming event or before the producer. But
               ;; first, check to make sure it doesn't trivially come after.
               (unless (and (eql point :start)
                            (eql threat-event (odo:episode-end-event ep)))
                 (let ((threat-var (odo:make-variable `(member ,@(when (or threat-guard (req-guard req))
                                                                   (list nil))
                                                               :after :before))))
                   (odo:state-space-add! csp threat-var)
                   ;; We don't care about this variable if the threat is inactive
                   ;; or the current req is inactive.
                   (cond
                     ((and threat-guard (req-guard req))
                      (odo:add-constraint! csp
                                           (odo:make-constraint
                                            (odo:<-> (odo:odo-not (odo:odo-equal threat-var nil))
                                                     (odo:and threat-guard (req-guard req))))))
                     (threat-guard
                      (odo:add-constraint! csp
                                           (odo:make-constraint
                                            (odo:<-> (odo:odo-not (odo:odo-equal threat-var nil))
                                                     threat-guard))))
                     ((req-guard req)
                      (odo:add-constraint! csp
                                           (odo:make-constraint
                                            (odo:<-> (odo:odo-not (odo:odo-equal threat-var nil))
                                                     (req-guard req))))))

                   (odo:add-constraint! csp
                                        (odo:make-constraint
                                         (odo:-> (odo:odo-equal threat-var :before)
                                                 (odo:simple-temporal
                                                  threat-event
                                                  producer-event
                                                  :lower-bound (if (eql point :start)
                                                                   9d-3
                                                                   1d-2)))
                                         :annotations (list :threat t)))
                   ;;(break "~S -> ~S" point producer-event)
                   ;;(when (eql point :all) (break "~S~%~%~S~%~%~S" req threat-reqs threat-guard))
                   (odo:add-constraint! csp
                                        (odo:make-constraint
                                         (odo:-> (odo:odo-equal threat-var :after)
                                                 (odo:simple-temporal
                                                  (ecase point
                                                    (:start
                                                     (odo:episode-start-event ep))
                                                    (:all
                                                     (odo:episode-end-event ep)))
                                                  threat-event
                                                  :lower-bound (if (eql point :start) 9d-3 8d-3)))
                                         :annotations (list :threat t))))))
              (t
               (unless (and (eql point :start)
                            (eql threat-event (odo:episode-end-event ep)))
                 ;; Otherwise, we need to iterate over all possible producers... this
                 ;; is the expensive case...
                 (let ((threat-var (odo:make-variable `(member ,@(when (or threat-guard (req-guard req))
                                                                   (list nil))
                                                               :after :before))))
                   (odo:state-space-add! csp threat-var)
                   ;; We don't care about this variable if the threat is inactive
                   ;; or the current req is inactive.
                   (cond
                     ((and threat-guard (req-guard req))
                      (odo:add-constraint! csp
                                           (odo:make-constraint
                                            (odo:<-> (odo:odo-equal threat-var nil)
                                                     (odo:or threat-guard (req-guard req))))))
                     (threat-guard
                      (odo:add-constraint! csp
                                           (odo:make-constraint
                                            (odo:<-> (odo:odo-equal threat-var nil)
                                                     threat-guard))))
                     ((req-guard req)
                      (odo:add-constraint! csp
                                           (odo:make-constraint
                                            (odo:<-> (odo:odo-equal threat-var nil)
                                                     (req-guard req))))))
                   (odo:add-constraint! csp
                                        (odo:make-constraint
                                         (odo:-> (odo:odo-equal threat-var :after)
                                                 (odo:simple-temporal
                                                  (ecase point
                                                    (:start
                                                     (odo:episode-start-event ep))
                                                    (:all
                                                     (odo:episode-end-event ep)))
                                                  threat-event
                                                  :lower-bound 7d-3))
                                         :annotations (list :threat t)))
                   (dolist (support possible-supports)
                     (destructuring-bind (support-event . support-reqs) support
                       (declare (ignore support-reqs))
                       (odo:add-constraint! csp
                                            (odo:make-constraint
                                             (odo:-> (odo:and
                                                      (odo:odo-equal threat-var :before)
                                                      (odo:odo-equal producer-var (odo:name support-event)))
                                                     (odo:simple-temporal
                                                      threat-event
                                                      support-event
                                                      :lower-bound 5d-3))
                                             :annotations (list :threat t)))))
                   ))))))))
    ;;(break "Supports: ~S" possible-supports)
    ))
