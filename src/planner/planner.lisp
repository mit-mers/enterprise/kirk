(in-package #:kirk-v2/planner)

(defclass planner (exec:simple-executive-pausable-planner-mixin)
  (;; Input
   (%goal
    :initarg :goal
    :reader planner-goal)
   ;; (solution-cb
   ;;  :initarg :solution-cb
   ;;  :reader planner-solution-cb)
   (%donep
    :initform nil
    :accessor planner-done-p)

   (%goal-state-plan
    :reader planner-goal-state-plan)
   (%init-csp
    :reader planner-init-csp)
   (%csp-solver
    :reader planner-csp-solver)

   (%episode-true-bounds
    :initform (make-hash-table)
    :reader planner-episode-true-bounds)

   ;; (init-csp
   ;;  :initarg :init-csp
   ;;  :reader planner-init-csp)

   ;; (current-csp
   ;;  :accessor planner-current-csp)
   ;; (frames-ht
   ;;  :initform (make-hash-table)
   ;;  :reader planner-frames-ht)

   ;; (model-modification-hooks
   ;;  :initform nil
   ;;  :accessor planner-model-modification-hooks)
   ;; (objective-functions
   ;;  :initform nil
   ;;  :accessor planner-objective-functions)
   ;; (objective-vars-with-coeffs
   ;;  :initform nil
   ;;  :accessor planner-objective-vars-with-coeffs)

   ;; (subplanner-urls
   ;;  :initform nil
   ;;  :initarg :subplanner-urls
   ;;  :accessor planner-subplanner-urls)
   ))

(defmethod make-goal-state-plan ((goal exec:state-plan-goal))
  (exec:state-plan-of goal))

(defmethod slot-unbound (class (planner planner) (slot-name (eql '%goal-state-plan)))
  (setf (slot-value planner slot-name)
        (make-goal-state-plan (planner-goal planner))))

(defmethod slot-unbound (class (planner planner) (slot-name (eql '%init-csp)))
  (setf (slot-value planner slot-name)
        (state-plan-to-csp planner (planner-goal-state-plan planner))))

(defmethod slot-unbound (class (planner planner) (slot-name (eql '%csp-solver)))
  (setf (slot-value planner slot-name)
        (opsat-v3:make-csp-solver (planner-init-csp planner)
                                  :search-method :depth-first ;; :branch-and-bound
                                  :search-method-initargs '(:split-on-constraints-p nil)
                                  :constraint-system-description opsat-v3/all:*temporal-network-solving-constraint-system-description*
                                  :temporal-network-solver :stnu-dynamic-controllability
                                  :model-modification-callback
                                  (lambda (csp cost)
                                    (format t "~S~%" cost)
                                    (model-modification-callback planner csp cost)))))

;; (defun dist (from to)
;;   (let* ((from-x (slot-value from 'kirk-v2/examples/pr-2022/simple::x))
;;          (from-y (slot-value from 'kirk-v2/examples/pr-2022/simple::y))
;;          (to-x (slot-value to 'kirk-v2/examples/pr-2022/simple::x))
;;          (to-y (slot-value to 'kirk-v2/examples/pr-2022/simple::y))
;;          (proj-output
;;            (uiop:run-program `("geod" "+ellps=WGS84" "-I" "+units=m")
;;                              :input (list (format nil "~F ~F ~F ~F~%" from-x from-y to-x to-y))
;;                              :output '(:string :stripped t))))
;;     (read-from-string (third (ss:split-sequence #\Tab proj-output :remove-empty-subseqs t)))))

;; (defun compute-travel-time (ep)
;;   (uiop:nest
;;    (let ((name (odo:episode-name ep))))
;;    (when (string-equal (first name) 'transit))
;;    (let* ((from (second name))
;;           (to (third name))
;;           (dist (dist from to)))
;;      (values (/ dist 0.32d0)
;;              from to))))

(defun model-modification/true-bounds (planner csp)
  ;; Loop over all episodes. If one is newly active, compute its true bounds.
  (let ((episode-iter (odo:goal-episode-iterator (planner-goal-state-plan planner))))
    (loop
      (when (odo:it-done episode-iter)
        (return))
      (let* ((episode (odo:it-next episode-iter))
             (guard (odo:annotation episode :kirk/guard)))
        (flet ((copy-var (in-var)
                 (odo:state-space-get csp (odo:name in-var))))
          (declare (dynamic-extent #'copy-var))
          (let ((new-guard (unless (null guard) (odo:copy guard :var-map-fun #'copy-var))))
            ;;(break "~S ~S" (odo:episode-name episode) guard)

            (when (or (null new-guard)
                      (odo:eval-expression new-guard))
              ;;              (break "HERE")
              (when (null (gethash episode (planner-episode-true-bounds planner)))
                (alex:if-let ((fun (odo:annotation episode :kirk/subplanner)))
                  (multiple-value-bind (lb ub reward from) (funcall fun episode)
                    ;;(break "TIME: ~S ~S" reward (odo:episode-name episode))
                    (setf (gethash episode (planner-episode-true-bounds planner)) (list lb ub))
                    ;; FIXME: There's a bug in opsat when canonicalizing
                    ;; constraints that require the addition of a new
                    ;; variable.
                    (let ((new-var (odo:make-boolean-variable)))
                      (odo:state-space-add! csp new-var)
                      (odo:add-constraint! csp (odo:make-constraint
                                                (odo:<->
                                                 new-guard
                                                 new-var)))
                      (odo:add-constraint! csp (odo:make-constraint
                                                (odo:->
                                                 new-var
                                                 (odo:simple-temporal (odo:episode-start-event episode)
                                                                      (odo:episode-end-event episode)
                                                                      :lower-bound lb
                                                                      :upper-bound ub))
                                                :contingent-p t))
                      ;; Reward/distance travelled (really TIME travelled).
                      (odo:add-constraint! csp (odo:make-constraint
                                                (odo:->
                                                 new-var
                                                 (odo:odo-equal
                                                  (odo:state-space-get csp `("distance" ,from))
                                                  reward))))))
                  (setf (gethash episode (planner-episode-true-bounds planner)) t))))))))))

(defun model-modification/causal-links (planner csp)
  )
(defun model-modification-callback (planner csp cost)
  (declare (ignore cost))
  (model-modification/true-bounds planner csp)
  ;;(break "MOD CALLBACK ~S ~S" planner csp)
  )

(defun make-planner-csp (planner)
  (let ((goal-state-plan (planner-goal-state-plan planner)))
    (state-plan-to-csp planner goal-state-plan)))

(defun my-test (planner)
  (make-planner-csp planner))

(defun make-planner (goal)
  (make-instance 'planner
                 :goal goal))

(defmethod csp-solution-to-state-plan (planner sol)
  (let ((orig-state-plan (planner-goal-state-plan planner)))
    (let ((out (odo:make-state-plan)))
      (flet ((var-map-fun (var)
               (or (odo:state-space-get out (odo:name var) nil)
                   (let ((csp-var (odo:state-space-get sol (odo:name var) nil)))
                     (when (odo:event-p var)
                       (odo:make-event :name (odo:name var)
                                       :uncontrollable-p (odo:uncontrollable-p var)
                                       :conditional-p (odo:conditional-p var)
                                       :annotations (copy-list (odo:annotation-plist var))
                                       :min (odo:min-val csp-var)
                                       :max (odo:max-val csp-var))))
                                       ;; :gamma (odo:gamma var))))
                   (odo:state-space-get sol (odo:name var) nil))))
        ;; Copy over the state space.
        (dolist (v (odo:state-space-list orig-state-plan))
          (odo:state-space-add! out (odo:copy v :var-map-fun #'var-map-fun)))
        ;; Copy over all constraints.
        (dolist (c (odo:constraint-list orig-state-plan))
          (odo:add-constraint! out (odo:copy c :var-map-fun #'var-map-fun)))
        ;; Copy over all episodes, if they are active.
        (dolist (ep (odo:value-episode-list orig-state-plan))
          (let ((guard (odo:copy (odo:annotation ep :kirk/guard) :var-map-fun #'var-map-fun)))
            (when (or (null guard) (odo:eval-expression guard))
              (odo:add-value-episode! out (odo:copy ep :var-map-fun #'var-map-fun)))))
        (dolist (ep (odo:goal-episode-list orig-state-plan))
          (let ((guard (odo:copy (odo:annotation ep :kirk/guard) :var-map-fun #'var-map-fun)))
            (when (or (null guard) (odo:eval-expression guard))
              (odo:add-goal-episode! out (odo:copy ep :var-map-fun #'var-map-fun)))))

        ;; Copy over any new temporal constraints corresponding to threats and
        ;; support.
        (dolist (c (odo:constraint-list sol))
          (let ((causal-link-p (odo:annotation c :causal-link))
                (threat-p (odo:annotation c :threat)
                          ))
            (when (or causal-link-p threat-p)
              (let* ((expr (odo:constraint-expression c))
                     (operator (odo:operator expr))
                     ;; Semi-hacky... Assumes that all threats and causal links are
                     ;; guarded, which they currently are.
                     (guard (when (eql operator 'odo:->) (first (odo:application-arguments expr))))
                     (stc-expression (if (eql operator 'odo:->)
                                         (second (odo:application-arguments expr))
                                         expr)))
                (when (or (null guard) (odo:eval-expression guard :state-space sol))
                  ;; HACK: Why does COPY not work???
                  (let ((from (var-map-fun (odo:application-argument-value stc-expression 'odo:from)))
                        (to (var-map-fun (odo:application-argument-value stc-expression 'odo:to)))
                        (lb (odo:application-argument-value stc-expression 'odo:lower-bound))
                        (ub (odo:application-argument-value stc-expression 'odo:upper-bound)))
                    (odo:add-constraint! out (odo:make-constraint
                                              (odo:simple-temporal
                                               from to :lower-bound lb :upper-bound ub)
                                              :annotations (list :causal-link causal-link-p
                                                                 :threat threat-p)))
                    ;;(break "HERE ~A ~A ~A ~A" from to lb ub)
                    ))))))
        out))))

(defun next-plans (planner)
  (let ((solver (planner-csp-solver planner)))
    (when (not (planner-done-p planner))
      (let ((csp-solution (opsat-v3:next-solution solver)))
        (unless (null csp-solution)
          (list (csp-solution-to-state-plan planner csp-solution)))))))

;; (defun next-plans (planner)
;;   (let* ((state-plan (planner-goal-state-plan planner))
;;          (d-graph (sched-utils::make-distance-graph-from-state-plan state-plan))
;;          (done-p (planner-done-p planner)))
;;     (when (and (not done-p)
;;                (ignore-errors (sched-utils::apsp-graph d-graph)))
;;       (setf (planner-done-p planner) t)
;;       (list state-plan))))

;; (defun %run-csp! (planner solution-cb
;;                   &key
;;                     max-num-solutions
;;                     conflict-cb)
;;   (handler-bind
;;       ((conflict-found (lambda (e)
;;                          (when conflict-cb
;;                            (let* ((constraints (remove-if-not
;;                                                 (lambda (x)
;;                                                   (and (not (listp x))
;;                                                        (odo:annotation x :id)))
;;                                                 (conflict-found/conflict e)))
;;                                   (assignments (remove-if-not
;;                                                 (lambda (x)
;;                                                   (and (listp x)
;;                                                        (odo:annotation (second x) :id)))
;;                                                 (conflict-found/conflict e)))
;;                                   (c-ids (mapcar (lambda (x) (odo:annotation x :id)) constraints))
;;                                   (assignment-ids (mapcar (lambda (x)
;;                                                             (odo:annotation (second x) :id))
;;                                                           assignments)))
;;                              (funcall conflict-cb (append c-ids assignment-ids)))))))
;;     (let ((solver (opsat-v3:make-csp-solver
;;                    (planner-init-csp planner)
;;                    :search-method-initargs '(:split-on-constraints-p nil)
;;                    :numeric-solver nil
;;                    :model-modification-callback (model-modification-hook-runner planner)
;;                    :search-method (if (eql (odo:objective-goal (planner-init-csp planner)) :satisfy)
;;                                       :depth-first
;;                                       :branch-and-bound)))
;;           (num-solutions 0))

;;       (iter
;;         (for sol := (opsat-v3:next-solution solver))
;;         (unless sol
;;           (return :no-more-solutions))
;;         (incf num-solutions)
;;         (funcall solution-cb (solution-to-ccqsp-json-obj sol) sol)
;;         (when (and max-num-solutions
;;                    (= num-solutions max-num-solutions))
;;           (return :max-solutions))))))
