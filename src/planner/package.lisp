(uiop:define-package #:kirk-v2/planner
  (:use #:cl
        #:anaphora
        #:iterate
        ;; #:kirk-v2/scheduler/floyd-warshall
        ;; #:kirk-v2/scheduler/stn
        ;; #:kirk-v2/planner/planner
        ;; #:kirk-v2/planner/state-plan
        #:rmpl/automata/odo-automata
        #:rmpl/state-plan/state-plan)
  (:import-from #:bordeaux-threads)
  (:local-nicknames (#:sched #:kirk-v2/scheduler)
                    (#:sched-utils #:%kirk-v2/scheduler/utils)
                    (#:exec #:mtk-executive)
                    (#:alex #:alexandria)
                    (#:ss #:split-sequence))
  (:export #:make-planner
           #:make-rmpl-planner
           #:next-plans
           #:run-planner
           #:start-planner))

(in-package #:kirk-v2/planner)
