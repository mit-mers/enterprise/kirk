(in-package #:kirk-v2/planner)

(defun evaluate-reward-thunk (planner csp thunk)
  (declare (ignore planner))
  (cond
    ((or (functionp thunk)
         (symbolp thunk))
     (funcall thunk csp))
    ((odo:attribute-objective-function-p thunk)
     (let ((alist (odo:attribute-objective-attribute-values-alist thunk)))
       (loop
         :for (var . vals) :in alist
         :for actual-var := (odo:state-space-get csp (odo:name var))
         :if (odo:assigned-p actual-var)
           :sum (alex:assoc-value vals (odo:assigned-value actual-var))
         :else
           :sum (reduce #'max vals :key #'cdr))))
    ;; ((and (consp thunk)
    ;;       (eql (car thunk) :spock-eval))
    ;;  (let* ((sub-state-plan (funcall (cdr thunk) csp))
    ;;         (external-state-plan (odo:externalize sub-state-plan :format :jsown))
    ;;         (spock-request (jsown:new-js
    ;;                          ("goalPlan" external-state-plan)))
    ;;         (spock-url (getf (planner-subplanner-urls planner) :spock)))
    ;;    (format t "~S~%" spock-url)
    ;;    (format t "~S~%" spock-request)
    ;;    (spock-evaluate-state-plan spock-request spock-url)))
    (t
     (error "Unknown reward thunk"))))

;; translates a state plan to opsat encoding

(defun state-plan-to-csp (planner state-plan)
  (values
   (aprog1 (odo:make-csp)
     ;; (setf (odo:objective-function it)
     ;;       (lambda (csp)
     ;;         (list
     ;;          (reduce #'+
     ;;                  (mapcar (alex:curry #'evaluate-reward-thunk planner csp)
     ;;                          (odo:annotation state-plan
     ;;                                          :kirk/internal/objective-functions)))
     ;;          (- (odo:min-val (odo:state-space-get csp "distance"))))))
     ;; (setf (odo:objective-goal it) :maximize)
     ;; (setf (odo:annotation it :kirk/internal/bounding-function)
     ;;       (lambda (csp)
     ;;         (list
     ;;          (reduce #'+
     ;;                  (mapcar (alex:curry #'evaluate-reward-thunk planner csp)
     ;;                          (odo:annotation state-plan
     ;;                                          :kirk/internal/bounding-functions)))
     ;;          (- (odo:min-val (odo:state-space-get csp "distance"))))))
     ;; First, loop over everything in the state space and add everything that is
     ;; not a state variable.
     ;;(break "here")
     (dolist (ep (odo:value-episode-list state-plan))
       (let ((start (odo:start-event ep))
             (end (odo:end-event ep))
             ;; This copy probably needs to be applied to all guards??
             (guard (odo:copy (odo:annotation ep :kirk/guard)
                                  :var-map-fun (lambda (old-var)
                                                 (odo:state-space-get state-plan (odo:name old-var))))))
         (dolist (sc (odo:start-constraints ep))
           (annotate-event-with-produces! start sc guard))
         (dolist (sc (odo:end-constraints ep))
           ;;(break)
           ;;(when guard (break "~S" guard))
           (annotate-event-with-produces! end sc
                                          ;;nil
                                          guard
                                          ))))
     (let ((start-event (odo:start-event state-plan))
           (default (gensym)))
       (dolist (state-var (odo:state-space-list state-plan))
         (when (odo:state-variable-p state-var)
           (let ((initial-value (odo:annotation state-var :rmpl/initial-value default)))
             (unless (eql initial-value default)
               (annotate-event-with-produces!
                start-event
                (odo:make-state-constraint (odo:odo-equal (odo:@ state-var odo:+current-time+)
                                                          initial-value))))))))
     (dolist (obj (odo:state-space-list state-plan))
       (unless (odo:state-variable-p obj)
         (odo:state-space-add! it obj)))

     ;; Then loop over all the constraints and add them
     (dolist (constraint (odo:constraint-list state-plan))
       (odo:add-constraint! it constraint))
     ;; Then loop over all the episodes and try to add them.
     (dolist (ep (odo:goal-episode-list state-plan))
       ;; Each episode implies a [0,inf] stc
       (let* ((conditional-p (odo:conditional-p ep))
              (guard (odo:annotation ep :kirk/guard))
              (stp (odo:simple-temporal
                    (odo:start-event ep)
                    (odo:end-event ep)
                    :lower-bound 0))
              (stc (or (odo:temporal-constraint ep)
                       (odo:make-constraint (if conditional-p
                                                (odo:-> guard stp)
                                                stp)))))
         ;; (setf (odo:annotation stc :episode-p) t
         ;;       (odo:annotation stc :episode-activity-name) (odo:episode-activity-name ep)
         ;;       (odo:annotation stc :episode-activity-args) (odo:episode-activity-args ep))
         (odo:add-constraint! it stc)
         (let ((start-constraints (odo:start-constraints ep))
               (over-all-constraints (odo:over-all-constraints ep))
               (end-constraints (odo:end-constraints ep))
               (guard (odo:copy (odo:annotation ep :kirk/guard)
                                    :var-map-fun (lambda (old-var)
                                                   (odo:state-space-get state-plan (odo:name old-var))))))
           (dolist (sc start-constraints)
             (let* ((expr (odo:constraint-expression sc))
                    (scope (odo:expression-scope expr)))
               (if (every #'odo:variable-p scope)
                   (odo:add-constraint! it (odo:make-constraint expr))
                   (add-causal-links! it state-plan ep :start sc guard))))
           (dolist (oc over-all-constraints)
             (let* ((expr (odo:constraint-expression oc))
                    (scope (odo:expression-scope expr)))
               (if (every #'odo:variable-p scope)
                   (odo:add-constraint! it (odo:make-constraint expr))
                   (add-causal-links! it state-plan ep :all oc guard))))
           (dolist (sc end-constraints)
             (let* ((expr (odo:constraint-expression sc))
                    (scope (odo:expression-scope expr)))
               (if (every #'odo:variable-p scope)
                   (odo:add-constraint! it (odo:make-constraint expr))
                   (add-causal-links! it state-plan ep :end sc guard))))))))

   state-plan))
