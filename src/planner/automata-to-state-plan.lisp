;;;; This file is a hierarchical temporal constraint automata evaluator. It is
;;;; responsible for walking a control program (expressed as an HTCA) and
;;;; creating a state plan expressing the run.

(in-package #:kirk-v2/planner)

(defvar *event-map*)

(defun ensure-event (start-or-end location &optional uncontrollable-p)
  (check-type start-or-end (member :start :end))
  (multiple-value-bind (existing exists-p) (gethash (list start-or-end location) *event-map*)
    (if exists-p
        (progn
          (assert (or (not uncontrollable-p)
                      (equal uncontrollable-p (odo:event-uncontrollable-p existing))))
          existing)
        (setf (gethash (list start-or-end location) *event-map*)
              (odo:make-event :uncontrollable-p uncontrollable-p)))))

(defun insert-automaton-into-state-plan! (automaton state-plan guards)
  (let ((odo-type (odo:odo-type automaton)))
    (%insert-automaton-into-state-plan! automaton odo-type state-plan guards)))

(defun make-guarded-constraint (expr guards &key contingent-p annotations)
  (odo:make-constraint (if guards
                           (odo:-> (apply #'odo:and guards) expr)
                           expr)
                       :contingent-p contingent-p
                       :annotations annotations))

(defgeneric %insert-automaton-into-state-plan! (automaton type state-plan guards))

(defmethod %insert-automaton-into-state-plan! (automaton
                                               (type odo:odo-hierarchical-automaton-all)
                                               state-plan
                                               guards)
  (let ((start-event (ensure-event :start automaton))
        (end-event (ensure-event :end automaton)))
    (odo:add-constraint! state-plan (make-guarded-constraint
                                     (odo:simple-temporal start-event end-event
                                                          :lower-bound 0)
                                     guards))
    (iter
      (for child-location :in (location-locations automaton))
      (odo:add-constraint! state-plan (make-guarded-constraint
                                       (odo:simple-temporal start-event
                                                            (ensure-event :start child-location)
                                                            :lower-bound 0
                                                            :upper-bound 0)
                                       guards))
      (odo:add-constraint! state-plan (make-guarded-constraint
                                       (odo:simple-temporal (ensure-event :end child-location)
                                                            end-event
                                                            :lower-bound 0
                                                            :upper-bound 0)
                                       guards))
      (insert-automaton-into-state-plan! child-location state-plan guards))))

(defmethod %insert-automaton-into-state-plan! (automaton
                                               (type odo:odo-hierarchical-automaton-xor)
                                               state-plan
                                               guards)
  ;; FIXME: Only handles a simple sequence currently
  (let ((start-event (ensure-event :start automaton))
        (end-event (ensure-event :end automaton))
        ;; FIXME: Odo should export a similar function...
        (entry-location (location-default-entry automaton)))

    (when (odo:annotation automaton :rmpl/has-temporal-constraint)
      (let ((lower-bound (or (odo:annotation automaton :rmpl/temporal-constraint-lower-bound)
                             0))
            (upper-bound (or (odo:annotation automaton :rmpl/temporal-constraint-upper-bound)
                             '*))
            (annotations))
        (awhen (odo:annotation automaton :rmpl/lb-relaxable-p)
          (setf (getf annotations :lb-relaxable-p) it))
        (awhen (odo:annotation automaton :rmpl/ub-relaxable-p)
          (setf (getf annotations :ub-relaxable-p) it))
        (awhen (odo:annotation automaton :rmpl/lb-cost-ratio)
          (setf (getf annotations :lb-cost-ratio) it))
        (awhen (odo:annotation automaton :rmpl/ub-cost-ratio)
          (setf (getf annotations :ub-cost-ratio) it))
        (odo:add-constraint! state-plan (make-guarded-constraint
                                         (odo:simple-temporal start-event end-event
                                                              :lower-bound lower-bound
                                                              :upper-bound upper-bound)
                                         guards
                                         :annotations annotations))))
    (odo:add-constraint! state-plan (make-guarded-constraint
                                     (odo:simple-temporal start-event end-event
                                                          :lower-bound 0)
                                     guards))
    (odo:add-constraint! state-plan (make-guarded-constraint
                                     (odo:simple-temporal start-event (ensure-event :start entry-location)
                                                          :lower-bound 0
                                                          :upper-bound 0)
                                     guards))

    (let ((visited (make-hash-table :test 'eq))
          (ending-locs nil))
      (labels ((get-next-locs-and-guards (current-loc)
                 (let ((transitions (odo:location-outgoing-transitions current-loc)))
                   (mapcar #'(lambda (transition)
                               (let ((branches (odo:transition-branches transition)))
                                 (assert (alex:length= 1 branches))
                                 (cons (odo:branch-target (first branches))
                                       (odo:transition-guard transition))))
                           transitions)))

               (chase (loc guards)
                 (when (and (gethash loc visited)
                            (not (member loc ending-locs)))
                   (error "Can't currently handle multiple paths to the same location"))
                 (unless (gethash loc visited)
                   (setf (gethash loc visited) t)
                   (insert-automaton-into-state-plan! loc state-plan guards)
                   (let ((locs-and-guards (get-next-locs-and-guards loc))
                         (previous-event (ensure-event :end loc)))
                     (dolist (loc-and-guard locs-and-guards)
                       (destructuring-bind (next-loc . next-guard)
                           loc-and-guard
                         (let ((guards (if next-guard
                                           (list* (odo:constraint-expression next-guard) guards)
                                           guards)))
                           (odo:add-constraint! state-plan (make-guarded-constraint
                                                            (odo:simple-temporal previous-event
                                                                                 (ensure-event
                                                                                  :start next-loc)
                                                                                 :lower-bound 0)
                                                            guards))
                           (chase next-loc guards))))
                     (unless locs-and-guards
                       (push loc ending-locs))))))
        (chase entry-location guards))
      (assert (every (lambda (x) (eql x (first ending-locs))) (rest ending-locs)))
      (odo:add-constraint! state-plan (make-guarded-constraint
                                       (odo:simple-temporal (ensure-event :end (first ending-locs))
                                                            end-event
                                                            :lower-bound 0)
                                       guards)))
    ;; (flet ((get-next-loc (current-loc)
    ;;          (let ((transitions (odo:location-outgoing-transitions current-loc)))
    ;;            (break "~S" transitions)
    ;;            (when transitions
    ;;              (assert (length= 1 transitions))
    ;;              (let* ((transition (first transitions))
    ;;                     (branches (odo:transition-branches transition)))
    ;;                (assert (length= 1 branches))
    ;;                (odo:branch-target (first branches)))))))
    ;;   (iter
    ;;     (for previous-loc :initially entry-location :then next-loc)
    ;;     (for previous-event :initially start-event :then (ensure-event :end next-loc))
    ;;     (for next-loc := (get-next-loc previous-loc))
    ;;     (while next-loc)
    ;;     (insert-automaton-into-state-plan! next-loc state-plan guards)
    ;;     (for next-event := (ensure-event :start next-loc))
    ;;     (odo:add-constraint! state-plan (make-guarded-constraint
    ;;                                      (odo:simple-temporal previous-event
    ;;                                                           next-event
    ;;                                                           :lower-bound 0)
    ;;                                      guards))
    ;;     (finally
    ;;      (odo:add-constraint! state-plan (make-guarded-constraint
    ;;                                       (odo:simple-temporal previous-event
    ;;                                                            end-event
    ;;                                                            :lower-bound 0
    ;;                                                            :upper-bound 0)
    ;;                                       guards)))))
    ))

(defmethod %insert-automaton-into-state-plan! (automaton
                                               (type odo:odo-primitive-automaton)
                                               state-plan
                                               guards)
  ;; Check if this is a primitive function call
  (let ((function-name (odo:annotation automaton :rmpl/primitive-function-name))
        (args (odo:annotation automaton :rmpl/primitive-function-args)))
    (when function-name
      ;; FIXME: Need some better way to pass around methods and their conditions,
      ;; effects, etc.
      ;;(break "handling function primitive")
      (cond
        ((eql function-name 'rmpl/lang:noop)
          (odo:add-goal-episode! state-plan
                                 (odo:make-episode :start-event (ensure-event :start automaton)
                                                   :end-event (ensure-event :end automaton)
                                                   :activity-name function-name
                                                   :activity-args args)))
        ((string-equal function-name :pick-and-place)
         (break))
        ((string-equal function-name :transit)
         (let ((start-event (ensure-event :start automaton))
               (end-event (ensure-event :end automaton t))
               ;;(method (odo:annotation automaton :rmpl/primitive-function-method))
               )
           (odo:add-goal-episode! state-plan
                                  (odo:make-episode :start-event start-event
                                                    :end-event end-event
                                                    :conditional-p (not (null guards))
                                                    :activity-name function-name
                                                    :activity-args args
                                                    ;; ;; FIXME
                                                    ;; :start-constraints
                                                    ;; (awhen (primitive-method-requires-to-state-constraint method args state-plan)
                                                    ;;   (list it))
                                                    :annotations (when guards
                                                                   (list :kirk/guard (apply #'odo:and guards)))
                                                    :temporal-constraint
                                                    (make-guarded-constraint
                                                     (odo:simple-temporal start-event
                                                                          end-event
                                                                          :lower-bound 60
                                                                          :upper-bound 70)
                                                     guards :contingent-p t)))))
        (t
         (let ( ;;(method (odo:annotation automaton :rmpl/primitive-function-method))
               )
            (odo:add-goal-episode! state-plan
                                   (odo:make-episode :start-event (ensure-event :start automaton)
                                                     :end-event (ensure-event :end automaton)
                                                     :conditional-p (not (null guards))
                                                     :activity-name function-name
                                                     :activity-args args
                                                     ;; FIXME
                                                     ;; :start-constraints
                                                     ;; (awhen (primitive-method-requires-to-state-constraint method args state-plan)
                                                     ;;   (list it))
                                                     :annotations (when guards
                                                                    (list :kirk/guard (apply #'odo:and guards)))))))))))

;; FIXME: Need to specially handle primitive locations outside of this function
;; (or change the name of this function)
(defmethod %insert-automaton-into-state-plan! (location
                                               (type odo:odo-primitive-location)
                                               state-plan
                                               guards)
  (odo:add-goal-episode! state-plan
                         (odo:make-episode :start-event (ensure-event :start location)
                                           :end-event (ensure-event :end location)
                                           :over-all-constraints (odo:location-constraints location))))

(defmethod %insert-automaton-into-state-plan! :around (location
                                                       type
                                                       state-plan
                                                       guards)
  (let ((out
          (aif (odo:annotation location :kirk/custom-state-plan-inserter)
               (funcall it state-plan (ensure-event :start location) (ensure-event :end location)
                        guards)
               (call-next-method))))
    (awhen (odo:annotation location :kirk/custom-reward)
      (push it (odo:annotation state-plan :kirk/internal/objective-functions))
      (push it (odo:annotation state-plan :kirk/internal/bounding-functions)))
    out))

(defun automaton-to-state-plan (automaton ;;state-space
                                )
  (let ((*event-map* (make-hash-table :test 'equal))
        (start-event (odo:make-event :value 0)))
    (setf (gethash (list :start automaton) *event-map*) start-event)
    (aprog1 (odo:make-state-plan :start-event start-event)
      (insert-automaton-into-state-plan! automaton it nil)))

  ;; (let ((*event-map* (make-hash-table :test 'equal))
  ;;       (start-event (odo:make-event :value 0)))
  ;;   (setf (gethash (list :start automaton) *event-map*) start-event)

  ;;   (aprog1 (odo:make-state-plan :state-space state-space
  ;;                                :start-event start-event)
  ;;     (insert-automaton-into-state-plan! automaton it nil)))
  )

(defmethod make-goal-state-plan ((goal exec:rmpl-control-program-goal))
  (control-program-to-state-plan (exec:goal-function-of goal)
                                 (exec:goal-function-args-of goal))
  ;; (let ((htca (rmpl:location-for-control-program (goal-function goal) (goal-function-args goal))))
  ;;   (values (automaton-to-state-plan htca) htca))
  ;; (multiple-value-bind (hca state-space)

  ;;     (rmpl:make-control-program-htca (goal-function goal) (goal-function-args goal))
  ;;   (values (automaton-to-state-plan hca state-space) hca))
  )
