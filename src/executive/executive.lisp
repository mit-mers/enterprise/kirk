(in-package #:kirk-v2/executive)

;; (defsection @kirk-v2/executive (:title "Kirk Executive")
;;   "This section provides an implementation of the Kirk executive. This
;;   executive ties together Kirk's planning and dispatching capabilities and
;;   exposes them via the @KIRK-V2/EXECUTIVE-API."
;;   (kirk-executive class))

(exec:define-executive-class kirk-executive (exec:simple-executive)
  ((child-executive
    :type executive
    :initarg :child-executive))
  (:default-initargs
   :name "kirk")
  (:goal-types rmpl-control-program-goal))

(defclass dispatcher ()
  ((%clock
    :initarg :clock)
   (%child-executive
    :initarg :child-executive)
   (%context
    :initarg :context))
  (:documentation
   "The dispatcher implemented by KIRK-V2/DISPATCHER is tied to a specific
plan. The dispatcher needed by MTK-EXECUTIVE is not. This class bridges the
gap."))

(defmethod exec:simple-executive-make-planner ((executive kirk-executive) context &key)
  (planner::make-planner (exec:goal-of context)))

(defmethod exec:simple-executive-planner-next-plans ((planner planner::planner) &key)
  (planner::next-plans planner))

(defmethod exec:simple-executive-make-dispatcher ((executive kirk-executive) context)
  (make-instance 'dispatcher
                 :clock (exec:clock-of executive)
                 :child-executive (slot-value executive 'child-executive)
                 :context context))

(defmethod exec:execute-plan! ((dispatcher dispatcher) plan &key)
  (let* ((driver (make-instance 'dispatcher::mtk-executive-event-driver
                                :child-executive (slot-value dispatcher '%child-executive)
                                :context (slot-value dispatcher '%context)
                                :plan (first plan)))
         (real-dispatcher (make-instance 'dispatcher::event-dispatcher
                                         :clock (slot-value dispatcher '%clock)
                                         :driver driver
                                         :plan (first plan))))
    (dispatcher::run-dispatcher real-dispatcher)))
