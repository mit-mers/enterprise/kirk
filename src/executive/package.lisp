(uiop:define-package #:kirk-v2/executive
  (:use #:cl)
  (:local-nicknames (#:exec #:mtk-executive)
                    (#:planner #:kirk-v2/planner)
                    (#:dispatcher #:kirk-v2/dispatcher))
  (:export #:kirk-executive))

(in-package #:kirk-v2/executive)
