(uiop:define-package #:kirk-v2/kirk-v2

  (:use #:cl
        #:kirk-v2/doc
        #:kirk-v2/executive))

(in-package #:kirk-v2/kirk-v2)

;; (defsection @kirk-v2 (:title "Kirk"
;;                       :export-children t)
;;   (@kirk-v2/executive section))
