;;;; Documentation generation for Kirk.

(uiop:define-package #:kirk-v2/doc-gen
    (:use #:cl
          #:alexandria
          #:mgl-pax)
  (:import-from #:kirk-v2/doc
                #:*sections*
                #:*mgl-pax-symbol-list*

                #:@kirk-v2
                #:@kirk-v2-dev)
  (:import-from #:mgl-pax/full)
  ;; These are symbols directly from this package.
  (:export #:update-html))

(in-package #:kirk-v2/doc-gen)

(defun mgl-sym (s)
  "Look up S in mgl-pax."
  (uiop:find-symbol* s :mgl-pax))

(defparameter *kirk-v2-doc-resources* '("jquery.min.js" "toc.min.js" "style.css"))

(defun eval-defsection (section)
  (let ((section section))
    (dolist (sym *mgl-pax-symbol-list*)
      (setf section (subst (mgl-sym sym) sym section)))
    (eval `(let ((*package* *package*))
             (in-package ,(package-name (symbol-package (first section))))
             (,(mgl-sym 'defsection) ,@section))))
  nil)

(defun update-html (&optional (directory (asdf:system-relative-pathname :kirk-v2 "build/doc/")))
  (dolist (section *sections*)
    (eval-defsection section))
  (setf directory (uiop:ensure-directory-pathname directory))
  (ensure-directories-exist directory)
  (dolist (f *kirk-v2-doc-resources*)
    (uiop:copy-file (asdf:system-relative-pathname
                     :kirk-v2 (concatenate 'string "src/doc/" f))
                    (merge-pathnames f directory)))
  (let ((*document-uppercase-is-code* t)
        (*document-text-navigation* t))
    (document
     @kirk-v2
     :pages (loop for section in
                              (list ;;@kirk-v2-dev
                                    @kirk-v2)
                  collect (section-to-page-spec section directory))
     :format :html)))

(defun section-to-filename (section directory)
  (merge-pathnames (format nil "~a.html"
                           (string-downcase (substitute #\- #\/ (subseq (symbol-name (section-name section)) 1))))
                   directory))

(defun section-to-page-spec (section directory &key
                                                 (open-args '(:if-does-not-exist :create
                                                              :if-exists :supersede
                                                              :ensure-directories-exist t)))
  (flet ((header (title stream)
           (html-header stream :title title
                        :stylesheet "style.css" :charset "UTF-8"))
         (footer (stream)
           (html-footer stream)))
    `(:objects
      (,section)
      :output (,(section-to-filename section directory) ,@open-args)
      :header-fn ,(lambda (stream)
                          (header (section-title section) stream))
      :footer-fn ,#'footer)))

(defun html-header (stream &key title stylesheet (charset "UTF-8")
                             link-to-pax-world-p)
  (format
   stream
   "<!DOCTYPE html>~%~
   <html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>~%~
   <head>~%~
   ~@[<title>~A</title>~]~%~
   ~@[<link type='text/css' href='~A' rel='stylesheet'/>~]~%~
   ~@[<meta http-equiv=\"Content-Type\" ~
            content=\"text/html; ~
            charset=~A\"/>~]~%~
   <script src=\"jquery.min.js\"></script>~%~
   <script src=\"toc.min.js\"></script>~%~
   <script type=\"text/x-mathjax-config\">
     MathJax.Hub.Config({
       tex2jax: {
         inlineMath: [['$','$']],
         processEscapes: true
       },
       TeX: {
         extensions: [\"AMSmath.js\"]
       }
     });
   </script>
   <script type=\"text/javascript\" src=\"https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML\"></script>
   </head>~%~
   <body>~%~
   <div id=\"content-container\">~%~
     <div id=\"toc\">~%~
       ~:[~;<div id=\"toc-header\"><ul><li><a href=\"index.html\">~
            PAX World</a></li></ul></div>~%~]~
       <div id=\"page-toc\">~%~
       </div>~%~
       <div id=\"toc-footer\">~
         <ul><li><a href=\"https://github.com/melisgl/mgl-pax\">[generated ~
             by MGL-PAX]</a></li></ul>~
       </div>~%~
     </div>~%~
     <div id=\"content\">~%"
   title stylesheet charset link-to-pax-world-p))

(defun html-footer (stream)
  (format
   stream
   "  </div>~%~
   </div>~%~
   <script>$('#page-toc').toc(~A);</script>~
   </body>~%</html>~%"
   (toc-options)))

(defun toc-options ()
  (format nil "{'selectors': '~{~A~^,~}'}"
          (loop for i upfrom 1 upto (1+ *document-max-table-of-contents-level*)
                collect (format nil "h~S" i))))

(pushnew :kirk-v2/doc-gen *features*)
