(uiop:define-package #:kirk-v2/doc
    (:use #:cl)
  (:export #:generate-docs))

(in-package #:kirk-v2/doc)

(defun generate-docs ()
  (40ants-doc/builder:update-asdf-system-docs
   (list kirk-v2::@kirk
         kirk-v2/scheduler:@scheduler)
   :kirk-v2))
