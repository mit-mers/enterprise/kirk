(uiop:define-package #:kirk-v2-tests
    (:use #:cl
          #:alexandria
          #:anaphora
          #:fiveam
          #:iterate
          #:kirk-v2
          #:%kirk-v2/scheduler/dynamic)
    (:local-nicknames (#:utils #:%kirk-v2/scheduler/utils)
                      (#:sched #:kirk-v2/scheduler)
                      (#:disp #:kirk-v2/dispatcher)
                      (#:tc #:temporal-controllability)
                      (#:tn #:temporal-networks)
                      (#:exec #:mtk-executive))
    (:export #:make-variable-state-plan)
    (:import-from #:fiveam #:is #:is-true #:is-false))

(in-package #:kirk-v2-tests)
