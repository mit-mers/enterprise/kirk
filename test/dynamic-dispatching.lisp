(in-package #:kirk-v2-tests)

(fiveam:def-suite :kirk-v2/dynamic-dispatching :in :kirk-v2)
(fiveam:in-suite :kirk-v2/dynamic-dispatching)

;;; a driver that we can inspect during dispatching

(defclass mock-driver (disp::dispatch-driver)
  ((history
    :initform (make-hash-table :test #'equal)
    :accessor history-of)
   (clock
    :initarg :clock
    :initform nil
    :accessor clock-of
    :documentation "A clock to play with"))
  (:documentation "A mock driver that recalls what was dispatched, can update the clock, and can run additional
                   callbacks"))

(defmethod disp:execute-now! ((driver mock-driver) (item disp::batch-dispatch-events))
  "Follow the dispatcher's expected behavior, update the clock, and run test callbacks"
  (let* ((clock (clock-of driver))
         ;; use the simulated-clock-time instead of clock-timestamp because we'll be using the
         ;; `:simulated-clock' in tests and it's easier to compare integers than timestamps
         (time (exec::simulated-clock-time clock))
         (events (disp::events-of item)))

    ;; track what events were executed by the driver for testing
    (loop :for event-id :in events
          :do (setf (gethash event-id (history-of driver)) time))

    (disp:confirm-execution! driver events)))


;;; test behavior of the dispatcher

(fiveam:def-test dynamic-dispatcher-tick ()
  ;; test that each iteration of the dispatcher is doing what it should
  ;; we'll be stepping through the entire schedule of the coffee example

  (let* ((state-plan (make-coffee-state-plan "test morning coffee"))
         (clock (exec:make-clock :simulated-clock))
         (start-stamp (exec:clock-timestamp clock))
         (mailbox (safe-queue:make-mailbox))
         (driver (make-instance 'mock-driver :clock clock :mailbox mailbox))
         (dispatcher (make-instance 'disp:dynamic-dispatcher :plan state-plan
                                                             :epsilon 0 ; execute events at the exact RTED time
                                                             :driver driver
                                                             :clock clock
                                                             :mailbox mailbox))
                                                             ;; :verbose t
                                                             
         ;; this is normally managed by `run-dispatcher', but making it here allows us to inspect
         ;; the behavior of `tick'
         (status (disp::make-dispatch-status)))

    ;; setting the dispatcher's start stamp would normally be handled by `run-dispatcher'
    (setf (disp::start-timestamp-of dispatcher) start-stamp)

    (is-false (gethash "COFFEE-START" (history-of driver))
             "COFFEE-START should not have been executed")

    ;; t=0
    (is-true (disp::tick dispatcher status)
             "The first tick should return that the dispatcher can continue")

    (is-true (equal (gethash "COFFEE-START" (history-of driver)) 0)
             "COFFEE-START should have been executed at 0")

    (is-true (equal (first (sched::rted-events (disp::dispatch-status-next-rted status)))
                    (list "COFFEE-START" nil))
             "The last decision should be waiting for a message confirming execution")

    ;; t=1
    (exec:clock-sleep clock 1)

    (is-true (disp::tick dispatcher status)
             "The dispatcher should be able to continue")

    ;; this is the last message we received
    (destructuring-bind (message-type &rest list-of-rteds)
        (disp::dispatch-status-message status)
      ;; FYI: list-of-rteds is
      ;; (list (list (list event-id noop) execution-time))
      (is-true (and (equal message-type :executed)
                    (equal (first (first (first list-of-rteds))) "COFFEE-START"))
               "A message should have been sent saying COFFEE-START was executed"))

    (is-true (equal (first (sched::rted-events (disp::dispatch-status-next-rted status)))
                    (list "++LOWER-COFFEE-READY-REWOL++" t))
             "The next decision should be waiting")

    ;; t=30
    (exec:clock-sleep clock 29)

    (is-true (disp::tick dispatcher status)
             "The dispatcher should be able to continue")

    (is-true (equal (gethash "++LOWER-COFFEE-READY-REWOL++" (disp::history-of dispatcher)) 30)
             "++LOWER-COFFEE-READY-REWOL++ should have been dispatched at 30")

    (is-false (gethash "++LOWER-COFFEE-READY-REWOL++" (history-of driver))
             "++LOWER-COFFEE-READY-REWOL++ is a noop and should not have reached the driver")


    ;; t=31
    (exec:clock-sleep clock 1)

    (is-true (disp::tick dispatcher status)
             "The dispatcher should be able to continue")

    (is-false (gethash "COFFEE-READY" (disp::history-of dispatcher))
              "++LOWER-COFFEE-READY-REWOL++ should not have been dispatched")

    ;; t=35
    (exec:clock-sleep clock 4)

    (is-true (disp::tick dispatcher status)
             "The dispatcher should be able to continue")

    (is-true (equal (gethash "COFFEE-READY" (disp::history-of dispatcher)) 35)
             "COFFEE-READY should have been dispatched at 35")

    (is-false (gethash "COFFEE-READY" (history-of driver))
             "COFFEE-READY is a contingent event and should not have reached the driver")

    ;; t=50
    (exec:clock-sleep clock 15)

    (is-true (disp::tick dispatcher status)
             "The dispatcher should be able to continue")

    (is-true (equal (gethash "GET-COFFEE" (disp::history-of dispatcher)) 50)
             "GET-COFFEE should have been dispatched at 50")

    (is-true (equal (gethash "GET-COFFEE" (history-of driver)) 50)
             "GET-COFFEE should have been executed at 50")

    ;; t=55
    (exec:clock-sleep clock 5)

    (is-true (disp::tick dispatcher status)
             "The dispatcher should be able to continue")

    (is-true (equal (gethash "MEETING-START" (disp::history-of dispatcher)) 55)
             "MEETING-START should have been dispatched at 55")

    (is-true (equal (gethash "MEETING-START" (history-of driver)) 55)
             "MEETING-START should have been executed at 55")

    (is-false (disp::tick dispatcher status)
             "The dispatcher should be done")))

(fiveam:def-test dynamic-dispatcher-preempted-ctg-event ()
  ;; test that each iteration of the dispatcher is doing what it should

  (let* ((state-plan (make-coffee-state-plan "test morning coffee"))
         (clock (exec:make-clock :simulated-clock))
         (start-stamp (exec:clock-timestamp clock))
         (mailbox (safe-queue:make-mailbox))
         (driver (make-instance 'mock-driver :clock clock :mailbox mailbox))
         (dispatcher (make-instance 'disp:dynamic-dispatcher :plan state-plan
                                                             :epsilon 0 ; execute events at the exact RTED time
                                                             :driver driver
                                                             :clock clock
                                                             :mailbox mailbox))
                                                             ;; :verbose t
         (status (disp::make-dispatch-status)))

    (setf (disp::start-timestamp-of dispatcher) start-stamp)

    ;; t=0
    (is-true (disp::tick dispatcher status)
             "The first tick should return that the dispatcher can continue")

    ;; t=1
    (exec:clock-sleep clock 1)

    (is-true (disp::tick dispatcher status)
             "The dispatcher should be able to continue")

    ;; t=30
    (exec:clock-sleep clock 29)

    (is-true (disp::tick dispatcher status)
             "The dispatcher should be able to continue")

    ;; t=31
    (exec:clock-sleep clock 1)

    (is-true (disp::tick dispatcher status)
             "The dispatcher should be able to continue")

    ;; preempt the COFFEE-READY decision with an actual, observed contingent event

    ;; t=32
    (exec:clock-sleep clock 1)

    (disp:observe-event! dispatcher "COFFEE-READY")

    (is-true (disp::tick dispatcher status)
             "The dispatcher should be able to continue")

    ;; t=47
    (exec:clock-sleep clock 15)

    (is-true (disp::tick dispatcher status)
             "The dispatcher should be able to continue")

    (is-true (equal (gethash "GET-COFFEE" (disp::history-of dispatcher)) 47)
             "GET-COFFEE should have been dispatched at 47")

    (is-true (equal (gethash "GET-COFFEE" (history-of driver)) 47)
             "GET-COFFEE should have been executed at 47")))

(fiveam:def-test dynamic-dispatcher-early-ctg-event ()
  ;; test that each iteration of the dispatcher is doing what it should

  (let* ((state-plan (make-coffee-state-plan "test morning coffee"))
         (clock (exec:make-clock :simulated-clock))
         (start-stamp (exec:clock-timestamp clock))
         (mailbox (safe-queue:make-mailbox))
         (driver (make-instance 'mock-driver :clock clock :mailbox mailbox))
         (dispatcher (make-instance 'disp:dynamic-dispatcher :plan state-plan
                                                             :epsilon 0 ; execute events at the exact RTED time
                                                             :driver driver
                                                             :clock clock
                                                             :mailbox mailbox))
                                                             ;; :verbose t
         (status (disp::make-dispatch-status)))

    (setf (disp::start-timestamp-of dispatcher) start-stamp)

    ;; t=0
    (is-true (disp::tick dispatcher status)
             "The first tick should return that the dispatcher can continue")

    ;; t=1
    (exec:clock-sleep clock 1)

    (is-true (disp::tick dispatcher status)
             "The dispatcher should be able to continue")

    ;; t=25
    (exec:clock-sleep clock 24)

    ;; preempt ++LOWER-COFFEE-READY-REWOL++ and COFFEE-READY with an early ctg event
    (disp:observe-event! dispatcher "COFFEE-READY")

    (is-true (disp::tick dispatcher status)
             "The dispatcher should be able to continue")

    (is-false (gethash "++LOWER-COFFEE-READY-REWOL++" (disp::history-of dispatcher))
             "++LOWER-COFFEE-READY-REWOL++ should not have been executed")

    ;; t=30
    (exec:clock-sleep clock 5)

    (is-true (disp::tick dispatcher status)
             "The dispatcher should be able to continue")

    (is-true (equal (gethash "++LOWER-COFFEE-READY-REWOL++" (disp::history-of dispatcher)) 30)
             "++LOWER-COFFEE-READY-REWOL++ should have been executed")

    ;; t=35
    (exec:clock-sleep clock 5)

    (is-true (disp::tick dispatcher status)
             "The dispatcher should be able to continue")

    (is-false (gethash "COFFEE-READY" (disp::history-of dispatcher))
             "COFFEE-READY should not have been executed")

    ;; t=45
    (exec:clock-sleep clock 10)

    (is-true (disp::tick dispatcher status)
             "The dispatcher should be able to continue")

    (is-true (equal (gethash "GET-COFFEE" (disp::history-of dispatcher)) 45)
             "GET-COFFEE should have been dispatched at 45")

    (is-true (equal (gethash "GET-COFFEE" (history-of driver)) 45)
             "GET-COFFEE should have been executed at 45")))
