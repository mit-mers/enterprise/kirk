(uiop:define-package #:kirk-v2-test/morning-lecture
    (:use #:cl
          #:alexandria
          #:kirk-v2)
  (:import-from #:fiveam
                #:is))

(in-package #:kirk-v2-test/morning-lecture)

(fiveam:def-suite :kirk-v2)
(fiveam:def-suite :kirk-v2/morning-lecture :in :kirk-v2)

(fiveam:in-suite :kirk-v2/morning-lecture)

(defclass my-echo-executive (mtk-executive:echo-executive)
  ((events
    :initform (safe-queue:make-queue))))

(defclass my-echo-dispatcher ()
  ((executive
    :initarg :executive)
   (context
    :initarg :context)))

(defmethod mtk-executive:simple-executive-make-dispatcher ((executive my-echo-executive) context)
  (make-instance 'my-echo-dispatcher
                 :executive executive
                 :context context))

(defmethod mtk-executive:execute-plan! ((dispatcher my-echo-dispatcher) plan &key)
  (safe-queue:enqueue (odo:name (odo:start-event plan))
                      (slot-value (slot-value dispatcher 'executive) 'events))
  (mtk-executive:send-message (slot-value dispatcher 'context)
                              (make-instance 'mtk-executive:event-executed-message
                                             :event-name (odo:name (odo:start-event plan))
                                             :time (mtk-executive:clock-timestamp
                                                    (mtk-executive:clock-of
                                                     (slot-value dispatcher 'executive)))))
  t)

(defun drain-queue (q)
  (loop :for msg := (safe-queue:dequeue q)
        :until (null msg)
        :collect msg))

(fiveam:def-test morning-lecture ()

  (let ((rmpl:*environment* (rmpl:make-environment)))

    (rmpl:load-rmpl (asdf:system-relative-pathname :kirk-v2 "examples/morning-lecture/script.rmpl"))

    (let* ((clock (mtk-executive:make-clock '(:scaled-wall-clock :scale 10)))
           (child-executive (make-instance 'my-echo-executive :clock clock))
           (p (rmpl/package:find-package :morning-lecture))
           (executive
             (make-instance 'mtk-executive:accepting-executive
                            :executive (make-instance 'kirk-executive
                                                      :child-executive child-executive
                                                      :clock clock)))
           (goal
             (make-instance 'mtk-executive:rmpl-control-program-goal
                            :function-designator (find-symbol (string :main)
                                                              (rmpl/package:find-package :morning-lecture))
                            :function-args nil
                            :env nil))
           (events nil)
           (context (mtk-executive:make-execution-context executive nil goal nil
                                                          (lambda (context event)
                                                            (declare (ignore context))
                                                            (push event events))
                                                          :callback-on-join-thread-p t)))
      (mtk-executive:start-planning! context)
      (mtk-executive:join-context context)
      (let ((dispatch-events (drain-queue (slot-value child-executive 'events))))
        ;; HACK: This doesn't actually test that things were executed at the
        ;; correct time (or even in the correct order!). Syncronizing multiple
        ;; threads is hard... But baby steps.
        (is (set-equal `((:start (,(find-symbol "SHOWER" p)))
                         (:end (,(find-symbol "SHOWER" p)))
                         (:start (,(find-symbol "REVIEW-SCHEDULING-NOTES" p)))
                         (:start (,(find-symbol "EAT-BREAKFAST" p)))
                         (:end (,(find-symbol "REVIEW-SCHEDULING-NOTES" p)))
                         (:start (,(find-symbol "REVIEW-PLANNING-NOTES" p)))
                         (:end (,(find-symbol "EAT-BREAKFAST" p)))
                         (:end (,(find-symbol "REVIEW-PLANNING-NOTES" p)))
                         (:start (,(find-symbol "PACK-BAG" p)))
                         (:start (,(find-symbol "BIKE-TO-LECTURE" p)))
                         (:end (,(find-symbol "PACK-BAG" p)))
                         (:end (,(find-symbol "BIKE-TO-LECTURE" p))))
                       dispatch-events
                       :test #'equal))))))
