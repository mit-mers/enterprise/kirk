(uiop:define-package #:kirk-v2-tests/empty-model
    (:use #:cl
          #:fiveam
          #:kirk-v2
          #:mtk-executive))

(in-package #:kirk-v2-tests/empty-model)

(defclass my-model-executive (mtk-executive:echo-executive)
  ((events
    :initform (safe-queue:make-queue))))

(defclass my-model-dispatcher ()
  ((executive
    :initarg :executive)
   (context
    :initarg :context
    :reader context-of)))

(defmethod mtk-executive:simple-executive-make-dispatcher ((executive my-model-executive) context)
  (make-instance 'my-model-dispatcher
                 :executive executive
                 :context context))

(defmethod mtk-executive:execute-plan! ((dispatcher my-model-dispatcher) plan &key)
  (print (rmpl:model-file-path (rmpl:get-model
                                                   (mtk-executive::rmpl-model-of (mtk-executive:model-of (context-of dispatcher)))))))

(fiveam:def-suite :kirk-v2-tests/empty-model :in :kirk-v2)

(fiveam:in-suite :kirk-v2-tests/empty-model)

(fiveam:def-test test-pipeline ()
  (let ((rmpl:*environment* (rmpl:make-environment)))
    (rmpl:load-rmpl (asdf:system-relative-pathname :kirk-v2 "examples/morning-lecture/script.rmpl"))

    (is-true (find-symbol (string :test-model) (rmpl/package:find-package :morning-lecture))
             "could not find model")

    (let* (
           (child-executive (make-instance 'echo-executive))
           ;;(p (rmpl/package:find-package :morning-lecture))
           (kirk (make-instance 'kirk-v2:kirk-executive :child-executive child-executive))
           (kirk-goal (make-instance 'mtk-executive:rmpl-control-program-goal
                                     :env rmpl:*environment*
                                     :function-args ()
                                     :function-designator (find-symbol (string :main)
                                                                       (rmpl/package:find-package :morning-lecture))))
           (kirk-model (make-instance 'mtk-executive::rmpl-model
                                      :env rmpl:*environment*
                                      :model (find-symbol (string :test-model)
                                                          (rmpl/package:find-package :morning-lecture))))

           (kirk-init (make-instance 'mtk-executive:rmpl-initial-state
                                     :env rmpl:*environment*
                                     :initial-state ()))
           (kirk-context (mtk-executive:make-execution-context
                          kirk kirk-model kirk-goal kirk-init (lambda (context event)
                                                                (print event))))
           (kirk-planner (mtk-executive:simple-executive-make-planner kirk kirk-context))
           (kirk-plan (mtk-executive:simple-executive-planner-next-plans kirk-planner)))
           ;;(child-dispatcher (mtk-executive:simple-executive-make-dispatcher child-executive kirk-context))
           ;;(kirk-dispatcher (mtk-executive:simple-executive-make-dispatcher kirk kirk-context)))
      ;;(print (rmpl:model-file-path (rmpl:get-model (mtk-executive:model-of kirk-model)))))))
      ;;(print "plan below:")
      ;;(print kirk-plan)
      ;;(print "plan above")
      ;;(print (rmpl:model-file-path (rmpl:get-model (find-symbol (string :test-model) (rmpl/package:find-package :morning-lecture)))))
      (mtk-executive:execute-plan! kirk-context kirk-plan))))






;;(def-suite :kirk-v2-test/empty-model :in :kirk-v2)
;;(in-suite :kirk-v2-test/empty-model)
