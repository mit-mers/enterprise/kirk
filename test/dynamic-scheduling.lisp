(in-package #:kirk-v2-tests)

(fiveam:def-suite :kirk-v2/dynamic-scheduling :in :kirk-v2)
(fiveam:in-suite :kirk-v2/dynamic-scheduling)

;;; Dynamic Scheduler

(fiveam:def-test scheduling-morning-coffee-good ()
  (let* ((state-plan (make-coffee-state-plan "test morning coffee"))
         (scheduler (make-instance 'sched:dynamic-scheduler :state-plan state-plan))
         (events-observed 0)
         last-event
         (last-event-changed t)
         ;; check that the RTEDs are going out at the right times
         (known-correct-rted-times (list (cons "COFFEE-START" 0)
                                         (cons "++LOWER-COFFEE-READY-REWOL++" 30)
                                         (cons "COFFEE-READY" 35)
                                         (cons "GET-COFFEE" 50)
                                         (cons "MEETING-START" 55))))

    ;; simulate a dispatcher. we'll keep observing events when the scheduler tells us to, and then
    ;; we'll make sure that the following RTEDs make sense
    (loop named fake-dispatcher
          :do
             (let* ((rted (sched:get-next-rted scheduler))
                    (time (rted-time rted))
                    (events (rted-events rted)))

               (when (typep rted 'wait)
                 (return-from fake-dispatcher))

               (loop :for event-noop :in events
                     :do (let* ((event-id (first event-noop))
                                (known-correct-time (alexandria:assoc-value known-correct-rted-times event-id :test #'equal)))
                           ;; make sure we're trying to observe a new event each time
                           (setf last-event-changed (and last-event-changed (not (equal last-event event-id))))
                           (when (not last-event-changed)
                             (return-from fake-dispatcher))
                           (setf last-event event-id)

                           (is-true (equal known-correct-time time)
                                    (format nil "Event ~A should be observed/scheduled at ~A, not ~A" event-id known-correct-time time))

                           (is-true (sched:update-schedule! scheduler event-id time)
                                    (format nil "~A should have been successfully executed at ~A~%" event-id time))
                           (sched:update-schedule! scheduler event-id time)
                           (incf events-observed)))))

    ;; sanity check we ran through all the events
    (is-true last-event-changed
             "We should have tried to dispatch a new event each iteration")
    (is-true (equal events-observed 5)
             (format nil "Saw ~A instead of the 5 events that should have been observed" events-observed))))

(fiveam:def-test scheduling-morning-coffee-lower-bound ()
  (let* ((state-plan (make-coffee-state-plan "test morning coffee"))
         (scheduler (make-instance 'sched:dynamic-scheduler :state-plan state-plan))
         (events-observed 0)
         ;; use our own (simplified) decisions after the coffee observation arrives at its lower bound
         (our-rteds (list (list "COFFEE-START" 0)
                          (list "++LOWER-COFFEE-READY-REWOL++" 30)
                          (list "COFFEE-READY" 30)
                          (list "GET-COFFEE" 45)
                          (list "MEETING-START" 50))))

    (loop :for event-time :in our-rteds
          :do (let* ((event-id (first event-time))
                     (time (second event-time)))

                (is-true (sched:update-schedule! scheduler event-id time)
                         (format nil "~A should have been successfully executed at ~A~%" event-id time))

                (incf events-observed)))

    ;; sanity check we ran through all the events
    (is-true (equal events-observed 5)
             (format nil "Saw ~A instead of the 5 events that should have been observed" events-observed))))

(fiveam:def-test scheduling-morning-coffee-early-ctg ()
  (let* ((state-plan (make-coffee-state-plan "test morning coffee"))
         (scheduler (make-instance 'sched:dynamic-scheduler :state-plan state-plan))
         (events-observed 0)
         last-event
         (last-event-changed t)
         (our-rted-times (list (cons "COFFEE-START" 0)
                               ;; we're going to simulate an early notice the coffee is ready
                               (cons "COFFEE-READY" 20)
                               (cons "++LOWER-COFFEE-READY-REWOL++" 30)
                               (cons "GET-COFFEE" 45)
                               (cons "MEETING-START" 50))))

    ;; simulate a dispatcher. we'll keep observing events when the scheduler tells us to, and then
    ;; we'll make sure that the following RTEDs make sense
    (loop named fake-dispatcher
          :do
             (let* ((rted (sched:get-next-rted scheduler))
                    (events (rted-events rted)))
               (when (typep rted 'wait)
                 (return-from fake-dispatcher))

               (loop :for event-noop :in events
                     :do (let* ((event-id (first event-noop))
                                (time (alexandria:assoc-value our-rted-times event-id :test #'equal)))
                           ;; make sure we're trying to observe a new event each time
                           (setf last-event-changed (and last-event-changed (not (equal last-event event-id))))
                           (when (not last-event-changed)
                             (return-from fake-dispatcher))
                           (setf last-event event-id)

                           (is-true (sched:update-schedule! scheduler event-id time)
                                    (format nil "~A should have been successfully executed at ~A~%" event-id time))

                           (incf events-observed)))))

    (let ((commitments (get-commitments scheduler)))
      (is-true (equal (gethash (list "COFFEE-READY" t) commitments) (list 30 30))
               "The coffee being ready should have been buffered to the lower bound of its range"))

    ;; sanity check we ran through all the events
    (is-true last-event-changed
             "We should have tried to dispatch a new event each iteration")
    (is-true (equal events-observed 5)
             (format nil "Saw ~A instead of the 5 events that should have been observed" events-observed))))

;;; getting RTEDs

(fiveam:def-test dynamic-scheduler-make-rted ()
  (let* ((enabled-events (list (list "EVENT-ONE" nil) ; format of (event-id contingent-p)
                               (list "EVENT-TWO" t)
                               (list "EVENT-THREE" nil)
                               (list "++LOWER-EVENT-FOUR-REWOL++" nil))))

    (let ((commitments (make-hash-table :test #'equal)))

      ;; the lower bounds of the first event makes it the next decision
      (setf (gethash (first enabled-events) commitments) (list 1 2))
      (setf (gethash (second enabled-events) commitments) (list 2 3))
      (setf (gethash (third enabled-events) commitments) (list 3 4))
      (setf (gethash (fourth enabled-events) commitments) (list 4 5))

      (let* ((rted (collect-rted enabled-events commitments))
             (time (rted-time rted))
             (events (rted-events rted)))

        (is-true (equal time 1)
                 "The first free event should be the first to be scheduled at earliest time")
        (is-true (equal (length events) 1)
                 "Only one event has a lower bounds of 1")
        (is-true (equal (first events) (first enabled-events))
                 "The first enabled event should be scheduled")))

    (let ((commitments (make-hash-table :test #'equal)))
      ;; the lower bounds of the third event makes it the next decision
      (setf (gethash (first enabled-events) commitments) (list 3 4))
      (setf (gethash (second enabled-events) commitments) (list 5 9))
      (setf (gethash (third enabled-events) commitments) (list 2 4))
      (setf (gethash (fourth enabled-events) commitments) (list 4 5))

      (let* ((rted (collect-rted enabled-events commitments))
             (time (rted-time rted))
             (events (rted-events rted)))

        (is-true (equal time 2)
                 "EVENT-THREE starts at the earliest time")
        (is-true (equal (length events) 1)
                 "Only one event has a lower bounds of 2")
        (is-true (equal (first events) (third enabled-events))
                 "The third event is the one that should be scheduled")))

    (let ((commitments (make-hash-table :test #'equal)))
      ;; the upper bounds of the second (contingent) event makes it the next decision
      (setf (gethash (first enabled-events) commitments) (list 3 4))
      (setf (gethash (second enabled-events) commitments) (list 1 2))
      (setf (gethash (third enabled-events) commitments) (list 3 4))
      (setf (gethash (fourth enabled-events) commitments) (list 4 5))

      (let* ((rted (collect-rted enabled-events commitments))
             (time (rted-time rted))
             (events (rted-events rted)))

        (is-true (equal time 2)
                 "EVENT-TWO must be observed by its upper bound")
        (is-true (equal (length events) 1)
                 "Only one event has a time of 2")
        (is-true (equal (first events) (second enabled-events))
                 "The second event is the one that should be scheduled")))

    (let ((commitments (make-hash-table :test #'equal)))
      ;; two free events should be dispatched simultaneously
      (setf (gethash (first enabled-events) commitments) (list 1 2))
      (setf (gethash (second enabled-events) commitments) (list 2 3))
      (setf (gethash (third enabled-events) commitments) (list 1 3))
      (setf (gethash (fourth enabled-events) commitments) (list 4 5))

      (let* ((rted (collect-rted enabled-events commitments))
             (time (rted-time rted))
             (events (rted-events rted)))

        (is-true (equal time 1)
                 "EVENT-ONE and EVENT-THREE have the same lower bound")
        (is-true (equal (length events) 2)
                 "Two events have the same lower bound")
        (is-true (and (equal (first events) (first enabled-events))
                      (equal (second events) (third enabled-events)))
                 "The first and third events should be scheduled")))

    (let ((commitments (make-hash-table :test #'equal)))
      ;; the synthetic event should be marked as a noop
      (setf (gethash (first enabled-events) commitments) (list 2 2))
      (setf (gethash (second enabled-events) commitments) (list 2 3))
      (setf (gethash (third enabled-events) commitments) (list 2 3))
      (setf (gethash (fourth enabled-events) commitments) (list 1 5))

      (let* ((rted (collect-rted enabled-events commitments))
             (time (rted-time rted))
             (events (rted-events rted)))

        (is-true (equal time 1)
                 "++LOWER-EVENT-FOUR-REWOL++ is ready first")
        (is-true (equal (length events) 1)
                 "Only the synthetic event should be dispatched")
        (is-true (equal (first events) (list "++LOWER-EVENT-FOUR-REWOL++" t))
                 "++LOWER-EVENT-FOUR-REWOL++ should be marked noop")))

    (let ((commitments (make-hash-table :test #'equal)))
      ;; mixing contingent upper bounds and free lower bounds is fine
      (setf (gethash (first enabled-events) commitments) (list 2 3))
      (setf (gethash (second enabled-events) commitments) (list 1 2))
      (setf (gethash (third enabled-events) commitments) (list 5 7))
      (setf (gethash (fourth enabled-events) commitments) (list 6 5))

      (let* ((rted (collect-rted enabled-events commitments))
             (time (rted-time rted))
             (events (rted-events rted)))

        (is-true (equal time 2)
                 "The next decision should be at time 2")
        (is-true (equal (length events) 2)
                 "The lower bounds of the first event and the upper of the second match")
        (is-true (and (equal (first events) (first enabled-events))
                      (equal (second events) (second enabled-events)))
                 "A free and a (noop) contingent event will be dispatched together")))))

;;; Freezing contingent events

(fiveam:def-test freezing-contingent-events ()
  (let* ((state-plan (make-coffee-state-plan "test morning coffee")))

    (let ((scheduler (make-instance 'sched:dynamic-scheduler :state-plan state-plan)))
      (is-true (sched:maybe-reschedule scheduler "COFFEE-READY" 20)
               "The new STNU should still be controllable"))

    ;; manually call maybe-reschedule, see if the new times show up
    (let ((scheduler (make-instance 'sched:dynamic-scheduler :state-plan state-plan)))
      (is-true (sched:update-schedule! scheduler "COFFEE-START" 0)
               "COFFEE-START scheduled")

      (is-true (let ((next (sched:get-next-rted scheduler)))
                 (and (equal (sched:rted-time next) 30)
                      (equal (first (first (sched:rted-events next))) "++LOWER-COFFEE-READY-REWOL++")))
               "The normalized lower bound of COFFEE-READY should initially be at 30")

      (is-true (sched:maybe-reschedule scheduler "COFFEE-READY" 20)
               "The new STNU should still be controllable")

      ;; if COFFEE-READY is observed at 20, we're guaranteed it actually occurred at 15
      (is-true (let ((next (sched:get-next-rted scheduler)))
                 (and (equal (sched:rted-time next) 15)
                      (equal (first (first (sched:rted-events next))) "++LOWER-COFFEE-READY-REWOL++")))
               "The normalized lower bound of COFFEE-READY should drop to 15")

      ;; kinda looks like we're scheduling this in the past, but we know for sure that the coffee
      ;; was ready at t=15 as well
      (is-true (sched:update-schedule! scheduler "++LOWER-COFFEE-READY-REWOL++" 15)
               "++LOWER-COFFEE-READY-REWOL++ scheduled"))

    ;; make sure reschedulep is doing what it should during update-schedule!
    (let ((scheduler (make-instance 'sched:dynamic-scheduler :state-plan state-plan :reschedule-early-ctg-p t)))
      (is-true (sched:update-schedule! scheduler "COFFEE-START" 0)
               "COFFEE-START scheduled")

      (is-true (let ((next (sched:get-next-rted scheduler)))
                 (and (equal (sched:rted-time next) 30)
                      (equal (first (first (sched:rted-events next))) "++LOWER-COFFEE-READY-REWOL++")))
               "The normalized lower bound of COFFEE-READY should initially be at 30")

      (is-true (sched:update-schedule! scheduler "COFFEE-READY" 20)
               "The new STNU should still be controllable and we should succeed in scheduling the ctg event")

      (is-true (let ((next (sched:get-next-rted scheduler)))
                 (and (equal (sched:rted-time next) 15)
                      (equal (first (first (sched:rted-events next))) "++LOWER-COFFEE-READY-REWOL++")))
               "The normalized lower bound of COFFEE-READY should drop to 15")

      (is-true (sched:update-schedule! scheduler "++LOWER-COFFEE-READY-REWOL++" 15)
               "++LOWER-COFFEE-READY-REWOL++ scheduled"))

    ;; make sure we revert when the new constraints are bad
    (let ((scheduler (make-instance 'sched:dynamic-scheduler :state-plan state-plan :reschedule-early-ctg-p t)))
      (is-true (sched:update-schedule! scheduler "COFFEE-START" 0)
               "COFFEE-START scheduled")

      (is-false (sched:maybe-reschedule scheduler "COFFEE-READY" 50)
                "We should not be able to reschedule COFFEE-READY so late")

      (is-true (let ((next (sched:get-next-rted scheduler)))
                 (and (equal (sched:rted-time next) 30)
                      (equal (first (first (sched:rted-events next))) "++LOWER-COFFEE-READY-REWOL++")))
               "The normalized lower bound of COFFEE-READY should be unchanged")

      (is-true (sched:update-schedule! scheduler "++LOWER-COFFEE-READY-REWOL++" 30)
               "++LOWER-COFFEE-READY-REWOL++ scheduled")

      (is-true (sched:update-schedule! scheduler "COFFEE-READY" 34)
               "COFFEE-READY scheduled")

      (is-true (let ((next (sched:get-next-rted scheduler)))
                 (and (equal (sched:rted-time next) 49)
                      (equal (first (first (sched:rted-events next))) "GET-COFFEE")))
               "GET-COFFEE 15 mins after COFFEE-READY like usual"))

    ;; if reschedule-early-p is not set, we shouldn't be trying to reschedule
    (let ((scheduler (make-instance 'sched:dynamic-scheduler :state-plan state-plan :reschedule-early-ctg-p nil)))
      (is-true (sched:update-schedule! scheduler "COFFEE-START" 0)
               "COFFEE-START scheduled")

      (is-true (sched:update-schedule! scheduler "COFFEE-READY" 20)
               "We should still be updating the schedule even if we don't reschedule")

      (is-true (let ((next (sched:get-next-rted scheduler)))
                 (and (equal (sched:rted-time next) 30)
                      (equal (first (first (sched:rted-events next))) "++LOWER-COFFEE-READY-REWOL++")))
               "The normalized lower bound of COFFEE-READY should be unchanged")

      (is-true (sched:update-schedule! scheduler "++LOWER-COFFEE-READY-REWOL++" 30)
               "++LOWER-COFFEE-READY-REWOL++ scheduled")

      (is-true (let ((next (sched:get-next-rted scheduler)))
                 (and (equal (sched:rted-time next) 45)
                      (equal (first (first (sched:rted-events next))) "GET-COFFEE")))
               "GET-COFFEE 15 mins after COFFEE-READY like usual"))))

;;; Real-Time Execution Decisions (RTEDs)

(fiveam:def-test make-wait-rted ()
  (let ((decision (make-rted)))
    (is (typep decision 'wait)
        "RTEDs default to wait decisions")))

(fiveam:def-test make-rted-with-events ()
  (let* ((time 1)
         (events (list (values "event-id" nil)))
         (decision (make-rted :time time :events events)))
    (is (not (typep decision 'wait))
        "If there is a (T, x), this is not a wait decision")
    (is (eq time (rted-time decision))
        "Times match")
    (is (eq 1 (length (rted-events decision)))
        "Events match")))

;;; Freezing ctg events

(fiveam:def-test narrow-ctg-events ()
  (let ((observation-time 40)
        (original-lower 15)
        (original-upper 30)
        (original-gamma- 5)
        (original-gamma+ 15)

        (expected-lower 25)
        (expected-upper 30)
        (expected-gamma- 10)
        (expected-gamma+ 15))

    (destructuring-bind (lower upper gamma- gamma+)
        (sched:narrow-bounds observation-time original-lower original-upper original-gamma- original-gamma+)

      (is-true (equal lower expected-lower)
               (format nil "lower bounds should be ~A at T = ~A. Saw: ~A" expected-lower observation-time lower))
      (is-true (equal upper expected-upper)
               (format nil "upper bound should be ~A at T = ~A. Saw: ~A" expected-upper observation-time upper))
      (is-true (equal gamma- expected-gamma-)
               (format nil "gamma- should be ~A at T = ~A. Saw: ~A" expected-gamma- observation-time gamma-))
      (is-true (equal gamma+ expected-gamma+)
               (format nil "gamma+ should be ~A at T = ~A. Saw: ~A" expected-gamma+ observation-time gamma+))))

  (let ((observation-time 35)
        (original-lower 15)
        (original-upper 30)
        (original-gamma- 5)
        (original-gamma+ 15)

        (expected-lower 20)
        (expected-upper 30)
        (expected-gamma- 5)
        (expected-gamma+ 15))

    (destructuring-bind (lower upper gamma- gamma+)
        (sched:narrow-bounds observation-time original-lower original-upper original-gamma- original-gamma+)

      (is-true (equal lower expected-lower)
               (format nil "lower bounds should be ~A at T = ~A. Saw: ~A" expected-lower observation-time lower))
      (is-true (equal upper expected-upper)
               (format nil "upper bound should be ~A at T = ~A. Saw: ~A" expected-upper observation-time upper))
      (is-true (equal gamma- expected-gamma-)
               (format nil "gamma- should be ~A at T = ~A. Saw: ~A" expected-gamma- observation-time gamma-))
      (is-true (equal gamma+ expected-gamma+)
               (format nil "gamma+ should be ~A at T = ~A. Saw: ~A" expected-gamma+ observation-time gamma+))))

  (let ((observation-time 30)
        (original-lower 15)
        (original-upper 30)
        (original-gamma- 5)
        (original-gamma+ 15)

        (expected-lower 15)
        (expected-upper 25)
        (expected-gamma- 5)
        (expected-gamma+ 15))

    (destructuring-bind (lower upper gamma- gamma+)
        (sched:narrow-bounds observation-time original-lower original-upper original-gamma- original-gamma+)

      (is-true (equal lower expected-lower)
               (format nil "lower bounds should be ~A at T = ~A. Saw: ~A" expected-lower observation-time lower))
      (is-true (equal upper expected-upper)
               (format nil "upper bound should be ~A at T = ~A. Saw: ~A" expected-upper observation-time upper))
      (is-true (equal gamma- expected-gamma-)
               (format nil "gamma- should be ~A at T = ~A. Saw: ~A" expected-gamma- observation-time gamma-))
      (is-true (equal gamma+ expected-gamma+)
               (format nil "gamma+ should be ~A at T = ~A. Saw: ~A" expected-gamma+ observation-time gamma+))))

  (let ((observation-time 25)
        (original-lower 15)
        (original-upper 30)
        (original-gamma- 5)
        (original-gamma+ 15)

        (expected-lower 15)
        (expected-upper 20)
        (expected-gamma- 5)
        (expected-gamma+ 10))

    (destructuring-bind (lower upper gamma- gamma+)
        (sched:narrow-bounds observation-time original-lower original-upper original-gamma- original-gamma+)

      (is-true (equal lower expected-lower)
               (format nil "lower bounds should be ~A at T = ~A. Saw: ~A" expected-lower observation-time lower))
      (is-true (equal upper expected-upper)
               (format nil "upper bound should be ~A at T = ~A. Saw: ~A" expected-upper observation-time upper))
      (is-true (equal gamma- expected-gamma-)
               (format nil "gamma- should be ~A at T = ~A. Saw: ~A" expected-gamma- observation-time gamma-))
      (is-true (equal gamma+ expected-gamma+)
               (format nil "gamma+ should be ~A at T = ~A. Saw: ~A" expected-gamma+ observation-time gamma+))))

  (let ((observation-time 20)
        (original-lower 15)
        (original-upper 30)
        (original-gamma- 5)
        (original-gamma+ 15)

        (expected-lower 15)
        (expected-upper 15)
        (expected-gamma- 5)
        (expected-gamma+ 5))

    (destructuring-bind (lower upper gamma- gamma+)
        (sched:narrow-bounds observation-time original-lower original-upper original-gamma- original-gamma+)

      (is-true (equal lower expected-lower)
               (format nil "lower bounds should be ~A at T = ~A. Saw: ~A" expected-lower observation-time lower))
      (is-true (equal upper expected-upper)
               (format nil "upper bound should be ~A at T = ~A. Saw: ~A" expected-upper observation-time upper))
      (is-true (equal gamma- expected-gamma-)
               (format nil "gamma- should be ~A at T = ~A. Saw: ~A" expected-gamma- observation-time gamma-))
      (is-true (equal gamma+ expected-gamma+)
               (format nil "gamma+ should be ~A at T = ~A. Saw: ~A" expected-gamma+ observation-time gamma+))))

  (let ((observation-time 35)
        (original-lower 15)
        (original-upper 30)
        (original-gamma- 5)
        (original-gamma+ 15)

        (expected-lower 20)
        (expected-upper 30)
        (expected-gamma- 5)
        (expected-gamma+ 15))

    (destructuring-bind (lower upper gamma- gamma+)
        (sched:narrow-bounds observation-time original-lower original-upper original-gamma- original-gamma+)

      (is-true (equal lower expected-lower)
               (format nil "lower bounds should be ~A at T = ~A. Saw: ~A" expected-lower observation-time lower))
      (is-true (equal upper expected-upper)
               (format nil "upper bound should be ~A at T = ~A. Saw: ~A" expected-upper observation-time upper))
      (is-true (equal gamma- expected-gamma-)
               (format nil "gamma- should be ~A at T = ~A. Saw: ~A" expected-gamma- observation-time gamma-))
      (is-true (equal gamma+ expected-gamma+)
               (format nil "gamma+ should be ~A at T = ~A. Saw: ~A" expected-gamma+ observation-time gamma+))))

  ;; check that we don't make up observation delay
  (let ((observation-time 35)
        (original-lower 30)
        (original-upper 40)
        (original-gamma- 0)
        (original-gamma+ 0)

        (expected-lower 35)
        (expected-upper 35)
        (expected-gamma- 0)
        (expected-gamma+ 0))

    (destructuring-bind (lower upper gamma- gamma+)
        (sched:narrow-bounds observation-time original-lower original-upper original-gamma- original-gamma+)

      (is-true (equal lower expected-lower)
               (format nil "lower bounds should be ~A at T = ~A. Saw: ~A" expected-lower observation-time lower))
      (is-true (equal upper expected-upper)
               (format nil "upper bound should be ~A at T = ~A. Saw: ~A" expected-upper observation-time upper))
      (is-true (equal gamma- expected-gamma-)
               (format nil "gamma- should be ~A at T = ~A. Saw: ~A" expected-gamma- observation-time gamma-))
      (is-true (equal gamma+ expected-gamma+)
               (format nil "gamma+ should be ~A at T = ~A. Saw: ~A" expected-gamma+ observation-time gamma+)))))
