(in-package #:kirk-v2-tests)

(fiveam:def-suite :kirk-v2/delay-scheduling :in :kirk-v2)
(fiveam:in-suite :kirk-v2/delay-scheduling)

(fiveam:def-test variable-to-fixed ()
  (let* ((state-plan-name "variable-delay")
         (state-plan (make-variable-state-plan state-plan-name))
         (vdc-stnu (%kirk-v2/scheduler/utils:state-plan-to-stnu state-plan))
         (fixed-stnu (kirk-v2/scheduler:vdc-to-fixed vdc-stnu)))
    (is-true t
             "tests are running")))
