(in-package #:kirk-v2-tests)

(defun make-variable-state-plan (state-plan-name)
  "Example CPS state-plan adapted from the examples in the Odo repo. Describes two rovers"
  (let* ((start-event (odo:make-event :value 0 :name 'start)))
    (aprog1 (odo:make-state-plan :name state-plan-name
                                 :start-event start-event)
      (let* ((ep1-start-event (odo:make-event :name 'ep1-start))

             (ep1-end-event (odo:make-event :name 'ep1-end :uncontrollable-p t))

             (ep2-start-event (odo:make-event :name 'ep2-start))

             (ep2-end-event (odo:make-event :name 'ep2-end))

             (rover-location (odo:make-state-variable '((member "free-space" "region-a" "region-b"))
                                                      :name 'rover-location))
             (rover-x (odo:make-state-variable '(real) :name 'rover-x))
             (rover-y (odo:make-state-variable '(real) :name 'rover-y))

             (ep1 (odo:make-episode :start-event ep1-start-event
                                    :end-event ep1-end-event
                                    :contingent-p t
                                    :temporal-constraint (odo:simple-temporal ep1-start-event
                                                                              ep1-end-event
                                                                              :lower-bound 1
                                                                              :upper-bound 2
                                                                              :min-observation-delay 0
                                                                              :max-observation-delay 1)
                                    :over-all-constraints
                                    (list (odo:make-state-constraint
                                           (odo:equal (odo:@ rover-location odo:+current-time+) "region-a")))
                                    :name 'ep1))
             (ep2 (odo:make-episode :start-event ep2-start-event
                                    :end-event ep2-end-event
                                    :temporal-constraint (odo:simple-temporal ep2-start-event
                                                                              ep2-end-event
                                                                              :lower-bound 1
                                                                              :upper-bound 2)
                                    :over-all-constraints
                                    (list (odo:make-state-constraint
                                           (odo:equal (odo:@ rover-location odo:+current-time+) "region-b")))
                                    :name 'ep2))

             (tc1 (odo:make-constraint (odo:simple-temporal start-event
                                                            ep1-start-event
                                                            :upper-bound 1)
                                       :name 'start-ep1-bound))
             (tc2 (odo:make-constraint (odo:simple-temporal ep1-end-event
                                                            ep2-start-event
                                                            :lower-bound 0
                                                            :upper-bound 1)
                                       :name 'episode-downtime-bound))
             (tc3 (odo:make-constraint (odo:simple-temporal start-event
                                                            ep2-end-event
                                                            :lower-bound 5)
                                       :name 'overall-time-bound)))

        ;; Add the other variables
        (odo:state-space-add! it rover-location)

        (odo:state-space-add! it rover-x)
        (odo:state-space-add! it rover-y)

        (odo:add-goal-episode! it ep1 ep2)
        (odo:add-constraint! it tc1)
        (odo:add-constraint! it tc2)
        (odo:add-constraint! it tc3)))))

(defun make-coffee-state-plan (state-plan-name)
  "State plan representation of the coffee delay STNU from Bhargava 2022, p. 4"
  (let* ((start-event (odo:make-event :value 0 :name 'start))
         (state-plan (odo:make-state-plan :name state-plan-name :start-event start-event))

         (coffee-start (odo:make-event :name 'coffee-start))                        ; A
         (coffee-ready (odo:make-event :name 'coffee-ready :uncontrollable-p t))    ; B
         (get-coffee (odo:make-event :name 'get-coffee))                            ; C
         (meeting-start (odo:make-event :name 'meeting-start))                      ; D

         (brewing (odo:make-constraint (odo:simple-temporal coffee-start coffee-ready
                                                            :lower-bound 15
                                                            :upper-bound 30
                                                            :min-observation-delay 5
                                                            :max-observation-delay 15)
                                       :contingent-p t))
         (cooling (odo:make-constraint (odo:simple-temporal coffee-ready get-coffee
                                                            :lower-bound 20
                                                            :upper-bound 30)))
         (drinking (odo:make-constraint (odo:simple-temporal get-coffee meeting-start
                                                             :lower-bound 5
                                                             :upper-bound 5)))
         (routine (odo:make-constraint (odo:simple-temporal coffee-start meeting-start
                                                            :lower-bound 0
                                                            :upper-bound 60))))

    (odo:add-constraint! state-plan brewing)
    (odo:add-constraint! state-plan cooling)
    (odo:add-constraint! state-plan drinking)
    (odo:add-constraint! state-plan routine)

    state-plan))

(fiveam:def-suite :kirk-v2/sample-state-plan :in :kirk-v2)
(fiveam:in-suite :kirk-v2/sample-state-plan)

(fiveam:def-test state-plan-to-variable-stnu ()
  (let* ((state-plan-name "test-state-plan")
         (state-plan (make-variable-state-plan state-plan-name))
         (stnu (utils:state-plan-to-stnu state-plan))
         (ev1 (tn:find-event stnu 'ep1-end))
         (ev2 (tn:find-event stnu 'ep2-end))
         (constraints (hash-table-keys (tn::constraints-by-id stnu)))
         (ep1 (tn:find-temporal-constraint-by-name stnu "EP1"))
         (num-constraints (length constraints))
         (num-events (length (hash-table-keys (tn::events-by-id stnu)))))

    (is-true (eql (tn:name stnu) state-plan-name)
             "state plan and stnu should have the same name")

    ;; start -> ep1-start -> ep1-end; ep2-start -> ep2-end
    (is-true (eql num-events 5)
             "there should be 5 temporal events in the stnu")
    (is-true (eql num-constraints 5)
             "there should be 5 temporal constraints in the stnu")

    ;; spot check a couple events
    (is-true (typep ev1 'tn:temporal-event)
             "end event 1 should exist in the stnu")
    (is-false (tn:controllable? ev1)
              "end event 1 is uncontrollable")
    (is-true (typep ev2 'tn:temporal-event)
             "end event 2 should exist")

    ;; make sure observation delay is present in the stnu
    (is-true (eql (tn:max-observation-delay ep1) 1)
             "end event 1 should have variable delay")

    ;; sanity check controllability
    (multiple-value-bind (controllablep conflicts)
        (temporal-controllability:dynamically-controllable? stnu)
      (is-true controllablep
               "the stnu should be dynamically controllable")
      (is (eql nil conflicts)
               "there should not be conflicts in this stnu"))

    (multiple-value-bind (controllablep conflicts)
        (temporal-controllability:variable-delay-controllable? stnu)
      (is-true controllablep
               "the stnu should be variable-delay controllable")
      (is (eql nil conflicts)
               "there should not be conflicts in this stnu"))))

(fiveam:def-test coffee-to-variable-stnu ()
  (let* ((state-plan-name "coffee-state-plan")
         (state-plan (make-coffee-state-plan state-plan-name))
         (stnu (utils:state-plan-to-stnu state-plan))

         (events (tn::events-by-id stnu))
         (constraints (tn::constraints-by-id stnu))

         (expected-constraints (make-hash-table :test #'equal)))

    ;; build a hash table to compare the STNU against
    (setf (gethash '("COFFEE-START" "MEETING-START") expected-constraints) '(0 60))
    (setf (gethash '("COFFEE-START" "COFFEE-READY") expected-constraints) '(15 30))
    (setf (gethash '("COFFEE-READY" "GET-COFFEE") expected-constraints) '(20 30))
    (setf (gethash '("GET-COFFEE" "MEETING-START") expected-constraints) '(5 5))

    (is-true (eq 4 (hash-table-count events))
             "4 events in the coffee example")
    (is-true (eq 4 (hash-table-count constraints))
             "4 constraints in the coffee example")

    (tn:do-constraints (constraint stnu)
      (let ((from (tn:name (tn:from-event constraint)))
            (to (tn:name (tn:to-event constraint)))
            (lower (tn:lower-bound constraint))
            (upper (tn:upper-bound constraint)))
        (is-true (equal (gethash `(,from ,to) expected-constraints)
                        `(,lower ,upper))
                 (format t "~%~S - ~S ∈ [~S, ~S], should be in the STNU~%" to from lower upper))))))
