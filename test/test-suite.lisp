(in-package :kirk-v2-tests)

(def-suite :kirk-v2 :description "Test behavior of Kirk systems")
(in-suite :kirk-v2)
