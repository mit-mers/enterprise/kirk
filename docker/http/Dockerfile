# This Dockerfile builds the Kirk HTTP server.
#
# The first stage first downloads and installs CLPM. It then builds SBCL with
# core compression enabled. Next, it uses CLPM to fetch all dependencies (from
# the clpmfile.lock file). Last, it builds and dumps the Kirk HTTP server.
FROM daewok/sbcl:1.4.13-debian-build as builder

# hadolint ignore=DL3008
RUN apt-get update \
    && apt-get install -y --no-install-recommends coinor-libipopt-dev

# hadolint ignore=DL3008
RUN apt-get install -y --no-install-recommends git unzip wget libssl1.0-dev ca-certificates

# This corresponds to clpm-v0.0.5-linux-gnu.zip
ENV CLPM_URL=https://gitlab.com/clpm/clpm/-/jobs/132588061/artifacts/download

WORKDIR /build/

# Add settings for using quicklisp and git.mers
COPY docker/http/clpm.toml /root/.config/clpm/clpm.toml

# Download and install CLPM.
RUN wget "${CLPM_URL}" -O clpm.zip \
    && unzip clpm.zip \
    && mv clpm /usr/local/bin/clpm

RUN clpm sync -VV

# Copy over the target features file for SBCL and rebuild it.
COPY docker/http/customize-target-features.lisp /usr/local/src/sbcl-${SBCL_VERSION}/customize-target-features.lisp
RUN set -x \
    && rebuild-sbcl

# Download all dependencies
ARG gitlab_user
ARG gitlab_token
COPY ./clpmfile /build/kirk-v2/clpmfile
COPY ./clpmfile.lock /build/kirk-v2/clpmfile.lock
COPY *.asd /build/kirk-v2/
WORKDIR /build/kirk-v2/
# hadolint ignore=SC2154
RUN CLPM_BUNDLE_GIT_AUTH_GIT__MERS__CSAIL__MIT__EDU="${gitlab_user}:${gitlab_token}" \
    && export CLPM_BUNDLE_GIT_AUTH_GIT__MERS__CSAIL__MIT__EDU \
    && clpm bundle install -VV \
    && rm clpmfile clpmfile.lock ./*.asd

# Copy over the source code...
COPY . /build/kirk-v2/
# ...and build!
RUN set -x \
    && git_version=$(git rev-parse --short HEAD) \
    && clpm bundle exec sbcl --non-interactive --no-sysinit --no-userinit \
    --eval "(require :asdf)" \
    --eval "(defmethod asdf:perform ((cl-user::o asdf:image-op) (cl-user::c asdf:system)) (uiop:dump-image (asdf:output-file cl-user::o cl-user::c) :executable (typep cl-user::o 'asdf:program-op) #+:sb-core-compression :compression #+:sb-core-compression t))" \
    --eval "(defmethod asdf:output-files ((op asdf:program-op) (system (eql (asdf:find-system :kirk-v2-http)))) \
    (values (list #p\"/build/bin/kirk-v2-http\") t))" \
    --eval '(pushnew :drakma-no-ssl *features*)' \
    --eval '(pushnew :hunchentoot-no-ssl *features*)' \
    --eval '(asdf:load-system :hunchentoot)' \
    --eval '(asdf:load-system :clack-handler-hunchentoot)' \
    --eval '(asdf:load-system :kirk-v2-http)' \
    --eval '(asdf:load-system :opsat-v3/subsolvers/ipopt/all)' \
    --eval "(setf kirk-v2-http/server::*version* \"${git_version}\")" \
    --eval "(asdf:operate 'asdf:program-op :kirk-v2-http)"

# Start creating the runtime image.
FROM debian:stretch

# hadolint ignore=DL3008
RUN apt-get update \
    && apt-get install -y --no-install-recommends coinor-libipopt-dev \
    && rm -rf /var/lib/apt/lists/*

# Copy over the executable from the builder.
COPY --from=builder /build/bin/kirk-v2-http /usr/local/bin/kirk-v2-http

# Last, set the entrypoint.
ENTRYPOINT ["/usr/local/bin/kirk-v2-http"]
