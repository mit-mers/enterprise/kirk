;;;; KIRK-V2 System Definition.

#-:asdf3.1
(error "Requires ASDF >=3.1")

(asdf:load-system :mtk-asdf)

(defsystem #:kirk-v2
  :version (:read-file-form "src/version.lisp" :at (2 2))
  :serial nil
  :defsystem-depends-on ((:version #:mtk-asdf "0.1.1"))
  ;; :class "mtk-asdf:mtk-system"
  :pathname "src/kirk/"
  :entry-point "kirk-v2/cli:main"
  ;; :build-operation "asdf-release-ops:dynamic-program-op"
  ;; :build-pathname #-os-windows "../build/bin/kirk" #+os-windows "../build/bin/kirk.exe"
  :in-order-to ((test-op (load-op "kirk-v2-tests")))
  :perform (test-op (op c) (symbol-call :fiveam :run! :kirk-v2))
  :depends-on ("40ants-doc"
               "kirk-v2/executive"
               "kirk-v2/planner"
               "kirk-v2/dispatcher"
               "kirk-v2/scheduler")
  :components ((:file "package")
               (:file "doc" :depends-on ("package")))

  ;; :release-staging-directory "../build/release-staging/"
  ;; :release-directory "../releases/")
  )

(defsystem #:kirk-v2/doc
  :version (:read-file-form "src/version.lisp" :at (2 2))
  :serial nil
  :pathname "src/doc"
  :depends-on ("kirk-v2" "40ants-doc-full")
  :components ((:file "doc")))

(defsystem #:kirk-v2/executive
  :version (:read-file-form "src/version.lisp" :at (2 2))
  :serial nil
  :defsystem-depends-on ((:version #:mtk-asdf "0.1.1"))
  :pathname "src/executive"
  :depends-on ("40ants-doc"
               "bordeaux-threads" "odo" "safe-queue"
               "mtk-executive"
               "kirk-v2/dispatcher"
               "kirk-v2/planner")
  :components ((:file "package")
               (:file "executive" :depends-on ("package"))))

(defsystem #:kirk-v2/dispatcher
  :version (:read-file-form "src/version.lisp" :at (2 2))
  :serial nil
  :defsystem-depends-on ((:version #:mtk-asdf "0.1.1"))
  :pathname "src/dispatcher"
  :depends-on ("40ants-doc"
               "bordeaux-threads" "odo" "safe-queue"
               "kirk-v2/scheduler")
  :components ((:file "package")
               (:file "base" :depends-on ("package"))
               (:file "driver" :depends-on ("package"))
               (:file "event-dispatcher" :depends-on ("base"))
               (:file "dynamic-dispatcher" :depends-on ("base" "event-dispatcher"))
               (:file "mtk-executive-driver" :depends-on ("driver" "event-dispatcher" "dynamic-dispatcher"))))

(defsystem #:kirk-v2/planner
  :version (:read-file-form "src/version.lisp" :at (2 2))
  :serial nil
  :defsystem-depends-on ((:version #:mtk-asdf "0.1.1"))
  :pathname "src/planner"
  :depends-on ("40ants-doc" "opsat-v3" "split-sequence"
               "anaphora" "alexandria" "iterate" "jsown" "mtk-executive" "rmpl"
               "rmpl/state-plan/state-plan"
               "bordeaux-threads" "odo")
  :components ((:file "package")
               (:file "automata-to-state-plan" :depends-on ("package"))
               (:file "causal-links" :depends-on ("package"))
               (:file "state-plan" :depends-on ("package"))
               (:file "planner" :depends-on ("package"))))

(defsystem #:kirk-v2/scheduler
  :version (:read-file-form "src/version.lisp" :at (2 2))
  :serial nil
  :defsystem-depends-on ((:version #:mtk-asdf "0.1.1"))
  ;;:class "mtk-asdf:mtk-system"
  :pathname "src/scheduler"
  :depends-on ("40ants-doc" "alexandria" "float-features" "log4cl"
                            ;;"mcclim-raster-image" "mtk-mcclim-dot"
                            "odo"
                            "opsat-v3/subsolvers/temporal-networks/common"
                            "temporal-controllability"
                            "temporal-networks"
                            "trivial-cltl2")
  :components ((:file "package")
               (:file "doc" :depends-on ("package"))
               (:module "defs"
                :depends-on ("package")
                :components ((:file "package")
                             (:file "defs" :depends-on ("package"))))
               (:module "utils"
                :depends-on ("defs")
                :components ((:file "package")
                             (:file "inf" :depends-on ("package"))
                             (:file "generics" :depends-on ("package"))
                             (:file "edge-overlay" :depends-on ("package"))
                             (:file "distance-graph" :depends-on ("package" "generics"))
                             ;;(:file "clim" :depends-on ("package"))
                             (:file "floyd-warshall" :depends-on ("inf"))
                             (:file "standard-scheduler" :depends-on ("package" "generics"))
                             (:file "state-plan-to-stnu" :depends-on ("package"))))
               (:module "mdf"
                :depends-on ("utils")
                :components ((:file "package")
                             (:file "mdf" :depends-on ("package"))))
               (:module "delay"
                :depends-on ("utils")
                :components ((:file "package")
                             (:file "delay" :depends-on ("package"))))
               (:module "dynamic"
                :depends-on ("utils")
                :components ((:file "package")
                             (:file "rted" :depends-on ("package"))
                             (:file "dynamic" :depends-on ("package" "rted"))))
               ;; (:module "amc"
               ;;  :depends-on ("utils")
               ;;  :components ((:file "package")
               ;;               ;(:file "amc" :depends-on ("package"))
               ;;               ))
               ))
